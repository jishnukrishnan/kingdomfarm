<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\wcategories;
use App\AssetList;
use App\operators;
use App\spareparts;
use App\field_data;
use App\inventories;

class workshopController extends Controller
{
   function manageAssets()
   {
       $assetlists = AssetList::where('AL_status',0)
                    ->join('wcategories','wcategories.C_id','=','assetlist.AL_cateId')
                    ->orderBy('AL_id','desc')->get();
       $categories = wcategories::where('C_status',0)->orderBy('C_id','desc')->get();
       return view('Workshop.assetList')->with(['assetlists' => $assetlists, 'categories' => $categories]);
   }

   function addAssets(Request $request)
   {
       $regno = $request->al_regno;
       $category = $request->al_category;
       $modelno = $request->al_modelno;
       $engine = $request->al_engine;
       $serialno = $request->al_slno;

       $action = $request->submit ;

       $insData = array('AL_regNo' => $regno , 'AL_cateId' => $category , 'AL_modelNo' => $modelno , 'AL_engine' => $engine , 'AL_serialNo' => $serialno );

       if($action == 'Add')
       {
           $insData['created_at'] = $this->getDate();
           AssetList::insert($insData);
       }
       elseif($action == 'Update')
       {
           $cate_id = $request->category_id ;
           $insData['updated_at'] = $this->getDate();
           AssetList::where('AL_id',$cate_id)->update($insData);
       }

       return redirect('/assetList')->with('success','AssetList Added Successfully');

   }

   function getAsset(Request $request)
   {
        $id = $request->id ;
        $assetlists = AssetList::where('AL_id',$id)->get();
        return response()->json(['assetlists' => $assetlists ]);
   }

   function deleteAsset(Request $request)
   {
        $del_id = $request->del_id;

        $upData = array('AL_status' => 1);
        AssetList::where('AL_id',$del_id)->update($upData);

        return redirect('/assetList')->with('success','AssetList Deleted Successfully');
   }

   function manageSpareParts()
   {
       $spareparts = spareparts::select('wcategories.*','operators.*','spareparts.*','spareparts.created_at as created_on')->join('wcategories','wcategories.C_id','=','spareparts.SP_cateId')->leftJoin('operators','operators.OP_id','=','spareparts.SP_operator')->orderBy('spareparts.SP_id','desc')->get();
        $categories = wcategories::where('C_status',0)->orderBy('C_id','desc')->get();
        $operators = operators::where('OP_status',0)->get();
        $sparepartsNames = spareparts::select('SP_no')->distinct()->get();

       return view('Workshop.spareparts')->with(['spareparts' => $spareparts ,'categories' => $categories , 'operators' => $operators , 'sparepartsNames' => $sparepartsNames ]);
   }

   function addSpareParts(Request $request)
   {
       $category = $request->as_category_id;
       $model = $request->as_model_no;
       $sparepartno = $request->as_spare_part_no;
       $description = $request->as_description;
       $qty = $request->as_stock_qty;
       $price = $request->as_stock_unit_cost;
       $total = $request->as_stock_total_cost;

       $lastQty = spareparts::where('SP_no',$sparepartno)->limit(1)->value('SP_currQty');
       if(empty($lastQty))
       {
           $lastQty = 0 ;
       }
       $currQty = $lastQty + $qty ;
       
       $insData = array(
                        'SP_cateId' => $category,
                        'SP_modelNo' => $model,
                        'SP_no' => $sparepartno,
                        'SP_desc' => $description,
                        'SP_qty' => $qty,
                        'SP_unitPrice' => $price,
                        'SP_costPrice' => $total,
                        'SP_process' => 1,
                        'SP_plotId' => 0 ,
                        'SP_prevQty' => $lastQty ,
                        'SP_currQty' => $currQty,
                        'created_at' => $this->getDate(),
                        'updated_at' => $this->getDate()
                    );

        spareparts::insert($insData);
        
        return redirect('/spareParts')->with('success','Spareparts added Successfully');

   }

   function utiliseSpareParts(Request $request)
   {
        $category = $request->us_category_id;
        $model = $request->us_model_no;
        $sparepartno = $request->us_spare_part_no;
        $description = $request->us_description;
        $operator = $request->us_operator;
        $plots = $request->us_plots;
        $no_of_plots = count($plots);
        $qty = $request->us_stock_qty;
        $each_plot_quantity = (int)$qty / $no_of_plots ;
        $price = $request->us_stock_unit_cost;
        $total = $each_plot_quantity * $price;
        $login_user = $request->us_login_user_id ;

        $lastQty = spareparts::where('SP_no',$sparepartno)->limit(1)->value('SP_currQty');
        
        if($lastQty > $qty)
        {
            $currQty = $lastQty - $each_plot_quantity ;
            if(!empty($plots))
            {
                for($i = 0 ; $i < count($plots) ; $i++)
                {
                    $insData = array(
                        'SP_cateId' => $category,
                        'SP_modelNo' => $model,
                        'SP_no' => $sparepartno,
                        'SP_desc' => $description,
                        'SP_qty' => $each_plot_quantity,
                        'SP_unitPrice' => $price,
                        'SP_costPrice' => $total,
                        'SP_process' => 2, 
                        'SP_plotId' => $plots[$i],
                        'SP_prevQty' => $lastQty ,
                        'SP_currQty' => $currQty,
                        'SP_operator' => $operator,
                        'created_at' => $this->getDate(),
                        'updated_at' => $this->getDate()
                    );

                    spareparts::insert($insData);

                    $field_unique_id = field_data::where('item_field',$i)->where('field_status',0)->value('field_unique_id');
                    if(empty($field_unique_id)){ $field_unique_id = 'Nil' ; }

                    $last_inv_ref_id = inventories::orderBy('id','desc')->value('inv_ref_id');

                    $inventory_Data = array(
                        'inv_ref_id' => $last_inv_ref_id + 1 ,
                        'stock_category' => 0,
                        'stock_item' => 0,
                        'inv_status' => 0,
                        'inv_desc' => 'Sparepart - '.$sparepartno,
                        'stock_qty_new' => $each_plot_quantity ,
                        'field_unique_id' => $field_unique_id ,
                        'stock_process' => 5,
                        'added_by' => $login_user,
                        'stock_qty_prev' => $lastQty ,
                        'stock_qty_curr' => $currQty ,
                        'stock_unit_cost' =>  $price,
                        'stock_total_cost' => $total,
                        'stock_field_id' =>  $plots[$i] ,
                        'created_at' => $this->getDate(),
                        'updated_at' => $this->getDate()
                    );
    
                    inventories::insert($inventory_Data);
                }
            }
        }
        else{
            return redirect('/spareParts')->with('error','No stock Available');
        }
       
        
        return redirect('/spareParts')->with('success','Spareparts Utilised Successfully');
   }

   function getSPValues(Request $request)
   {
        $value = $request->id;
        $spareparts = spareparts::where('SP_no',$value)->orderBy('SP_id','desc')->limit(1)->get();
    
        return response()->json([ 'spareparts' => $spareparts ]);
   }

   public function dateFormat($date){
    $exp_date = explode('-',$date);
    $new_d = $exp_date[2].'-'.$exp_date[1].'-'.$exp_date[0] ;
    return $new_d ;
}

   function filterSPValues(Request $request)
   {
             //getting values from the post 
             $from_date = $request->from_d;
             $to_date = $request->to_d;
             $stock_category = $request->stock_category;
             $plot_id = $request->plot_id;
             $process = $request->process_id;
             $onlyShowValue = $request->onlyShowValue;
      
             if($request->filter == 'Filter')
             {
                $spareparts = spareparts::select('wcategories.*','operators.*','spareparts.*','spareparts.created_at as created_on')->join('wcategories','wcategories.C_id','=','spareparts.SP_cateId')->leftJoin('operators','operators.OP_id','=','spareparts.SP_operator');
               
                 if($from_date && $to_date)
                 {
                     $from_d = $this->dateFormat($from_date). ' 00:00:00' ;
                     $to_d = $this->dateFormat($to_date).' 23:59:59';
                     //return $from_d.' '.$to_d ;
                     $spareparts->where('spareparts.created_at', '>=' , $from_d)->where('spareparts.created_at', '<=' ,$to_d); 
                 }
     
                 if($stock_category)
                 {
                      $spareparts->where('spareparts.SP_cateId',$stock_category);
                 }
     
                 if($plot_id)
                 {
                      $spareparts->where('spareparts.SP_plotId',$plot_id);
                 }
     
                 if($process)
                 {
                      
                      if($process == 2)
                      {
                         $spareparts->where('spareparts.SP_process','<>',1);
                      }
                      else
                      {
                         $spareparts->where('spareparts.SP_process',$process);
                      }
                 }
     
                 if($onlyShowValue)
                 {
                     if($onlyShowValue == 500)
                     {
                         $spareparts->limit(500);
                     }
                     else if($onlyShowValue == 200)
                     {
                         $spareparts->limit(200);
                     }
                     else if($onlyShowValue == 100)
                     {
                         $spareparts->limit(100);
                     }
                     else if($onlyShowValue == 50)
                     {
                         $spareparts->limit(50);
                     }
                    
                 }
     
                 $spareparts = $spareparts->orderBy('spareparts.SP_id','desc')->get();

     
                 $categories = wcategories::where('C_status',0)->orderBy('C_id','desc')->get();
                 $operators = operators::where('OP_status',0)->get();
                 $sparepartsNames = spareparts::select('SP_no')->distinct()->get();
 
                 $selected = array( 'from_date' => $from_date , 'to_date' => $to_date , 'stock_category' => $stock_category , 'plot_id' => $plot_id , 'process' => $process , 'show' =>$onlyShowValue);
     
                 return view('Workshop.spareparts')->with(['spareparts' => $spareparts, 'categories' => $categories, 'operators' => $operators ,'sparepartsNames' => $sparepartsNames ,  'selected' => $selected ]);
             }
             else if($request->reset == 'Reset')
             {
                 return redirect('/spareParts');
             }
   }

   function filterSPCategory($id)
   {
    $spareparts = spareparts::select('wcategories.*','operators.*','spareparts.*','spareparts.created_at as created_on')->join('wcategories','wcategories.C_id','=','spareparts.SP_cateId')->leftJoin('operators','operators.OP_id','=','spareparts.SP_operator')->where('SP_cateId',$id)->orderBy('spareparts.SP_id','desc')->get();

    return view('Workshop.sparepartCategory')->with(['spareparts'=> $spareparts ]);
   }

   function makeWCategoryAsc(Request $request)
   {    
    $sparepartstock = spareparts::leftJoin('wcategories','wcategories.C_id','=','spareparts.SP_cateId')->select('SP_no','spareparts.*','wcategories.*','spareparts.SP_currQty as stock')->orderBy('SP_id','desc')->orderBy('wcategories.C_name','asc')->get()->unique('SP_no');

    return view('Workshop.viewstock')->with(['spareparts'=> $sparepartstock ]);
   }

   function makeSpareAsc(Request $request)
   {
        $sparepartstock = spareparts::leftJoin('wcategories','wcategories.C_id','=','spareparts.SP_cateId')->select('SP_no','spareparts.*','wcategories.*','spareparts.SP_currQty as stock')->orderBy('SP_id','desc')->orderBy('SP_name','asc')->get()->unique('SP_no');

        return view('Workshop.viewstock')->with(['spareparts'=> $sparepartstock ]);
   }

   function viewWorkshopStock()
   {
      $categories = wcategories::where('C_status',0)->orderBy('C_id','desc')->get();

      $sparepartstock = spareparts::leftJoin('wcategories','wcategories.C_id','=','spareparts.SP_cateId')->select('SP_no','spareparts.*','wcategories.*','spareparts.SP_currQty as stock')->orderBy('SP_id','desc')->get()->unique('SP_no');

       return view('Workshop.viewstock')->with(['spareparts'=> $sparepartstock,'categories' => $categories]);
   }

   

   function manageCategory()
   {
       $categories = wcategories::where('C_status',0)->orderBy('C_id','desc')->get();
       return view('Workshop.categories')->with(['categories'=>$categories]);
   }

   function addCategory(Request $request)
   {
       $name = $request->work_category ;
       $action = $request->submit;

       $insData = array( 'C_name' => $name);

       if($action == 'Add')
       {
           $insData['created_at'] = $this->getDate();
            wcategories::insert($insData);
       }
       elseif($action == 'Update')
       {
           $cate_id = $request->category_id ;
           $insData['updated_at'] = $this->getDate();
           wcategories::where('C_id',$cate_id)->update($insData);
       }
      
       return redirect('/manageCategory')->with('success','Category Added Successfully');
   }

   function deleteCategory(Request $request)
   {
        $del_id = $request->del_id;

        $upData = array('C_status' => 1);
        wcategories::where('C_id',$del_id)->update($upData);

        return redirect('/manageCategory')->with('success','Category Deleted Successfully');
   }

   function manageOperator()
   {
       $operators = operators::where('OP_status',0)->orderBy('OP_id','desc')->get();
       return view('Workshop.operators')->with(['operators' => $operators]);
   }

   function addOperator(Request $request)
   {
        $fname = $request->op_fname ;
        $lname = $request->op_lname ;
        $phone = $request->op_phone;
        $dl = $request->op_drivelicence;
        $address =  $request->op_address;

        $action = $request->submit;

        $insData = array( 'OP_fname' => $fname , 'OP_lname' => $lname , 'OP_phoneNo' => $phone , 'OP_driveLicence' => $dl, 'OP_Address' => $address );

        if($action == 'Add')
        {
            $insData['created_at'] = $this->getDate();
            operators::insert($insData);
        }
        elseif($action == 'Update')
        {
            $op_id = $request->operator_id ;
            $insData['updated_at'] = $this->getDate();
            operators::where('OP_id',$op_id)->update($insData);
        }
    
        return redirect('/manageOperator')->with('success','Category Added Successfully');
   }

   public function getOperator(Request $request)
   {
        $id = $request->id ;
        $operators = operators::where('OP_id',$id)->get();
        return response()->json(['operators' => $operators ]);
   }

   public function deleteOperator(Request $request)
   {
        $del_id = $request->del_id;

        $upData = array('OP_status' => 1);
        operators::where('OP_id',$del_id)->update($upData);

        return redirect('/manageOperator')->with('success','Operator Deleted Successfully');
   }

   public function getDate(){
    $tz = 'Asia/Dubai'; // your required location time zone.
     $timestamp = time();
     $dt = new \DateTime("now", new \DateTimeZone($tz)); //first argument "must" be a string
     $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
     return $dt->format('Y-m-d H:i:s');
}

   
}
