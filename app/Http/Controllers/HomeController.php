<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use App\Menuprivilege;
use App\field_data;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $percentages = array();
        for($i = 1 ; $i <= 67 ; $i++){
            $total = field_data::where('item_field',$i)->where('item_status',3)->where('field_status',0)->count();   
            if($total != 0){
                $percent = round( ($total / 24) * 100 );    
            }
            else{
                $percent = 0 ;
            }
            array_push($percentages,$percent);
        }

        array_unshift($percentages , 0);

        $gc_array = DB::table('inventories')->where('inv_status',0)->where('stock_process','!=',4)->get();

        $user_id = Auth::user()->id;
        
        $previlage = Menuprivilege::where('user_id', 1)->find('privilege_id');
        if($previlage != null)
        {
           Session::put('privilege', $previlage);
        }
        else{
            $prevData = array('user_id' => $user_id , 'privilege_id' => 1);
            DB::table('menuprivileges')->insert($prevData);
            Session::put('privilege', $previlage);
        }
        return view('Pages.dashboard')->with(['percent' => $percentages, 'generalCostings' => $gc_array ]);
    }
}
