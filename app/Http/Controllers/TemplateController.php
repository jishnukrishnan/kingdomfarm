<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use App\templates ;
use App\Event ;
use App\stock_items;
use App\GeneralCosting;
use App\field_data;
use App\Menuprivilege;
use App\inventories;

class TemplateController extends Controller
{
  
    public function store(Request $request)
    {
        $template_name = $request->template_name; 
        $start_day = $request->start_day;
        $end_day = $request->end_dayv;
        $item_qty = $request->item_qty;
        $item_category = $request->item_category;
        $item_stock = $request->item_stock;
        $item_status  = $request->item_status; 
        $login_user = $request->login_user_id ;

        $exists = templates::where('template_name',$template_name)->first();

        if($request->submit == "Save"){ 
            if(!$exists){
                for($i = 0 ; $i < 24 ; $i++){

                        $start_date[$i] = $this->dateFormat($start_day[$i]);
                        $end_date[$i] = $this->dateFormat($end_day[$i]);

                        if(empty($item_qty[$i]))
                        {
                            $item_qty[$i] = 0 ;
                        }

                        if(empty($item_category[$i]))
                        {
                            $item_category[$i] = 0 ;
                        }

                        if(empty($item_stock[$i]))
                        {
                            $item_stock[$i] = 0 ;
                        }

                        if(empty($item_status[$i]))
                        {
                            $item_status[$i] = 0 ;
                        }


                        $ins_data = array(
                            'plot_desc_id' => $i + 1,
                            'item_status_id' => $item_status[$i],
                            'template_status' => 0,
                            'start_date' => $start_date[$i], 
                            'end_date' => $end_date[$i],
                            'item_qty' => $item_qty[$i],
                            'item_ledger_id' => $item_category[$i],
                            'item_stock_id' => $item_stock[$i],
                            'created_at' => $this->getDate() ,
                            'updated_at' => $this->getDate(),
                            'template_name' => $template_name
                        );             

                        templates::insert($ins_data);
                    }

                    return redirect("/plot")->with('success', 'Template Added Successfully');
                }
                else{
                    return redirect("/plot")->with('error', 'Template with Same Name Exists');
                }
            }

            if($request->submit == "Update")
            {
                $template_id = $request->template_id ;
                for($i = 0 ; $i < 24 ; $i++){

                    $start_date[$i] = $this->dateFormat($start_day[$i]);
                    $end_date[$i] = $this->dateFormat($end_day[$i]);

                    if(empty($item_qty[$i]))
                    {
                        $item_qty[$i] = 0 ;
                    }

                    if(empty($item_category[$i]))
                    {
                        $item_category[$i] = 0 ;
                    }

                    if(empty($item_stock[$i]))
                    {
                        $item_stock[$i] = 0 ;
                    }

                    if(empty($item_status[$i]))
                    {
                        $item_status[$i] = 0 ;
                    }

                    $ins_data = array(
                        'plot_desc_id' => $i + 1,
                        'item_status_id' => $item_status[$i],
                        'start_date' => $start_date[$i], 
                        'end_date' => $end_date[$i],
                        'item_qty' => $item_qty[$i],
                        'item_ledger_id' => $item_category[$i],
                        'item_stock_id' => $item_stock[$i],
                        'created_at' => $this->getDate() ,
                        'updated_at' => $this->getDate(),
                        'template_name' => $template_name
                    );             
                    templates::where('id',$template_id[$i])->update($ins_data);
                    
                }

                return redirect("/viewSinglePlot/$template_name")->with('success', 'Template Updated Successfully');
            }
   

    }

    public function getDate(){
        $tz = 'Asia/Dubai'; // your required location time zone.
         $timestamp = time();
         $dt = new \DateTime("now", new \DateTimeZone($tz)); //first argument "must" be a string
         $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
         return $dt->format('Y-m-d H:i:s');
    }

    public function dateFormat($date){
        $exp_date = explode('-',$date);
        $new_d = $exp_date[2].'-'.$exp_date[1].'-'.$exp_date[0] ;
        return $new_d ;
    }


    public function check_exists(Request $request)
    {
        $template_name = $request->name;
        $exists = templates::where('template_name',$template_name)->where('template_status',0)->first();
        if($exists) 
        {
            return response()->json(['msg' => 'exists']);
        }
        else
        {
            return response()->json(['msg' => 'notexists']);
        }
    }

    public function fetch_template(Request $request){
        $template_name = $request->name;
        $id = $request->field ;
        $fd_up = array('field_status' => 2);
        DB::table('field_datas')->where('item_field', $id)->update($fd_up);
        $ev_up = array('event_status' => 2);
        DB::table('events')->where('field_id', $id)->update($ev_up);
        $ef_up = array('ef_status' => 2);
        DB::table('extra_field_descs')->where('field_id', $id)->update($ef_up);

        $templateDetails = templates::where('template_name',$template_name)
                            ->join('plot_datas','plot_datas.id','=','templates.plot_desc_id')
                            ->get();
        return response()->json(['templates' => $templateDetails ]);
    }


    public function delete_template(Request $request){
        $template_name = $request->del_id ;

        $del_arr = array('template_status' => 1);
        templates::where('template_name',$template_name)->update($del_arr);

        return redirect('/viewplot')->with('success','Template Deleted Successfully');    
    }

    public function filter_inventory($id){

        $inventories = DB::table('inventories')
        ->select('inventories.inv_ref_id','inventories.id','stock_process','stock_categories.category_name','item_name','inventories.created_at','stock_qty_prev','stock_qty_new','stock_qty_curr','stock_unit_cost','stock_total_cost','stock_field_id','unit_name')
        ->join('stock_items','stock_items.id','=','inventories.stock_item') 
        ->join('stock_categories','stock_categories.id','=','inventories.stock_category')   
        ->join('units','units.id','=','stock_items.stock_unit')
        ->where('stock_category',$id)
        ->orderBy('inventories.id','desc')
        ->get();

        $category_name = DB::table('stock_categories')->select('category_name as name')->where('id',$id)->get();

        return view('Pages.inventoryCategory')->with(['inventories'=> $inventories , 'category_name' => $category_name]);
    }

    public function generalCosting()
    {
        $gencost = inventories::where('inv_status',0)->where('stock_process',3)->orderBy('id','desc')->get();
        return view('Pages.generalCosting')->with(['gencost' => $gencost]);
    }

    public function insertGenCost(Request $req)
    {
        $amount = $req->gc_amount;
        $desc = $req->gc_desc;
        $plots = $req->gc_plots;
        $user_id = $req->login_user_id;
        $plot = '';
        $gc_all_plots = $req->gc_all_plots ;
        $login_user = $req->us_login_user_id ;


         $action = $req->submit;

        /* if($gc_all_plots == 'on')
            {
            $amount_per = round($amount/67,2) ;
            for($k = 1;$k <= 66;$k++)
            { 
                $field_unique_id = field_data::where('item_field',$k)->where('field_status',0)->value('field_unique_id');
                if(empty($field_unique_id)){ $field_unique_id = 'Nil' ; }
                $ins_data['gc_plots'] = $k;
                $ins_data['gc_amount'] = $amount_per;
                $ins_data['field_unique_id'] = $field_unique_id ;
                if($action == 'Add')
                {
                    DB::table('general_costings')->insert($ins_data);
                    $message = 'General Costing Added Successfully';
                }
                else if($action == "Update")
                {
                    $id = $req->gc_id;
                    DB::table('general_costings')->where('id',$id)->update($ins_data);
                    $message = 'General Costing Updated Successfully';
                }
            }
        }
        else
        { */

            // if($action == "Update")
            // {
        

            //     $inv_id = DB::table('general_costings')->where('id',$id)->value('gc_ref_id');

            //     $inv_up_data = array(
            //         'inv_status' => 1
            //     );
            //     DB::table('inventories')->where('id',$inv_id)->update($inv_up_data);
            //     DB::table('general_costings')->where('id',$id)->update($up_data);
            // }

            $amount = str_replace(',','',$amount) ;

            $amount_per = round($amount/count($plots),2);

            $last_inv_ref_id = inventories::orderBy('id','desc')->value('inv_ref_id');

            for($i = 0;$i < count($plots); $i++)
            {
                $field_unique_id = field_data::where('item_field',$i)->where('field_status',0)->value('field_unique_id');
                if(empty($field_unique_id)){ $field_unique_id = 'Nil' ; }
                $ins_data['gc_plots'] = $plots[$i];
                $ins_data['gc_amount'] = $amount_per;
                $ins_data['field_unique_id'] = $field_unique_id ;




                $inventory_Data = array(
                    'inv_ref_id' => $last_inv_ref_id + 1 ,
                    'stock_category' => 0,
                    'stock_item' => 0,
                    'inv_status' => 0,
                    'inv_desc' => $desc ,
                    'stock_qty_new' => 0 ,
                    'field_unique_id' => $field_unique_id ,
                    'stock_process' => 3,
                    'added_by' => $login_user,
                    'stock_qty_prev' => 0 ,
                    'stock_qty_curr' => 0 ,
                    'stock_unit_cost' => 0,
                    'stock_total_cost' => $amount_per,
                    'stock_field_id' =>  $plots[$i] ,
                    'created_at' => $this->getDate(),
                    'updated_at' => $this->getDate()
                );

                    $inventory_id = DB::table('inventories')->insertGetId($inventory_Data);

                    $message = 'General Costing Added Successfully';
               
            }
      //  }
        

        return redirect('/genCosting')->with('success' , $message);
        
    }

    public function getGCItem(Request $req)
    {
        $id = $req->id;
        $generalcost = DB::table('general_costings')
                        ->where('general_costings.id',$id)->get();
        return response()->json(['generalcost' => $generalcost]);
    }

    public function deleteGC(Request $request)
    {
        $id = $request->del_id ;


         $inv_up_data = array(
             'inv_status' => 1
         );
         DB::table('inventories')->where('id',$inv_id)->update($inv_up_data);

        return redirect('/genCosting')->with('success','General Costing Deleted Successfully'); 
    }

    public function viewCalender()
    {
        $events = [];
        $data = DB::table('events')
                    ->join('plot_datas','events.plot_desc_id','=','plot_datas.id')
                    ->where('event_status',0)
                    ->get();
        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    'Plot '.$value->field_id.' - '.$value->description ,
                    true,
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date.' +1 day'),
                    null,
                    // $value->description,
                    // $value->field_id,
                    // Add color and link on event
                 [
                     'color' => '#84DCCF',
                     'url' => '/fields/'.$value->field_id,
                 ]
                );
            }
        }

        $calendar = Calendar::addEvents($events);

        return view('Pages.calendar')->with('calendar' , $calendar);

    }


    public function viewUsers()
    {
        $users = DB::table('users')
                ->paginate(15);
        return view('Pages.users')->with('users', $users);
    }

    public function updatePrivileges(Request $req)
    {
        $id = $req->user_id;
        $prev_id = $req->privilege_id ;   
        $prevData = array('privilege_id' => $prev_id); 
        DB::table('menuprivileges')->where('user_id',$id)->update($prevData);
        return redirect("/users")->with('success', 'Privilege Updated Successfully');      
    }

    public function plotDetails(Request $request)
    {
        $total = field_data::where('item_field',$plot)->where('item_status',3)->where('field_status',0)->count();

        if($total != 0){
            $percent = round( ($total / 24) * 100 ) ;     
        }
        else{
            $percent = 0 ;
        }

        $gc_amount = DB::table('general_costings')->where('gc_plots',$plot)->where('gc_process', 1)->where('gc_status',0)->sum('gc_amount');
       
        return response()->json(['gc_amount' => $gc_amount, 'completed' => $percent ]);
    }

    public function assignedPrivilege(Request $request) {
        $user_id = $request->id;

        $privilege = Menuprivilege::where('user_id',$user_id)->first();

        return response()->json(['privilege' => $privilege]);
    }

    public function getAvailItem(Request $request)
    {
        $id = $request->id ;
        $qty = DB::table('stock_items')
        ->where('id',$id)
        ->value('stock_qty') ;

        return response()->json(['qty' => $qty]);
    }

    public function viewStockItem()
    {   
        $stock_items = DB::table('stock_items')
        ->select('stock_items.*','stock_categories.id as cate_id','stock_categories.category_name','units.id as unit_id','units.unit_name')
        ->join('stock_categories','stock_categories.id','=','stock_items.category_name')   
        ->join('units','units.id','=','stock_items.stock_unit')
        ->where('item_status',0)
        ->orderby('stock_items.id','desc')->get();

        return view('Pages.viewStock')->with('stock_items', $stock_items);
    }

    public function makeCategoryAsc(Request $request)
    {   
        $stock_items = DB::table('stock_items')
        ->select('stock_items.*','stock_categories.id as cate_id','stock_categories.category_name','units.id as unit_id','units.unit_name')
        ->join('stock_categories','stock_categories.id','=','stock_items.category_name')   
        ->join('units','units.id','=','stock_items.stock_unit')
        ->where('item_status',0)
        ->orderby('stock_categories.category_name','asc')->paginate(50);

        return view('Pages.viewStock')->with('stock_items', $stock_items);
    }

    public function makeItemAsc(Request $request)
    {   
        $stock_items = DB::table('stock_items')
        ->select('stock_items.*','stock_categories.id as cate_id','stock_categories.category_name','units.id as unit_id','units.unit_name')
        ->join('stock_categories','stock_categories.id','=','stock_items.category_name')   
        ->join('units','units.id','=','stock_items.stock_unit')
        ->where('item_status',0)
        ->orderby('stock_items.item_name','asc')->paginate(50);

        return view('Pages.viewStock')->with('stock_items', $stock_items);
    }

    public function Audits()
    {
        $audits = DB::table('inventories')
        ->select('inventories.inv_ref_id','inventories.inv_desc','inventories.id','stock_process','stock_categories.category_name','item_name','inventories.created_at','stock_qty_prev','stock_qty_new','stock_qty_curr','stock_unit_cost','stock_total_cost','stock_field_id','unit_name')
        ->leftJoin('stock_items','stock_items.id','=','inventories.stock_item') 
        ->leftJoin('stock_categories','stock_categories.id','=','inventories.stock_category')   
        ->leftJoin('units','units.id','=','stock_items.stock_unit')
        ->where('stock_process','!=',4)
        //->where('inv_status',0)
        ->orderby('inventories.id','desc')
        ->get();

        $stock_items = stock_items::orderby('item_name','asc')->where('item_status',0)->get();

        return view('Pages.audit')->with(['audits' => $audits, 'stock_items' => $stock_items , 'selected' => '']);
    }

    public function filterAudit(Request $request)
    {
        //getting values from the post 
        $from_date = $request->from_d;
        $to_date = $request->to_d;
        $stock_item = $request->stock_item;
        $plot_id = $request->plot_id;
        $process = $request->process_id;
        $onlyShowValue = $request->onlyShowValue;
 
        if($request->filter == 'Filter')
        {
            $audits = DB::table('inventories')
                ->select('inventories.inv_ref_id','inventories.inv_desc','inventories.id','stock_process','stock_categories.category_name','item_name','inventories.created_at','stock_qty_prev','stock_qty_new','stock_qty_curr','stock_unit_cost','stock_total_cost','stock_field_id','unit_name')
                ->leftJoin('stock_items','stock_items.id','=','inventories.stock_item')
                ->leftJoin('stock_categories','stock_categories.id','=','inventories.stock_category')  
                ->leftJoin('units','units.id','=','stock_items.stock_unit');
               

            if($from_date && $to_date)
            {
                $from_d = $this->dateFormat($from_date). ' 00:00:00' ;
                $to_d = $this->dateFormat($to_date).' 23:59:59';
                //return $from_d.' '.$to_d ;
                $audits->where('inventories.created_at', '>=' , $from_d)->where('inventories.created_at', '<=' ,$to_d); 
            }

            if($stock_item)
            {
                 $audits->where('inventories.stock_item',$stock_item);
            }

            if($plot_id)
            {
                 $audits->where('inventories.stock_field_id',$plot_id);
            }

            if($process)
            {
                 
                 if($process == 2)
                 {
                    $audits->where('inventories.stock_process','<>',1);
                 }
                 else
                 {
                    $audits->where('inventories.stock_process',$process);
                 }
            }

            if($onlyShowValue)
            {
                if($onlyShowValue == 500)
                {
                    $audits->limit(500);
                }
                else if($onlyShowValue == 200)
                {
                    $audits->limit(200);
                }
                else if($onlyShowValue == 100)
                {
                    $audits->limit(100);
                }
                else if($onlyShowValue == 50)
                {
                    $audits->limit(50);
                }
               
            }

            $audits = $audits ->where('stock_process','!=',4)
           //->where('inv_status',0)
            ->orderby('inventories.id','desc')->get();

            //return $audits ;

            $stock_items = stock_items::orderby('item_name','asc')->get();

            $selected = array( 'from_date' => $from_date , 'to_date' => $to_date , 'stock_item' => $stock_item , 'plot_id' => $plot_id , 'process' => $process , 'show' =>$onlyShowValue);

            return view('Pages.audit')->with(['audits' => $audits, 'stock_items' => $stock_items, 'selected' => $selected]);
        }
        else if($request->reset == 'Reset')
        {
            return redirect('/audits');
        }
    }

   
    public function oldPlotDatas()
    {
        $plotdatas = DB::table('field_datas')
                    ->where('field_status',2)
                    ->orderBy('id','desc')
                    ->groupBy('field_unique_id')                
                    ->get();


        return view('Pages.oldPlots')->with(['plotdatas'=> $plotdatas]);
    }

    public function viewSingleOldPlotData($id)
    {
       
        $categories = DB::table('stock_categories')->where('category_status',0)->orderby('category_name','asc')->get();
        $items = stock_items::where('item_status',0)->orderby('item_name','asc')->get();
        $status = DB::table('statuses')->where('status_status',0)->orderby('id','asc')->get();
       // $item_values = field_data::where('item_field',$id)->orderby('id','desc')->get();
        $item_values = DB::table('field_datas')
                        //->select('users.id','users.name','profiles.photo')
                        ->join('stock_categories','stock_categories.id','=','field_datas.item_category')
                        ->join('stock_items','stock_items.id','=','field_datas.item_stock')
                        ->join('statuses','statuses.id','=','field_datas.item_status')
                        ->where('field_unique_id',$id)
                        ->get();



        $field_datas = DB::table('field_datas')
                    ->select('field_datas.*','plot_datas.description','plot_datas.start_day','plot_datas.end_day')
                    ->join('plot_datas','plot_datas.id','=','field_datas.items_id')
                    ->where('field_unique_id',$id)->get();

        $extra_field_datas = DB::table('extra_field_descs')
                    ->select('field_datas.*','extra_field_descs.id as sub_id','extra_field_descs.*')
                    ->join('field_datas','field_datas.id','=','extra_field_descs.field_data_id')
                    ->where('field_datas.field_unique_id',$id)->get();

        $gencost = inventories::where('field_unique_id',$id)->get();

        $harvest = inventories::where('field_unique_id',$id)->where('stock_process',4)->value('stock_qty_new');
        

        return view('Pages.singleOldData')->with(['id' => $id , 'extra_field_d' => $extra_field_datas ,'categories' => $categories , 'field_datas'=> $field_datas ,'field_data' => $item_values ,'stock_items' => $items , 'status' => $status, 'gc_datas' => $gencost , 'harvest' => $harvest ]);   
    }

    public function filtergclist(Request $request)
    {
        $showOnlyValue = $request->show;

        $gencost = inventories::where('inv_status',0)->where('stock_process',3);

        if($showOnlyValue == 500)
        {
            $gencost->limit(500);
        }
        else if($showOnlyValue == 200)
        {
            $gencost->limit(200);
        }
        else if($showOnlyValue == 100)
        {
            $gencost->limit(100);
        }
        else if($showOnlyValue == 50)
        {
            $gencost->limit(50);
        }

        $selected = array('show' => $showOnlyValue );

        $gencost = $gencost->orderby('id','desc')->get();
       
        return view('Pages.generalCosting')->with(['gencost' => $gencost, 'selected' => $selected]);
       
    }
}
