<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Status;
use App\stock_items;
use App\stock_categories;
use App\field_data;
use App\units;
use App\inventories;
use App\plot_data;
use App\templates;
use App\GeneralCosting ;
use App\Menuprivilege ;
use Auth ;

class PagesController extends Controller
{
    public function index(){
        return view('layouts.main');
       // return false;
    }

    public function dashboard(){
        $percentages = array();
        for($i = 1 ; $i <= 67 ; $i++){
            $total = field_data::where('item_field',$i)->where('item_status',3)->where('field_status',0)->count();   
            if($total != 0){
                $percent = round( ($total / 24) * 100 );    
            }
            else{
                $percent = 0 ;
            }
            array_push($percentages,$percent);
        }

        array_unshift($percentages , 0);

        $gc_array = DB::table('inventories')->where('inv_status',0)->where('stock_process','!=',4)->get();

        $user_id = Auth::user()->id;

        $previlage = Menuprivilege::where('user_id', 1)->find('privilege_id');

        if($previlage != null)
        {
           Session::put('privilege', $previlage);
        }
        else{
            $prevData = array('user_id' => $user_id , 'privilege_id' => 1);
            DB::table('menuprivileges')->insert($prevData);
            Session::put('privilege', $previlage);
        }

        return view('Pages.dashboard')->with(['percent' => $percentages, 'generalCostings' => $gc_array ]);
    }

    public function status(Request $req){
    
        $status = Status::where('status_status',0)->get();
        return view('Pages.status')->with(['status' => $status]);
    }


    function insertStatus(Request $req){

        $action_name = $req->submit;

        $stat_name = $req->status_name;

        if($action_name == 'Add')
        {

            $data = array( 'status' => $stat_name ,'status_status' => 0 , 'created_at' => $this->getDate() , 'updated_at' => $this->getDate());

            DB::table('statuses')->insert($data);

            return redirect('/status')->with('success', 'Status Inserted Successfully');
        }
        else if($action_name == 'Update')         
        {
            $id = $req->status_id ;
            $data = array( 'status' => $stat_name , 'updated_at' => date("Y-m-d H:i:s"));
            DB::table('statuses')->where('id',$id )->update($data);

            return redirect('/status')->with('success', 'Status Updated Successfully');
        }
      
    }

    function deleteStatus(Request $req)
    {
        $id = $req->del_id;

        $del_arr = array('status_status' => 1);

        DB::table('statuses')->where('id', $id)->update($del_arr);
    
        return redirect('/status')->with('success', 'Status Deleted Successfully');
    }

    public function viewStockItem() {
        $categories = DB::table('stock_categories')->where('category_status',0)->get();
        $stock_items = DB::table('stock_items')
                        ->select('stock_items.*','stock_categories.id as cate_id','stock_categories.category_name','units.id as unit_id','units.unit_name')
                        ->join('stock_categories','stock_categories.id','=','stock_items.category_name')   
                        ->join('units','units.id','=','stock_items.stock_unit')
                        ->where('item_status',0)
                        ->orderby('stock_items.id','desc')->get();
        $units = DB::table('units')->where('unit_status',0)->get();
        return view('Pages.stockItem')->with(['categories' => $categories , 'stock_items' => $stock_items ,'units' => $units]);
    }

    public function addStockItem(Request $req){

        // requesting values
        $action_name = $req->submit;
        $stock_category = $req->stock_category;
        $stock_name = str_replace('-','_',$req->stock_name);     
        $stock_price = $req->stock_price ;
        $unit_id = $req->stock_unit;
        $user_id  = $req->login_user_id; // requesting login_user_id

        if($action_name == 'Add')
        {

            $data = array( 'category_name' => $stock_category , 'item_name' => $stock_name.'-'.$stock_price , 'created_at' => $this->getDate(), 'stock_unit' => $unit_id , 'stock_qty' => 0 , 'item_status' => 0 , 'added_by' => $user_id , 'updated_by' => $user_id);
            DB::table('stock_items')->insert($data);
            return redirect('/stock_items')->with('success', 'Stock Item Added Successfully');
        }
        else if($action_name == 'Update')         
        {
            $id = $req->stock_item_id ;
            $data = array( 'category_name' => $stock_category , 'item_name' => $stock_name.'-'.$stock_price , 'stock_unit' => $unit_id , 'updated_at' => date("Y-m-d H:i:s"));
            DB::table('stock_items')->where('id',$id )->update($data);
            return redirect('/stock_items')->with('success', 'Stock Item Updated Successfully');
        }
        
    }

    public function delStockItem(Request $req){
        $id = $req->del_id;
        $del_arr = array('item_status'=>1);
        DB::table('stock_items')->where('id', $id)->update($del_arr); 
        return redirect('/stock_items')->with('success', 'Stock Item Deleted Successfully');
    }

    public function viewStockCategory() {
        $categories = DB::table('stock_categories')->where('category_status',0)->orderBy('id','desc')->get();
        return view('Pages.stock_category')->with('categories',$categories);
    }

    public function addStockCategory(Request $req){
         // requesting values
         $action_name = $req->submit;
         $stock_category = $req->stock_category; 
         $user_id  = $req->login_user_id; // requesting login_user_id
 
         if($action_name == 'Add')
         {
             $data = array( 'category_name' => $stock_category ,'category_status' => 0 , 'created_at' => $this->getDate() , 'added_by' => $user_id , 'updated_by' => $user_id );
 
             DB::table('stock_categories')->insert($data);
             return redirect('/stock_category')->with('success', 'Stock Category Added Successfully');
         }
         else if($action_name == 'Update')         
         {
             $id = $req->stock_category_id ;
             $data = array( 'category_name' => $stock_category ,  'updated_at' => date("Y-m-d H:i:s"));
             DB::table('stock_categories')->where('id',$id )->update($data);
             return redirect('/stock_category')->with('success', 'Stock Category Updated Successfully');
         }
         
      
    }

    public function delStockCategory(Request $req)
    {
        $id = $req->del_id;
        $del_arr = array('category_status' => 1);
        DB::table('stock_categories')->where('id', $id)->update($del_arr);
    
        return redirect('/stock_category')->with('success', 'Stock Category Deleted Successfully');
    }

    public function viewInventory()
    {
        $categories = stock_categories::orderby('category_name','asc')->where('category_status',0)->get(); 
        $stock_items = stock_items::orderby('item_name','asc')->where('item_status',0)->get();
        
        $inventories = DB::table('inventories')
                        ->select('inventories.inv_ref_id','inventories.id','inventories.stock_process','inventories.stock_category','stock_categories.category_name','item_name','inventories.created_at','stock_qty_prev','stock_qty_new','stock_qty_curr','stock_unit_cost','stock_total_cost','stock_field_id','unit_name')
                        ->leftjoin('stock_items','stock_items.id','=','inventories.stock_item') 
                        ->leftjoin('stock_categories','stock_categories.id','=','inventories.stock_category')   
                        ->leftjoin('units','units.id','=','stock_items.stock_unit')
                        ->orderBy('inventories.id','desc')  
                        ->where('inventories.stock_process',1)
                        ->orWhere('inventories.stock_process',2)
                        ->get();

        return view('Pages.inventory')->with(['inventories'=> $inventories , 'categories' => $categories , 'stock_items' => $stock_items ]);
    }

    public function getDate(){
        $tz = 'Asia/Dubai'; // your required location time zone.
         $timestamp = time();
         $dt = new \DateTime("now", new \DateTimeZone($tz)); //first argument "must" be a string
         $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
         return $dt->format('Y-m-d H:i:s');
    }

    public function addStock(Request $req){
         // requesting values
        $stock_process = $req->as_stock_process;
        $qty = $req->as_stock_qty; 
        $item_id = $req->as_item_id; 
        $category_id = $req->as_category_id; 
        $user_id  = $req->as_login_user_id; // requesting login_user_id
        $stock_unit_cost = $req->as_stock_unit_cost;
        $stock_unit_cost = str_replace(',','',$stock_unit_cost); 
        $stock_total_cost = $req->as_stock_total_cost;

        $stockQty = DB::table('stock_items')->select('stock_qty')->where('id',$item_id)->get();
       
        $prev_qty = $stockQty[0]->stock_qty;

        $new_qty = $prev_qty + $qty ;
   
        $stockCurrQty = inventories::where('stock_item',$item_id)->orderBy('id','desc')->first();

        if(!empty($stockCurrQty))
        {
            $inv_prev_qty = $stockCurrQty->stock_qty_curr;
        }
        else{
            $inv_prev_qty = 0;
        }

        $inv_new_qty = $inv_prev_qty + $qty ;

         $data = array( 'stock_qty' => $new_qty ,  'added_by' => $user_id );

         DB::table('stock_items')->where('id',$item_id )->update($data);

         $last_inv_ref_id = inventories::orderBy('id','desc')->value('inv_ref_id');



         $insData = array(
            'inv_ref_id' => $last_inv_ref_id + 1 ,
            'inv_status' => 0,
            'inv_desc' => 'Stock Added',
            'stock_category' => $category_id,
            'stock_item' => $item_id ,
            'stock_qty_new' => $qty ,
            'stock_process' => $stock_process,
            'added_by' => $user_id,
            'stock_qty_prev' => $inv_prev_qty,
            'stock_qty_curr' => $inv_new_qty,
            'stock_unit_cost' => $stock_unit_cost ,
            'stock_total_cost' => $stock_total_cost,
            'stock_field_id' => 0  ,
            'field_unique_id' => 'Nil',
            'created_at' => $this->getDate()
         );

         $inventories = DB::table('inventories')->insert($insData);


        return redirect('/inventory')->with('success', 'Stock Added Successfully');
        
        
    }

    public function utiliseStock(Request $req)
    {
        $stock_process = $req->us_stock_process;
        $qty = $req->us_stock_qty; 
        $item_id = $req->us_item_id; 
        $category_id = $req->us_category_id; 
        $user_id  = $req->us_login_user_id; // requesting login_user_id
        $us_plots = $req->us_plots;


        $stockQty = inventories::where('stock_item',$item_id)->orderBy('id','desc')->first();


        if(!empty($stockQty))
        {
            $prev_qty = $stockQty->stock_qty_curr;
        }
        else{
            $prev_qty = 0;
        }
        
        if($prev_qty >= $qty)
        {
            $new_qty = (int) $prev_qty - (int) $qty ;

            $data = array( 'stock_qty' => $new_qty ,  'added_by' => $user_id );

            DB::table('stock_items')->where('id',$item_id )->update($data);

            $total_no_of_plots = count($us_plots);

            $qty_for_each_plots = round($qty/ $total_no_of_plots,2) ;

            $total_cost_new = $stockQty->stock_unit_cost * $qty_for_each_plots ;

            $last_inv_ref_id = inventories::orderBy('id','desc')->value('inv_ref_id');

           for($i = 0 ;$i < $total_no_of_plots; $i++){

                $field_unique_id = field_data::where('field_status',0)->where('item_field',$us_plots[$i])->value('field_unique_id');

                if(empty($field_unique_id)){ $field_unique_id = 'Nil' ; }

                $currStockQty = inventories::where('stock_item',$item_id)->orderBy('id','desc')->value('stock_qty_curr');

                $new_Qty_of_this_plot = $currStockQty - $qty_for_each_plots ;

                $insData = array(
                'inv_ref_id' => $last_inv_ref_id + 1 ,
                'inv_status' => 0,
                'inv_desc' => 'Stock Utilised',
                'stock_category' => $category_id,
                'stock_item' => $item_id ,
                'stock_qty_new' => $qty_for_each_plots ,
                'stock_process' => $stock_process,
                'added_by' => $user_id,
                'stock_qty_prev' => $currStockQty,
                'stock_qty_curr' => $new_Qty_of_this_plot,
                'stock_field_id' => $us_plots[$i] ,
                'stock_unit_cost' => $stockQty->stock_unit_cost ,
                'field_unique_id' => $field_unique_id,
                'stock_total_cost' =>  $total_cost_new,
                'created_at' => $this->getDate()
                );

                $inventories = DB::table('inventories')->insert($insData);
           }


            $msg_status = 'success';
            $msg = 'Stock Utilise Added Successfully';
        }
        else{
            $msg_status = 'error';
            $msg = 'Stock Not Available';
        }


       return redirect('/inventory')->with($msg_status, $msg);
    }

    public function viewfield($id){
        $categories = stock_categories::where('category_status',0)->orderby('category_name','asc')->get();
        $items = stock_items::where('item_status',0)->orderby('item_name','asc')->get();
        $status = Status::where('status_status',0)->orderby('id','asc')->get();
        //$templates = DB::select("SELECT DISTINCT(template_name) FROM templates WHERE template_status = 0");
        $templates = DB::select("SELECT DISTINCT(template_name) FROM templates WHERE template_status = 0");
       // $item_values = field_data::where('item_field',$id)->orderby('id','desc')->get();
        $item_values = DB::table('field_datas')
                        //->select('users.id','users.name','profiles.photo')
                        ->join('stock_categories','stock_categories.id','=','field_datas.item_category')
                        ->join('stock_items','stock_items.id','=','field_datas.item_stock')
                        ->join('statuses','statuses.id','=','field_datas.item_status')
                        ->where('item_field',$id)
                        ->where('field_status',0)
                        ->get();

        $field_datas = DB::table('field_datas')
                    ->select('field_datas.*','plot_datas.description','plot_datas.start_day','plot_datas.end_day')
                    ->join('plot_datas','plot_datas.id','=','field_datas.items_id')
                    ->where('field_status',0)
                    ->where('item_field',$id)->get();

        $extra_field_datas = DB::table('extra_field_descs')
                    ->select('field_datas.*','extra_field_descs.id as sub_id','extra_field_descs.*')
                    ->join('field_datas','field_datas.id','=','extra_field_descs.field_data_id')
                    ->where('ef_status',0)
                    ->where('field_datas.item_field',$id)->get();

        $gencost = inventories::where('stock_field_id',$id)->where('inv_status',0)->get();

        $harvest = inventories::where('stock_field_id',$id)->where('stock_process',4)->where('inv_status',0)->value('stock_qty_new');

        return view('Pages.fields')->with(['id' => $id , 'extra_field_d' => $extra_field_datas ,'categories' => $categories ,'field_datas'=> $field_datas , 'templates' => $templates ,'stock_items' => $items , 'status' => $status , 'field_data' => $item_values , 'gc_datas' => $gencost , 'harvest' => $harvest ]);    
    }


    public function insertField(Request $req)
    {
        // requesting values
        $id = $req->fieldId; 
        $start_day = $req->start_day;
        $end_day = $req->end_dayv;
        $item_qty = $req->item_qty;
        $item_category = $req->item_category;
        $item_stock = $req->item_stock;
        $item_status  = $req->item_status; 
        $item_template = $req->temp_name;
        $login_user = $req->login_user_id ;// requesting login_user_id
        $acres = $req->acres ;
        //requesing the extra field datas 
        $item_status_sub = $req->item_status_sub;
        $item_qty_sub = $req->item_qty_sub ;
        $item_category_sub = $req->item_category_sub;
        $item_stock_sub = $req->item_stock_sub;
        $main_plot_id = $req->main_plot_id;
        $sub_plot_id = $req->sub_plot_id ;

        $field_data_ids = $req->field_data_id ;
        $submit_btn = $req->submit_btn ;
        $action_name = $req->submit ;

        


       //  for($i = 0 ; $i < count($start_day) ; $i++)
       //  { 
       //     if($i == 23)
       //     {
       //         $fd_up = array('field_status' => 1);
       //         DB::table('field_datas')->where('item_field', $id)->update($fd_up);
       //         $ev_up = array('event_status' => 1);
       //         DB::table('events')->where('field_id', $id)->update($ev_up);
       //         $ef_up = array('ef_status' => 1);
       //         DB::table('extra_field_descs')->where('field_id', $id)->update($ef_up);
       //     }
       //     else
       //     {

       //         DB::table('field_datas')->where('item_field', $id)->delete();
       //         DB::table('events')->where('field_id', $id)->delete();
       //         DB::table('extra_field_descs')->where('field_id', $id)->delete();

       //     }
       //  }

       
      

       $isGC_Update = false ;
       $msgstatus = "";
       $msg = '';
       $qtyItems = array(5,6,10,11,14,15,16);


       $field_datas = DB::table('field_datas')
                   ->join('plot_datas','plot_datas.id','=','field_datas.items_id')
                   ->where('field_status',0)
                   ->where('item_field',$id)->get();
                   
   

        if($submit_btn == 'Update'){
            DB::table('extra_field_descs')->where('field_id',$id)->delete();
            for($i = 0 ; $i < count($start_day) ; $i++)
            { 
                $up_val_id = $field_data_ids[$i];

                $unique_id = field_data::find($up_val_id)->field_unique_id;

                $oldStatus = field_data::select('item_status')->find($up_val_id);
                $ins_data['item_status'] = $item_status[$i];
                if($main_plot_id)
                {
                    for($k = 0;$k < count($main_plot_id);$k++)
                    {

                        $ins_ef_data = array(
                                            'field_id' => $id,
                                            'field_data_id' => $up_val_id - 1,
                                            'ef_status' => 0 ,
                                            'ef_item_qty' => $item_qty_sub[$k],
                                            'ef_item_category' => $item_category_sub[$k],
                                            'ef_item_stock' => $item_stock_sub[$k]
                                        );
                        if($main_plot_id[$k] == $i)
                        {
                            DB::table('extra_field_descs')->insert($ins_ef_data);
                        }
                        
                    }
                }           
            }
        }

        if(!empty($start_day)){
        
        for($i = 0 ; $i < count($start_day) ; $i++)
        { 
            
               $start_date[$i] = $this->dateFormat($start_day[$i]);

               $end_date[$i] = $this->dateFormat($end_day[$i]);

               if(empty($item_qty[$i]))
               {
                   $item_qty[$i] = 0 ;
               }

               if(empty($item_category[$i]))
               {
                   $item_category[$i] = 0 ;
               }

               if(empty($item_stock[$i]))
               {
                   $item_stock[$i] = 0 ;
               }

               if(empty($item_status[$i]))
               {
                   $item_status[$i] = 0 ;
               }

               $event_data = array(
                   'field_id' => $id,
                   'event_status' => 0,
                   'plot_desc_id' => $i + 1,
                   'start_date' => $start_date[$i],
                   'end_date' => $end_date[$i],
                   'created_at' => $this->getDate()
               );

               $ins_data = array(
                   'items_id' => $i + 1,
                   'field_status' => 0,
                   'start_date' => $start_date[$i], 
                   'end_date' => $end_date[$i],
                   'item_qty' => $item_qty[$i],
                   'item_category' => $item_category[$i],
                   'item_stock' => $item_stock[$i],
                   'item_template' => $item_template,
                   'item_field' => $id,
                   'field_acres' => $acres ,
                   'created_at' => $this->getDate() ,
                   'updated_at' => $this->getDate(),
               ); 



               if($submit_btn == 'Save')
               {
                   
                    if($i == 0)
                    {
                        $uniq_id = DB::table('field_datas')->count('id');
                        $new_id_num = ($uniq_id/24) + 1 ;
                        $unique_id = 'YIELD'.$new_id_num;
                    }
                    else{
                        $unique_id = field_data::orderBy('id','desc')->first()->field_unique_id;
                    }
                   $ins_data['field_unique_id'] = $unique_id ;
                   $ins_data['item_status'] = 0 ;
                   $event_data['field_unique_id'] = $unique_id;
                   DB::table('events')->insert($event_data);
                   DB::table('field_datas')->insert($ins_data);

                   $msgstatus = "success";
                    $msg = 'Plot Saved Successfully';        
                   
               }
               else if($submit_btn == 'Update')
               {
                
         

                $checkNilInv = inventories::where('stock_field_id',$id)->where('field_unique_id','Nil')->count();

                if($checkNilInv) 
                {
                    $up_inv_data = array('field_unique_id' => $unique_id);
                    inventories::where('stock_field_id',$id)->where('field_unique_id','Nil')->update($up_inv_data);  
                }

                  $up_val_id = $field_data_ids[$i];

                  $unique_id = field_data::find($up_val_id)->field_unique_id;

                  $oldStatus = field_data::select('item_status')->find($up_val_id);
                  $ins_data['item_status'] = $item_status[$i];
                //    if($main_plot_id)
                //    {
                //        for($k = 0;$k < count($main_plot_id);$k++)
                //        {
                //            $ins_ef_data = array(
                //                                 'field_id' => $id,
                //                                 'field_data_id' => $up_val_id - 1,
                //                                 'ef_status' => 0 ,
                //                                 'ef_item_qty' => $item_qty_sub[$k],
                //                                 'ef_item_category' => $item_category_sub[$k],
                //                                 'ef_item_stock' => $item_stock_sub[$k]
                //                             );
                //             if($main_plot_id[$k] == $i && $item_status[23] != 3)
                //             {
                //                 DB::table('extra_field_descs')->insert($ins_ef_data);
                //             }
                           
                //         }
                //     }
                $harvest = $req->harvestQty;
                
                if($i == 18 && !empty($harvest) && $item_status[$i] == 3 && $oldStatus->item_status != 3)
                {

                    $gencost = inventories::where('stock_field_id', $id)->where('stock_process',3)->orwhere('stock_process',2)->sum('stock_total_cost');      
                    $output =  round(($gencost / $harvest) , 2 ) ;

                    $last_inv_ref_id = inventories::orderBy('id','desc')->value('inv_ref_id');

                   
                    
                    $insData = array(
                        'inv_ref_id' => $last_inv_ref_id + 1 ,
                        'inv_status' => 0,
                        'inv_desc' => 'Harvest Complete ' ,
                        'stock_category' => 0,
                        'stock_item' => 0,
                        'stock_qty_new' =>  $harvest ,
                        'field_unique_id' => $unique_id ,
                        'stock_process' => 4,
                        'added_by' => $login_user,
                        'stock_qty_prev' => 0 ,
                        'stock_qty_curr' => 0 ,
                        'stock_unit_cost' => 0,
                        'stock_total_cost' => 0,
                        'stock_field_id' =>  $id ,
                        'created_at' => $this->getDate(),
                        'updated_at' => $this->getDate()
                    );
            
                    $inventories = DB::table('inventories')->insert($insData);

                    // $gc_ref_id = GeneralCosting::orderBy('id','desc')->value('gc_ref_id');

                    // if(empty($gc_ref_id))
                    // {
                    //     $gc_ref_id = 1 ;
                    // }
                    // else{
                    //     $gc_ref_id++ ;
                    // }


                    // $ins_gc_data = array(
                    //     'gc_ref_id' => $gc_ref_id++,
                    //     'gc_amount' =>  $output,
                    //     'gc_desc' => 'Status Complete - Harvest' ,
                    //     'gc_plots' => $id,
                    //     'gc_status' => 0 ,
                    //     'gc_harvest' => $harvest,
                    //     'field_unique_id' =>$unique_id,
                    //     'gc_qty' => $item_qty[$i],
                    //     'gc_process' => 2,
                    //     'gc_addedby' => $login_user,
                    //     'created_at' =>  $this->getDate()
                    // );   
                    
                    // DB::table('general_costings')->insert($ins_gc_data); 


                }

                if($oldStatus->item_status != 3 && $item_status[$i] == 3)
                {
                    if($main_plot_id)
                    {   
                        for($k = 0;$k < count($main_plot_id);$k++)
                        {
                            if($main_plot_id[$k] == $i + 1)
                             {
                                $inv_prev_qty = DB::table('stock_items')->where('id',$item_stock_sub[$k])->value('stock_qty');

                                if(empty($inv_prev_qty))
                                {
                                    $inv_prev_qty = 0;
                                }

                                $total_item_qty_sub[$k] = $acres * $item_qty_sub[$k] ;
                                    
                                if($inv_prev_qty >= (double)$total_item_qty_sub[$k])
                                {
                                    
                                    $new_qty = (int)$inv_prev_qty - (int) $total_item_qty_sub[$k] ;

                                    $data = array( 'stock_qty' => $new_qty ,  'added_by' => $login_user );

                                    DB::table('stock_items')->where('id',$item_stock_sub[$k] )->update($data);

                                    $stock_unit_cost =  DB::table('inventories')->where('stock_item',$item_stock_sub[$k])->where('stock_process',1)->orderBy('id','desc')->value('stock_unit_cost');

                                    $stock_total_cost =  $stock_unit_cost * $total_item_qty_sub[$k];

                                    $last_inv_ref_id = inventories::orderBy('id','desc')->value('inv_ref_id');

                                   $category_name_query = stock_categories::find($item_category_sub[$k]); 
                                    
                                    if(!empty($category_name_query))
                                    {
                                        $category_name = $category_name_query->category_name ;
                                    }
                                    else{
                                        $category_name = '' ;
                                    }

                                    $insData = array(
                                        'inv_ref_id' => $last_inv_ref_id + 1 ,
                                        'inv_status' => 0,
                                        'inv_desc' => 'Status Complete - '.$category_name ,
                                        'stock_category' => $item_category_sub[$k],
                                        'stock_item' => $item_stock_sub[$k] ,
                                        'stock_qty_new' => $total_item_qty_sub[$k] ,
                                        'field_unique_id' => $unique_id ,
                                        'stock_process' => 2,
                                        'added_by' => $login_user,
                                        'stock_qty_prev' => $inv_prev_qty,
                                        'stock_qty_curr' => $new_qty,
                                        'stock_unit_cost' => $stock_unit_cost,
                                        'stock_total_cost' => $stock_total_cost,
                                        'stock_field_id' =>  $id ,
                                        'created_at' => $this->getDate(),
                                        'updated_at' => $this->getDate()
                                    );
                            
                                    $inventories = DB::table('inventories')->insert($insData);

                                  /*  $category_name_query = stock_categories::find($item_category_sub[$k]); 
                                    
                                    if(!empty($category_name_query))
                                    {
                                        $category_name = $category_name_query->category_name ;
                                    }
                                    else{
                                        $category_name = '' ;
                                    }
                                    
                                    $gc_ref_id = GeneralCosting::orderBy('id','desc')->value('gc_ref_id');

                                    if(empty($gc_ref_id))
                                    {
                                        $gc_ref_id = 1 ;
                                    }
                                    else{
                                        $gc_ref_id++ ;
                                    }
                                    
                                    $ins_gc_data = array(
                                        'gc_ref_id' => $gc_ref_id,
                                        'gc_amount' => $stock_total_cost,
                                        'gc_desc' => 'Status Complete - '.$category_name ,
                                        'field_unique_id' => $unique_id ,
                                        'gc_plots' => $id,
                                        'gc_qty' => $total_item_qty_sub[$k],
                                        'gc_harvest' => 0,
                                        'gc_process' => 1,
                                        'gc_addedby' => $login_user,
                                        'gc_status' => 0 ,
                                        'created_at' => $this->getDate()
                                    );   
                                    
                                    DB::table('general_costings')->insert($ins_gc_data); */

                                }
                                else{
                                    $msgstatus = "error";
                                    $msg = 'Not Enough Stock Available';
        
                                    $ins_data_up= array('item_status' => 2);
                                    DB::table('field_datas')->where('id',$up_val_id)->update($ins_data_up);
                                    return redirect("/fields/$id")->with($msgstatus, $msg);
                                }
                             }
                        }
                    }

                    if(!empty($item_qty[$i]) && !empty($item_category[$i]) && !empty($item_stock[$i]))
                    {
                        $inv_prev_qty = DB::table('stock_items')->where('id',$item_stock[$i])->value('stock_qty');

                        if(empty($inv_prev_qty))
                        {
                            $inv_prev_qty = 0;
                        }


                        $total_item_qty[$i] = $acres * $item_qty[$i] ;


                        if((double) $inv_prev_qty >= (double) $total_item_qty[$i])
                        {
                           
                            $new_qty = (int)$inv_prev_qty - (int) $total_item_qty[$i] ;

                            $data = array( 'stock_qty' => $new_qty ,  'added_by' => $login_user );
    
                            DB::table('stock_items')->where('id',$item_stock[$i] )->update($data);

                            $stock_unit_cost =  DB::table('inventories')->where('stock_item',$item_stock[$i])->where('stock_process',1)->orderBy('id','desc')->value('stock_unit_cost');

                            $stock_total_cost =  $stock_unit_cost * $total_item_qty[$i];

                            $last_inv_ref_id = inventories::orderBy('id','desc')->value('inv_ref_id');

                            $category_name_query = stock_categories::find($item_category[$i]); 
                            
                            if(!empty($category_name_query))
                            {
                                $category_name = $category_name_query->category_name ;
                            }
                            else{
                                $category_name = '' ;
                            }

                            $insData = array(
                                'inv_ref_id' => $last_inv_ref_id + 1 ,
                                'inv_status' => 0,
                                'inv_desc' => 'Status Complete - '.$category_name ,
                                'stock_category' => $item_category[$i],
                                'stock_item' => $item_stock[$i] ,
                                'stock_qty_new' => $total_item_qty[$i] ,
                                'stock_process' => 2,
                                'added_by' => $login_user,
                                'stock_qty_prev' => $inv_prev_qty,
                                'stock_qty_curr' => $new_qty,
                                'field_unique_id' =>$unique_id,
                                'stock_unit_cost' => $stock_unit_cost,
                                'stock_total_cost' => $stock_total_cost,
                                'stock_field_id' =>  $id ,
                                'created_at' => $this->getDate(),
                                'updated_at' => $this->getDate()
                             );
                    
                            $inventories = DB::table('inventories')->insert($insData);

                         /*

                            $category_name_query = stock_categories::find($item_category[$i]); 
                            
                            if(!empty($category_name_query))
                            {
                                $category_name = $category_name_query->category_name ;
                            }
                            else{
                                $category_name = '' ;
                            }
                            
                            $gc_ref_id = GeneralCosting::orderBy('id','desc')->value('gc_ref_id');

                            if(empty($gc_ref_id))
                            {
                                $gc_ref_id = 1 ;
                            }
                            else{
                                $gc_ref_id++ ;
                            }


                            $ins_gc_data = array(
                                'gc_amount' => $stock_total_cost,
                                'gc_desc' => 'Status Complete - '.$category_name ,
                                'gc_plots' => $id,
                                'gc_ref_id' => $gc_ref_id,
                                'gc_harvest' => 0,
                                'field_unique_id' =>$unique_id,
                                'gc_qty' => $total_item_qty[$i],
                                'gc_process' => 1,
                                'gc_addedby' => $login_user,
                                'gc_status' => 0 ,
                                'created_at' => $this->getDate()
                            );   
                            
                            DB::table('general_costings')->insert($ins_gc_data);   */
                            
                            

                            $msgstatus = "success";
                            $msg = 'Status Updated Successfully';  
                            $ins_data['item_status'] = 3;                  
                        }
                        else
                        {
                            
                            $msgstatus = "error";
                            $msg = 'Not Enough Stock Available';
                            
                            $ins_data_up= array('item_status' => 2);
                            DB::table('field_datas')->where('id',$up_val_id)->update($ins_data_up);
                            return redirect("/fields/$id")->with($msgstatus, $msg);
                        }

                       
                    }
                    else{
                        $msgstatus = "success";
                        $msg = 'Status Updated Successfully';  
                        $ins_data['item_status'] = 3;     
                    }

                    
                }

                    DB::table('events')->where('id',$up_val_id)->update($event_data);
                    DB::table('field_datas')->where('id',$up_val_id)->update($ins_data);

            }

            }
        }
        else{

            $msgstatus = 'error';
            $msg = "No data Found !!";

        }

        $f_unique_id = $req->f_unique_id ;
        
        if($req->finish == 'Finish Season')
        {
            if($item_status[23] == 3)
            {
                $checkNilInv = inventories::where('stock_field_id',$id)->where('field_unique_id','Nil')->count();

                if($checkNilInv) 
                {
                    $up_inv_data = array('field_unique_id' => $f_unique_id);
                    inventories::where('stock_field_id',$id)->where('field_unique_id','Nil')->update($up_inv_data);  
                }
                $up_event_data = array( 'event_status' => 2) ;
                $up_ins_data = array('field_status' => 2)  ;
                $ins_gc_data = array('inv_status' => 2);
                DB::table('events')->where('field_unique_id',$f_unique_id)->update($up_event_data);
                DB::table('field_datas')->where('field_unique_id',$f_unique_id)->update($up_ins_data);
                DB::table('inventories')->where('field_unique_id',$f_unique_id)->where('inv_status',0)->update($ins_gc_data);
            }
            else{
                $msgstatus = "error";
                $msg = 'Please Complete the tasks first';  
                return redirect("/fields/$id")->with($msgstatus, $msg);
            }
           
        }
        


       // return redirect("/fields/$id")->with([$msgstatus => $msg, 'field_datas' => $field_datas]);
       return redirect("/fields/$id")->with($msgstatus, $msg);
       // return redirect("/dashboard")->with($msgstatus,$msg);
      
   }

    public function dateFormat($date){
        $exp_date = explode('-',$date);
        $new_d = $exp_date[2].'-'.$exp_date[1].'-'.$exp_date[0] ;
        return $new_d ;
    }

    public function viewUnit(){
        $units = DB::table('units')->where('unit_status',0)->orderBy('id','desc')->get();
        return view('Pages.units')->with(['units' => $units]);
    }

    public function insertUnit(Request $req){
        $action_name = $req->submit;
        $unit = $req->unit_name; 
        $user_id  = $req->login_user_id; // requesting login_user_id

        if($action_name == 'Add')
        {
            $data = array( 'unit_name' => $unit ,  'unit_status' => 0 ,'added_by' => $user_id );

            DB::table('units')->insert($data);
            return redirect('/units')->with('success','Unit Added Successfully');
        }
        else if($action_name == 'Update')         
        {
            $id = $req->unit_id ;

            $data = array( 'unit_name' => $unit ,  'added_by' => $user_id );

            DB::table('units')->where('id',$id )->update($data);
            return redirect('/units')->with('success','Unit Updated Successfully');
        }
     
      
    }

    public function deleteUnit(Request $req){

        $id = $req->del_id;
        $del_arr = array('unit_status' => 1);
        DB::table('units')->where('id', $id)->update($del_arr);
    
        return redirect('/units')->with('success','Unit Deleted Successfully');
    }

    // public function loadData(Request $req)
    // {
    //     $tofieldId = $req->tofieldId;
    //     $fromfieldId = $req->fromfieldId ;

    //     $fromfieldData = DB::table('field_datas')->where('item_field', $fromfieldId)->get();

    //     foreach($fromfieldData as $data)
    //     {
    //         $ins_data = array(
    //             'items_name' => $data->items_name,
    //             'item_status' =>$data->item_status,
    //             'item_qty' => $data->item_qty,
    //             'item_category' => $data->item_category,
    //             'item_stock' => $data->item_stock,
    //             'item_field' => $tofieldId,
    //             'created_at' => date("Y-m-d H:i:s") ,
    //             'updated_at' => date("Y-m-d H:i:s")
    //         );             

    //         DB::table('field_datas')->insert($ins_data);
    //     }
    //     return redirect("/fields/$tofieldId")->with('success', 'Field Data is Loaded Successfully');
    // }

    public function viewPlot()
    {
        $categories = stock_categories::where('category_status',0)->orderby('category_name','asc')->get();
        $items = stock_items::where('item_status',0)->orderby('item_name','asc')->get();
        $status = Status::where('status_status',0)->orderby('id','asc')->get();
        $plot_datas = plot_data::all();
        return view('Pages.plot')->with(['plot_datas' => $plot_datas ,'categories' => $categories , 'stock_items' => $items , 'status' => $status ]);    
    }

    public function getItemsByCategory(Request $req)
    {
        $cat_id = $req->id;
        $stock_items = DB::table('stock_items')
                        ->where('stock_items.category_name',$cat_id)
                        ->where('item_status',0)
                        ->get();
        return response()->json(['stock_items' => $stock_items]);
    }

    public function viewPlotData()
    {

        $templates = DB::select("SELECT DISTINCT(template_name) FROM templates WHERE template_status = 0");
        return view('Pages.viewPlot')->with(['templates' => $templates]);
    }

    public function viewSinglePlot($id)
    {
        $categories = stock_categories::where('category_status',0)->orderby('category_name','asc')->get();
        $items = stock_items::where('item_status',0)->orderby('item_name','asc')->get();
        $status = Status::where('status_status',0)->orderby('id','asc')->get();
        $templates = DB::table('templates')
                    ->select('template_name','templates.id','start_date','end_date','item_status_id','item_qty','item_ledger_id','item_stock_id','description','start_day','end_day')
                    ->join('plot_datas','plot_datas.id','=','templates.plot_desc_id')
                    ->where('template_name',$id)->get();


        return view('Pages.singlePlot')->with(['templates'=> $templates ,'template' => $id ,'categories' => $categories , 'stock_items' => $items , 'status' => $status ]);  
    }

  
}
