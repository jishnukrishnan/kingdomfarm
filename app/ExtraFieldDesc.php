<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraFieldDesc extends Model
{
     //
     protected $table = 'extra_field_descs';

     //primary key 
     public $primaryKey = 'id';
  
     //timestamps
     public $timestamp = true; 
}
