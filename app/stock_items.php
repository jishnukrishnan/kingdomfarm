<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stock_items extends Model
{
    // table 

    protected $table = 'stock_items';

    //primary key

    public $primary = 'id';

    public $timestamp = true;
}
