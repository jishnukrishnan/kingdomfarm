<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class templates extends Model
{
    protected $table = 'templates';

    public $primaryKey = 'id';

    public $timestamps = true;
}
