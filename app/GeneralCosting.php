<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralCosting extends Model
{
    //
    protected $table = 'general_costings';

    //primary key 
    public $primaryKey = 'id';
 
    //timestamps
    public $timestamp = true; 
}
