<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menuprivilege extends Model
{
    //
    protected $table = 'menuprivileges';

    //primary key 
    public $primaryKey = 'id';
 
    //timestamps
    public $timestamp = true; 
}
