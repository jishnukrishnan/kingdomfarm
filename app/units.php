<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class units extends Model
{
    //
    protected $table = 'units';

    //primary key 
    public $primaryKey = 'id';
 
    //timestamps
    public $timestamp = true; 
}
