<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inventories extends Model
{
    //
    protected $table = 'inventories';

    //primary key 
    public $primaryKey = 'id';
 
    //timestamps
    public $timestamp = true; 
}
