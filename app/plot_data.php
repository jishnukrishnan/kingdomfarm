<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class plot_data extends Model
{
    protected $table = 'plot_datas';

    public $primaryKey = 'id';

    public $timestamps = true ;
}
