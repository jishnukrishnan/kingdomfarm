<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class field_data extends Model
{
      // table 
      protected $table = 'field_datas';

      //primary key 
      public $primaryKey = 'id';
 
      //timestamps
      public $timestamp = true; 
}
