<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stock_categories extends Model
{
    // table 

    protected $table = 'stock_categories';

    //primary key

    public $primary = 'id';

    public $timestamp = true;
}
