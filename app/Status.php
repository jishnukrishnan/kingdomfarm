<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    // table 
     protected $table = 'statuses';

     //primary key 
     public $primaryKey = 'id';

     //timestamps
     public $timestamp = true;  
}
