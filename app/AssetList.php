<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetList extends Model
{
     //
     protected $table = 'assetlist';

     //primary key 
     public $primaryKey = 'AL_id';
  
     //timestamps
     public $timestamp = true; 
}
