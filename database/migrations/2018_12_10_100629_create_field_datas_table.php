<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('items_id');
            $table->string('field_unique_id');
            $table->tinyInteger('field_status');
            $table->date('start_date');
            $table->date('end_date');
            $table->double('field_acres', 15, 2);
            $table->integer('item_status');
            $table->double('item_qty');
            $table->integer('item_category');
            $table->integer('item_stock');
            $table->integer('item_field');
            $table->string('item_template');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_datas');
    }
}
