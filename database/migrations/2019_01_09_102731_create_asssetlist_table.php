<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsssetlistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assetlist', function (Blueprint $table) {
            $table->increments('AL_id');
            $table->tinyInteger('AL_status')->default(0);
            $table->string('AL_regNo');
            $table->integer('AL_cateId');
            $table->string('AL_modelNo');
            $table->string('AL_engine');
            $table->string('AL_serialNo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assetlist');
    }
}
