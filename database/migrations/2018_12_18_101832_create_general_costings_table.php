<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralCostingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_costings', function (Blueprint $table) {
            $table->increments('id');
            $table->double('gc_amount');
            $table->integer('gc_ref_id');
            $table->integer('gc_status');
            $table->integer('gc_process');
            $table->integer('gc_qty');
            $table->string('field_unique_id');
            $table->double('gc_harvest');
            $table->string('gc_desc');
            $table->string('gc_plots');
            $table->integer('gc_addedby');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_costings');
    }
}
