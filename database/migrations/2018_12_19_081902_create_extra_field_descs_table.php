<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraFieldDescsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_field_descs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('field_data_id');
            $table->integer('field_id');
            $table->integer('ef_status');
            $table->integer('ef_item_qty');
            $table->integer('ef_item_category');
            $table->integer('ef_item_stock');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_field_descs');
    }
}
