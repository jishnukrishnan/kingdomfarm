<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('template_status');
            $table->string('template_name');
            $table->integer('plot_desc_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('item_status_id');
            $table->double('item_qty');
            $table->integer('item_ledger_id');
            $table->integer('item_stock_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('templates');
    }
}
