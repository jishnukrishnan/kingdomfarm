<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_category');
            $table->string('field_unique_id');
            $table->integer('stock_item');
            $table->integer('inv_status');
            $table->string('inv_desc');
            $table->integer('inv_ref_id');
            $table->integer('stock_field_id');
            $table->float('stock_qty_prev', 8, 2);
            $table->float('stock_qty_new', 8, 2);
            $table->float('stock_qty_curr', 8, 2);
            $table->double('stock_unit_cost', 15, 2);
            $table->double('stock_total_cost', 15, 2);
            $table->integer('stock_process');
            $table->integer('added_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
