<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSparepartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spareparts', function (Blueprint $table) {
            $table->increments('SP_id');
            $table->integer('SP_cateId');
            $table->string('SP_modelNo');
            $table->integer('SP_prevQty');
            $table->integer('SP_currQty');
            $table->string('SP_no');
            $table->string('SP_desc');
            $table->integer('SP_qty');
            $table->double('SP_unitPrice',12,2)->default(0)->nullable();
            $table->double('SP_costPrice',12,2)->default(0)->nullable();
            $table->integer('SP_plotId');
            $table->integer('SP_operator');
            $table->string('SP_regNo')->nullable();
            $table->tinyInteger('SP_process');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spareparts');
    }
}
