@extends('layouts.navbar')
@section('content')

   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Workshop Management <i class="fa fa-angle-double-right" aria-hidden="true"></i> View Stock</h2>

           <div class="row col-md-12 table-responsive">
               <table class="table table-bordered text-center table-data-tr ">
                    <tr>
                        <th>#</th>
                        <th>Category <form action="/makeWCategoryAsc" id="wcat_asc_form" method="post" style="display:inline;">{{csrf_field()}}<a href="#" class="pull-right" onclick="document.getElementById('wcat_asc_form').submit();"><i class="fa fa-sort-alpha-asc" aria-hidden="true"></i></a></button></form></th>
                        <th>Sparepart No. <form action="/makeSpareAsc" id="sp_asc_form" method="post" style="display:inline;">{{csrf_field()}}<a href="#" class="pull-right" onclick="document.getElementById('sp_asc_form').submit();"><i class="fa fa-sort-alpha-asc" aria-hidden="true"></i></a></button></form> </th>
                        <th>Avail Stock</th>
                        <th>Model No </th>
                      
                    </tr>

                    @if(count($spareparts) > 0)
                    @php
                       // dd($spareparts);
                        $i = 1;
                    @endphp
                        @foreach($spareparts as $sparepart)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$sparepart->C_name}}</td>
                                <td>{{$sparepart->SP_no}}</td>
                                <td>{{$sparepart->stock}}</td>
                                <td>{{$sparepart->SP_modelNo}}</td>
                                
                            </tr>
                        @endforeach
                    @endif
                   
               </table>
           </div>
   
   </div>
    
 
    
@endsection

<script>
  
</script>