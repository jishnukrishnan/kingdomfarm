@extends('layouts.navbar')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Workshop Management <i class="fa fa-angle-double-right" aria-hidden="true"></i> Spare Parts  <i class="fa fa-angle-double-right" aria-hidden="true"></i> {{@$spareparts[0]->C_name}}   <a href="/spareParts"><button class="btn btn-default pull-right" >Go Back</button></a>
        </h2>
        
       
   
    <div class="submit-form">

           <div class="row col-md-12 col-sm-12 table-responsive" id="sparepart-table">
               <table class="table table-bordered text-center  table-data-tr ">
                    <tr>
                        <th>Sl.No</th>
                        <th>Time</th>
                        <th>Add/ Utilise</th>
                        <th>Category</th>
                        <th>Model No</th>
                        <th>Spare Part</th> 
                        <th>Desc</th>      
                        <th>Prev. Qty</th>              
                        <th>Qty</th>
                        <th>Cur. Qty</th>
                        <th>Unit Cost</th>
                        <th>Total Cost</th>
                        <th>Issued Plot </th>
                        <th>Operator</th>
                    </tr>
                    @if(count($spareparts) > 0)
                        @foreach($spareparts as $sp)
                            <tr>
                                <td>{{$sp->SP_id}}</td>
                                <td>{{date('d-m-Y',strtotime($sp->created_on))}}</td>
                                <td>
                                    @if($sp->SP_process == 1)
                                        @php
                                            echo 'Added'; 
                                        @endphp
                                    @elseif($sp->SP_process == 2)
                                        @php
                                            echo 'Utilised'; 
                                        @endphp
                                    @endif
                                </td>
                                <td>{{$sp->C_name}}</td>
                                <td>{{$sp->SP_modelNo}}</td>
                                <td>{{$sp->SP_no}}</td>
                                <td>{{$sp->SP_desc}}</td>
                                <td>{{$sp->SP_prevQty}}</td>
                                <td>{{$sp->SP_qty}}</td>
                                <td>{{$sp->SP_currQty}}</td>
                                <td>{{$sp->SP_unitPrice}}</td>
                                <td>{{$sp->SP_costPrice}}</td>
                                <td>
                                    <?php 
                                        if(!empty($sp->SP_plotId))
                                        {
                                            if($sp->SP_plotId == 67)
                                            {
                                                echo 'PLOT 55 A';
                                            }
                                            else
                                            {
                                                echo 'PLOT '.$sp->SP_plotId;
                                            }
                                        }    
                                    ?>
                                </td>
                                <td>{{$sp->OP_fname.' '}} {{$sp->OP_lname}}</td>
                            </tr>
                        @endforeach
                    @endif
                   
               </table>
               {{-- {{$inventories->links()}} --}}
           </div>
    </div>
   </div>
    
  
    
@endsection
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script>

    function check_selection(from) {
        var result = [];
        if(from == 1)
        {
            if($('#us_all_plots').prop('checked') ==  true){
                $('#us_all_plots').prop('checked',false);
            }
            // $('#gc_plots option:selected').css('background-color','rgb(60, 141, 188)');
            
        } 
        else if (from == 2)
        {
            if($('#us_all_plots').prop('checked') == true)
            {
                $('#us_plots option').attr('selected',true);
                for(var i = 0; i < 68; i++)
                {
                    result.push(i);
                }
                $('#us_plots').val(result);
            }
            else{
                $('#us_plots option').attr('selected',false);
                $('#us_plots').val('');
            }
        }   
    }

    function calTotal()
    {
        let unit_cost = $('#as_stock_unit_cost').val();
        let qty = $('#as_stock_qty').val();
        let total = unit_cost * qty;
        $('#as_stock_total_cost').val(total);
        total = (total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'); 
        $('#as_stock_total_cost1').val(total);
       
    }

    function setCost(val)
    {
        const opvalue = document.getElementById('as_item_id').options[document.getElementById('as_item_id').selectedIndex].text;
        const unit_cost = opvalue.split('-');
        $('#as_stock_unit_cost').val(unit_cost[1]);
        get_avail_stock(val,1);
    }

    function get_avail_stock(id,value)
    {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/getAvailItem') }}",
                  method: 'post',
                  data: {
                     id: id,
                  },
                  success: function(result){
                      //  console.log(result);
                      if(value == 1)
                      {
                        $('#stck_q_label1').css('display','block');
                        $('#stck_q1').html(result.qty);
                        $('#stck_q1').css('color','green');
                      }
                      else if(value == 2)
                      {
                        $('#stck_q_label2').css('display','block');
                        $('#stck_q2').html(result.qty);
                        $('#stck_q2').css('color','green');
                      }
                  }
            });
    }

        function setSPValues(sp){
            
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    url: "{{ url('/getSPValues') }}",
                    method: 'post',
                    data: {
                        id: sp,
                    },
                    success: function(result){
                
                        $.each(result.spareparts, function(i,item){
                            if(item){
                                $('#us_category_id').val(item.SP_cateId);
                                $('#us_model_no').val(item.SP_modelNo);
                                $('#us_description').val(item.SP_desc);
                                $('#us_stock_unit_cost').val(item.SP_unitPrice);
                                
                            }
                        })
                        

                    }
                });
            }

            $(document).ready(function(){
            $("#from_d").datepicker({
                    format:'dd-mm-yyyy',
                    autoclose: true,
                    orientation: 'bottom'
            });
            $("#to_d").datepicker({
                    format:'dd-mm-yyyy',
                    autoclose: true,
                    orientation: 'bottom'
            });

        })

        function print_inv()
        {
            var myWindow = window.open('','','width=1024,height=800');
                var  content = '<div id="div_print"  >';
                    content +=  $('#sparepart-table').html();
                    content += '</div>';

                    content += '<style type="text/css">' +
            'table,table th, table td {' +
            'border:1px solid #000;' +
            'border-collapse:collapse;text-align:center;     }' +
            'table tr td:nth-child(2), tr th:nth-child(2) { width:20%;}' +
            '</style>';
                
        
            // $('#div_print table tr td').css('border','1px solid #000');
            // $('#div_print table tr td').css('font-size','5px');
            myWindow.document.write(content);
            myWindow.print();
            myWindow.close();
        }

        function onlyShow()
        {
            const show = $('#show').val();
            $('#onlyShowValue').val(show);

            $('#filter').click();
            
        }

</script>