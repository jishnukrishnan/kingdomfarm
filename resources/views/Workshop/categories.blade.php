@extends('layouts.navbar')
<style>
    .mkyellow {
        color:#f0ad4e !important;
        border-color:#d58512 !important;
    }
    .mkred {
        color:#c9302c;
        border-color:#ac2925;
    }
</style>
@section('content')

   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Workshop Management  <i class="fa fa-angle-double-right" aria-hidden="true"></i> Workshop Category</h2>
        <div class="submit-form col-md-12 col-sm-12">
            <form action="/workshopCateIns" method="POST">
                {{csrf_field()}}
                    <input type="hidden" name="login_user_id" value="{{Auth::user()->id}}">
                    <div class="col-md-4 col-sm-6  col-xs-8">
                            <input type="text" class="form-control " name="work_category" id="work_category" placeholder="Workshop Category Name" required>                          
                    </div>
                    <input type="hidden" id="cate_id" name="category_id">
                    <div class="col-md-2 col-sm-2 col-xs-2">
                            <input type="submit" class="btn btn-primary " id="submit" name="submit" value="Add"> 
                    </div>
                    <div class="col-md-6 col-sm-4  col-xs-2"></div>
            </form>
        </div>
           <div class="row col-md-12 table-responsive">
               <table class="table table-bordered text-center table-data-tr">
                    <tr>
                        <th>Sl.No</th>
                        <th>Category Name</th>
                        <th>Options</th>
                    </tr>   

                    @if(count($categories) > 0)
                        @php
                            $i = 1 ;
                        @endphp
                        @foreach($categories as $category)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$category->C_name}}</td>
                                <td class="action-icons">
                                    <a href="#" onclick="update('{{$category->C_name}}',{{$category->C_id}})"><i class="fa fa-pencil mkyellow"></i></a>
                                    <a href="#" onclick="deleteC('{{$category->C_name}}',{{$category->C_id}})" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash mkred"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
               </table>
           </div>
           
    </div>
   </div>
    
   <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Category ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="del_text"></p>
            </div>
            <div class="modal-footer">
                <form action="/deleteWorkCate" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" id="del_id" name="del_id">
                    <input type="submit" class="btn btn-primary" value="Delete" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    
@endsection

<script>
    function update(category,id)
    {
        document.getElementById('work_category').value = category;
        document.getElementById('cate_id').value = id;
        document.getElementById('submit').value = "Update";
        document.getElementById('submit').innerHTML = "Update";
    }

    function deleteC(category,id) {
        document.getElementById('del_text').innerHTML = 'Are you sure to delete Work Category named '+category+'?';
        document.getElementById('del_text').style.color = 'red';
        document.getElementById('del_id').value = id ;
    }
</script>