@extends('layouts.navbar')
<style>
    .mkyellow {
        color:#f0ad4e !important;
        border-color:#d58512 !important;
    }
    .mkred {
        color:#c9302c;
        border-color:#ac2925;
    }
</style>
@section('content')

   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Workshop Management  <i class="fa fa-angle-double-right" aria-hidden="true"></i>  Operator</h2>
        <div class="submit-form col-md-12 col-sm-12">
            <form action="/operatorIns" method="POST">
                {{csrf_field()}}
                    <input type="hidden" name="login_user_id" value="{{Auth::user()->id}}">
                    <div class="col-md-2">  
                            <label>First Name</label>
                            <input type="text" class="form-control" name="op_fname" id="op_fname" placeholder="First  Name" required>                          
                    </div>
                    <div class="col-md-2">  
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="op_lname" id="op_lname" placeholder="Last Name" required>                          
                    </div>
                    <div class="col-md-2">  
                            <label>Phone No</label>
                            <input type="text" class="form-control" name="op_phone" id="op_phone" placeholder="Phone No" required>                          
                    </div>
                    <div class="col-md-2">  
                            <label>Driving Licence</label>
                            <input type="text" class="form-control" name="op_drivelicence" id="op_drivelicence" placeholder="Licence No" required>                          
                    </div>
                    <div class="col-md-2">  
                            <label>Address </label>
                            <textarea type="text" class="form-control" name="op_address" id="op_address" placeholder="Address" rows="1" required>   </textarea>                       
                    </div>
                    <input type="hidden" id="operator_id" name="operator_id">
                    <div class="col-md-1">
                            <label for="" class="invisible">fdsaf dsdd</label>
                            <input type="submit" class="btn btn-primary " id="submit" name="submit" value="Add"> 
                    </div>
            </form>
        </div>
           <div class="row col-md-12 table-responsive">
               <table class="table table-bordered text-center table-data-tr">
                    <tr>
                        <th>Sl.No</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone No</th>
                        <th>Driving Licence</th>
                        <th>Address</th>
                        <th>Options</th>
                    </tr>   

                    @if(count($operators) > 0)
                        @php
                            $i = 1 ;
                        @endphp
                        @foreach($operators as $operator)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$operator->OP_fname}}</td>
                                <td>{{$operator->OP_lname}}</td>
                                <td>{{$operator->OP_phoneNo}}</td>
                                <td>{{$operator->OP_driveLicence}}</td>
                                <td>{{$operator->OP_Address}}</td>
                                <td class="action-icons">
                                    <a href="#" onclick="update({{$operator->OP_id}})"><i class="fa fa-pencil mkyellow"></i></a>
                                    <a href="#" onclick="deleteC('{{$operator->OP_fname}}',{{$operator->OP_id}})" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash mkred"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
               </table>
           </div>
           
    </div>
   </div>
    
   <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Operator ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="del_text"></p>
            </div>
            <div class="modal-footer">
                <form action="/deleteOperator" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" id="del_id" name="del_id">
                    <input type="submit" class="btn btn-primary" value="Delete" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    
@endsection

<script>
    function update(id)
    {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/getOperator') }}",
                  method: 'post',
                  data: {
                     id: id,
                  },
                  success: function(result){
                   // console.log();
                    
                     $.each(result.operators, function(i,op){
                        if(op){
                            $('#op_fname').val(op.OP_fname);
                            $('#op_lname').val(op.OP_lname);
                            $('#op_phone').val(op.OP_phoneNo);
                            $('#op_drivelicence').val(op.OP_driveLicence);
                            $('#op_address').val(op.OP_Address);
                            $('#operator_id').val(op.OP_id);
                            $('#submit').val('Update');
                            $('#submit').html('Update');
                        }
                    })


                  }
        });
    }

    function deleteC(operatorname,id) {
        document.getElementById('del_text').innerHTML = 'Are you sure to delete Operator named '+operatorname+'?';
        document.getElementById('del_text').style.color = 'red';
        document.getElementById('del_id').value = id ;
    }
</script>