@extends('layouts.navbar')
<style>
    .mkyellow {
        color:#f0ad4e !important;
        border-color:#d58512 !important;
    }
    .mkred {
        color:#c9302c;
        border-color:#ac2925;
    }
</style>
@section('content')

   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Workshop Management  <i class="fa fa-angle-double-right" aria-hidden="true"></i>  Asset List</h2>
        <div class="row submit-form col-md-12 col-sm-12">
            <form action="/assetListIns" method="POST">
                {{csrf_field()}}
                    <input type="hidden" name="login_user_id" value="{{Auth::user()->id}}">
                    <div class="col-md-2">  
                            <label>Registration Number</label>
                            <input type="text" class="form-control" name="al_regno" id="al_regno" placeholder="Registration No." required>                          
                    </div>
                    <div class="col-md-2">  
                            <label>Category</label>
                            <select type="text" class="form-control" name="al_category" id="al_category" required> 
                                <option value=" ">Select</option>
                                @if(count($categories) > 0)
                                @foreach($categories as $category)
                                <option value="{{$category->C_id}}">{{$category->C_name}}</option>
                                    @endforeach
                                @endif
                            </select>                         
                    </div>
                    <div class="col-md-2">  
                            <label>Model No</label>
                            <input type="text" class="form-control" name="al_modelno" id="al_modelno" placeholder="Model No" required>                          
                    </div>
                    <div class="col-md-2">  
                            <label>Engine</label>
                            <input type="text" class="form-control" name="al_engine" id="al_engine" placeholder="Engine" required>                          
                    </div>
                    <div class="col-md-2">  
                            <label>Serial No. </label>
                            <input type="text" class="form-control" name="al_slno" id="al_slno" placeholder="Serial No"  required>                  
                    </div>
                    <input type="hidden" id="assetlist_id" name="assetlist_id">
                    <div class="col-md-1">
                            <label for="" class="invisible">fdsaf dsdd</label>
                            <input type="submit" class="btn btn-primary " id="submit" name="submit" value="Add"> 
                    </div>
            </form>
        </div>
           <div class="row col-md-12 table-responsive">
               <table class="table table-bordered text-center table-data-tr">
                    <tr>
                        <th>#</th>
                        <th>Registration No</th>
                        <th>Category</th>
                        <th>Model No</th>
                        <th>Engine</th>
                        <th>Serial No.</th>
                        <th>Options</th>
                    </tr>   

                    @if(count($assetlists) > 0)
                        @php
                            $i = 1 ;
                        @endphp
                        @foreach($assetlists as $assetlist)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$assetlist->AL_regNo}}</td>
                                <td>{{$assetlist->C_name}}</td>
                                <td>{{$assetlist->AL_modelNo}}</td>
                                <td>{{$assetlist->AL_engine}}</td>
                                <td>{{$assetlist->AL_serialNo}}</td>
                                <td class="action-icons">
                                    <a href="#" onclick="update({{$assetlist->AL_id}})"><i class="fa fa-pencil mkyellow"></i></a>
                                    <a href="#" onclick="deleteC('{{$assetlist->AL_regNo}}',{{$assetlist->AL_id}})" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash mkred"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
               </table>
           </div>
           
    </div>
   </div>
    
   <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Assetlist ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="del_text"></p>
            </div>
            <div class="modal-footer">
                <form action="/deleteAssetlist" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" id="del_id" name="del_id">
                    <input type="submit" class="btn btn-primary" value="Delete" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    
@endsection

<script>
    function update(id)
    {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/getAssetList') }}",
                  method: 'post',
                  data: {
                     id: id,
                  },
                  success: function(result){
                   // console.log();
                    
                     $.each(result.assetlists, function(i,op){
                        if(op){
                            $('#al_regno').val(op.AL_regNo);
                            $('#al_category').val(op.AL_cateId);
                            $('#al_modelno').val(op.AL_modelNo);
                            $('#al_engine').val(op.AL_engine);
                            $('#al_slno').val(op.AL_serialNo);
                            $('#assetlist_id').val(op.AL_id);
                            $('#submit').val('Update');
                            $('#submit').html('Update');
                        }
                    })


                  }
        });
    }

    function deleteC(assetlist,id) {
        document.getElementById('del_text').innerHTML = 'Are you sure to delete AssetList named '+assetlist+'?';
        document.getElementById('del_text').style.color = 'red';
        document.getElementById('del_id').value = id ;
    }
</script>