@extends('layouts.navbar')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Workshop Management <i class="fa fa-angle-double-right" aria-hidden="true"></i> Spare Parts  <button class="btn btn-danger pull-right round-btn "  data-toggle="modal" data-target="#utiliseStockModal"><i class="fa fa-minus fa-2x"></i></button>
            <button class="btn btn-success pull-right round-btn " data-toggle="modal" data-target="#addStockModal"><i class="fa fa-plus fa-2x"></i></button></h2>
        
       
   
    <div class="row submit-form">

            <form action="/filterSpareparts" method="POST">
                {{csrf_field()}}

                <div class="col-lg-1 col-md-2 col-sm-4 col-xs-4">
                        <label>From  </label>
                        <input type="text" class="form-control " name="from_d" id="from_d" value="<?php echo @$selected['from_date']; ?>" placeholder="dd-mm-yy" >                              
                </div>
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-3">
                        <label>To </label>
                        <input type="text" class="form-control " name="to_d" id="to_d" value="<?php echo @$selected['to_date']; ?>" placeholder="dd-mm-yy" >                             
                </div>


                <div class="col-lg-2 col-md-2 col-sm-4  col-xs-4">
                    <label>Category </label>
                        <select  class="form-control " name="stock_category" id="stock_category" >    
                            <option value="">Select Category</option>
                            @if(count($categories) > 0)
                                @foreach($categories as $category)
                                    <option value="{{$category->C_id}}" <?php if(@$selected['stock_category'] == $category->C_id){ echo 'selected' ; } ?> >{{$category->C_name}}</option>
                                @endforeach
                            @endif
                        </select>                        
                </div>
                    
                   
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                    <label>Plot</label>
                    <select name="plot_id" id="plot_id" class="form-control" >
                        <option value="">Select Plot</option>
                        
                            @for($i = 1; $i < 67; $i++)
                                <option value="{{$i}}" <?php if(@$selected['plot_id'] == $i){ echo 'selected' ; } ?>>Plot {{$i}}</option>
                                @if($i == 55)
                                    <option value="67" <?php if(@$selected['plot_id'] == 67){ echo 'selected' ; } ?>>Plot 55 A</option>
                                @endif
                            @endfor
                        
                    </select>                        
                </div>

                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                    <label>Add/ Utilise</label>
                    <select name="process_id" id="process_id" class="form-control" >
                        <option value="">Select </option>
                        <option value="1" <?php if(@$selected['process'] == 1){ echo 'selected' ; } ?>>Added </option>
                        <option value="2" <?php if(@$selected['process'] == 2){ echo 'selected' ; } ?>>Utilised </option>
                    </select>                        
                </div>

                <input type="hidden" name="onlyShowValue" id="onlyShowValue" />
                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
                            <label style="visibility: hidden;">Plot</label><br/>
                            <input type="submit" class="btn btn-success " id="filter" name="filter" value="Filter"> 
                    </div>

                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
                            <label style="visibility: hidden;">Plot</label><br/>
                            <input type="submit" class="btn btn-danger " id="reset" name="reset" value="Reset"> 
                    </div>

                   

                </form>

                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
                            <label style="visibility: hidden;">Plot</label><br/>
                            <button type="submit" class="btn btn-primary pull-right" style="margin-top:-15px" onclick="print_inv()" id="print" name="print" > Print</button>
                    </div>

                    <div class="col-lg-1 col-md-1 col-sm-4 col-xs-4">
                            <label style="visibility: hidden;">Plot</label><br/>
                        <select name="show" id="show" class="form-control" style="margin-top:-15px" onchange="onlyShow()">
                            <option value=""> All </option>
                            <option value="50" <?php if(@$selected['show'] == 50){ echo 'selected' ; } ?>>50 </option>
                            <option value="100" <?php if(@$selected['show'] == 100){ echo 'selected' ; } ?>>100 </option>
                            <option value="200" <?php if(@$selected['show'] == 200){ echo 'selected' ; } ?>>200 </option>
                            <option value="500" <?php if(@$selected['show'] == 500){ echo 'selected' ; } ?>>500 </option>
                        </select>                        
                    </div>



           <div class="row col-md-12 col-sm-12 table-responsive" id="sparepart-table">
               <table class="table table-bordered text-center  table-data-tr ">
                    <tr>
                        <th>Sl.No</th>
                        <th>Time</th>
                        <th>Add/ Utilise</th>
                        <th>Category</th>
                        <th>Model No</th>
                        <th>Spare Part</th> 
                        <th>Desc</th>      
                        <th>Prev. Qty</th>              
                        <th>Qty</th>
                        <th>Cur. Qty</th>
                        <th>Unit Cost</th>
                        <th>Total Cost</th>
                        <th>Issued Plot </th>
                        <th>Operator</th>
                    </tr>
                    @if(count($spareparts) > 0)
                        @foreach($spareparts as $sp)
                            <tr>
                                <td>{{$sp->SP_id}}</td>
                                <td>{{date('d-m-Y',strtotime($sp->created_on))}}</td>
                                <td>
                                    @if($sp->SP_process == 1)
                                        @php
                                            echo 'Added'; 
                                        @endphp
                                    @elseif($sp->SP_process == 2)
                                        @php
                                            echo 'Utilised'; 
                                        @endphp
                                    @endif
                                </td>
                                <td><a href="/filterSparepartCategory/{{$sp->SP_cateId}}">{{$sp->C_name}}</a></td>
                                <td>{{$sp->SP_modelNo}}</td>
                                <td>{{$sp->SP_no}}</td>
                                <td>{{$sp->SP_desc}}</td>
                                <td>{{$sp->SP_prevQty}}</td>
                                <td>{{$sp->SP_qty}}</td>
                                <td>{{$sp->SP_currQty}}</td>
                                <td>{{$sp->SP_unitPrice}}</td>
                                <td>{{$sp->SP_costPrice}}</td>
                                <td>
                                    <?php 
                                        if(!empty($sp->SP_plotId))
                                        {
                                            if($sp->SP_plotId == 67)
                                            {
                                                echo 'PLOT 55 A';
                                            }
                                            else
                                            {
                                                echo 'PLOT '.$sp->SP_plotId;
                                            }
                                        }    
                                    ?>
                                </td>
                                <td>{{$sp->OP_fname.' '}} {{$sp->OP_lname}}</td>
                            </tr>
                        @endforeach
                    @endif
                   
               </table>
               {{-- {{$inventories->links()}} --}}
           </div>
    </div>
   </div>
    
   <div class="modal" tabindex="-1" role="dialog" id="addStockModal">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Add Workshop Stock </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fa fa-remove"></i></span>
              </button>
            </div>
            <form action="/addSparePart" method="POST">

                {{csrf_field()}}
            <div class="modal-body col-md-12">

                <div class="col-md-3">
                        <div class="form-group">
                                <label for="">Category</label>
                                <select name="as_category_id" id="as_category_id" class="form-control" required>
                                    <option value="">Select</option>
                                    @if(count($categories) > 0)
                                        @foreach($categories as $category)
                                            <option value="{{$category->C_id}}">{{$category->C_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                        </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                            <label for="">Model No</label>
                            <input type="text" name="as_model_no" id="as_model_no" class="form-control" placeholder="Model No" required>     </div>
                 </div>

                 <div class="col-md-4">
                    <div class="form-group">
                            <label for="">Spare Part </label>
                                <input type="text" name="as_spare_part_no" id="as_spare_part_no" class="form-control" placeholder="Spare Part " required>   
                    </div>
                 </div>

                 <div class="col-md-3">
                    <div class="form-group">
                            <label for="">Description </label>
                                <textarea type="text" name="as_description" id="as_description" class="form-control" placeholder="Description" rows="1"></textarea>  
                    </div>
                 </div>
           
                <div class="col-md-2">
                        <div class="form-group">
                                <label for="">Qty</label>
                                <input type="number" min="0" step="0.01" name="as_stock_qty" id="as_stock_qty" class="form-control" onchange="calTotal()" placeholder="qty" required>
                                <label style="display:none" id="stck_q_label1" for="">Available : <span id="stck_q1"></span></label>
                        </div>
                        
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                            <label for="">Unit Cost</label>
                            <input type="number" min="0" step="0.01" name="as_stock_unit_cost" id="as_stock_unit_cost" onchange="calTotal()" class="form-control"  placeholder="Unit" >
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                            <label for="">Total Cost</label>
                            <input type="text" name="as_stock_total_cost1" id="as_stock_total_cost1" class="form-control" placeholder="Total" disabled>
                            <input type="hidden" name="as_stock_total_cost" id="as_stock_total_cost" class="form-control"  >
                    </div>
                </div>
     
            </div>
            <input type="hidden" name="as_stock_process" id="as_stock_process" value="1">
            <input type="hidden" name="as_login_user_id" value="{{Auth::user()->id}}">
            <div class="modal-footer">
                <input type="submit" class="btn btn-success" id="submit" value="Add" />
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>    
            </div>
          </div>
        </form> 
        </div>
      </div>


      <div class="modal" tabindex="-1" role="dialog" id="utiliseStockModal">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Utilise Workshop Stock </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fa fa-remove"></i></span>
              </button>
            </div>
            <form action="/utiliseSpareParts" method="POST">
                {{csrf_field()}}
            <div class="modal-body col-md-12">

                <div class="col-md-3">
                        <div class="form-group">
                                <label for="">Category</label>
                                <select name="us_category_id" id="us_category_id" class="form-control"  required>
                                    <option value="">Select</option>
                                    @if(count($categories) > 0)
                                        @foreach($categories as $category)
                                            <option value="{{$category->C_id}}">{{$category->C_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                        </div>
                </div>
                
                <div class="col-md-3">
                        <div class="form-group">
                                <label for="">Model No</label>
                                <input type="text" name="us_model_no" id="us_model_no" class="form-control" placeholder="Model No" required>     </div>
                     </div>
    
                     <div class="col-md-3">
                        <div class="form-group">
                                <label for="">Spare Part </label>
                                    <select  name="us_spare_part_no" id="us_spare_part_no" onchange="setSPValues(this.value)" class="form-control" required> 
                                        <option value="">Select SparePart</option>
                                        @if(count($sparepartsNames) > 0)
                                        @foreach($sparepartsNames as $sparepart)
                                            <option value="{{$sparepart->SP_no}}">{{$sparepart->SP_no}}</option>
                                        @endforeach
                                    @endif
                                    </select>
                        </div>
                     </div>
    
                     <div class="col-md-3">
                        <div class="form-group">
                                <label for="">Description </label>
                                    <textarea type="text" name="us_description" id="us_description" class="form-control" placeholder="Description" rows="1"></textarea>  
                        </div>
                     </div>
                
                <input type="hidden" name="us_login_user_id" value="{{Auth::user()->id}}">

                <div class="col-md-2">
                        <div class="form-group">
                                <label for="us_stock_qty">Qty</label>
                                <input type="number" min="0" step="0.01" name="us_stock_qty" id="us_stock_qty" class="form-control" placeholder="qty" required>
                        </div>
                        <label style="display:none" id="stck_q_label2" for="">Available : <span id="stck_q2"></span></label>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                            <label for="">Unit Cost</label>
                            <input type="number" min="0" step="0.01" name="us_stock_unit_cost" id="us_stock_unit_cost" onchange="calTotal()" class="form-control"  placeholder="Unit" >
                    </div>
                </div>


                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="margin-bottom:5px;" id="gc_plots_div">
                        <label for="us_plots" >PLOT ( Ctrl + )</label>
                        <select name="us_plots[]" id="us_plots" class="form-control" onchange="check_selection(1)"  required multiple>
                            @for($i = 1 ; $i <= 66 ; $i++)
                                <option value="{{$i}}">PLOT - {{$i}}</option>
                                @if($i == 55)
                                    <option value="67">PLOT - 55 A</option>
                                @endif
                            @endfor
                    </select>                        
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                        <label for="us_plots" >Select All</label><br />&emsp;
                        <input type="checkbox" onclick="check_selection(2)" id="us_all_plots" name="us_all_plots">
                    </div>

                    <div class="col-md-2">
                            <div class="form-group">
                                    <label for="">Registration No.</label>
                                    <input type="text" name="us_regNo" id="us_regNo"  class="form-control"  placeholder="Reg No" required>
                        </div>
                    </div>

                    <div class="col-md-2">
                            <div class="form-group">
                                    <label for="">Operator</label>
                                <select name="us_operator" id="us_operator" class="form-control"  >
                                    <option>Select Operator</option>
                                    @if(count($operators) > 0)
                                    @foreach($operators as $operator)
                                        <option value="{{$operator->OP_id}}">{{$operator->OP_fname}} {{$operator->OP_lname}}</option>
                                    @endforeach
                                @endif
                                </select>
                        </div>
                    </div>
     
            </div>

            <input type="hidden" name="us_stock_process" id="us_stock_process" value="2">
            <input type="hidden" name="us_login_user_id" value="{{Auth::user()->id}}">
            <div class="modal-footer">
                <input type="submit" class="btn btn-danger" id="submit" value="Utilise" />
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>    
            </div>
          </div>
        </form>
        </div>
      </div>
    
@endsection
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script>

    function check_selection(from) {
        var result = [];
        if(from == 1)
        {
            if($('#us_all_plots').prop('checked') ==  true){
                $('#us_all_plots').prop('checked',false);
            }
            // $('#gc_plots option:selected').css('background-color','rgb(60, 141, 188)');
            
        } 
        else if (from == 2)
        {
            if($('#us_all_plots').prop('checked') == true)
            {
                $('#us_plots option').attr('selected',true);
                for(var i = 0; i < 68; i++)
                {
                    result.push(i);
                }
                $('#us_plots').val(result);
            }
            else{
                $('#us_plots option').attr('selected',false);
                $('#us_plots').val('');
            }
        }   
    }

    function calTotal()
    {
        let unit_cost = $('#as_stock_unit_cost').val();
        let qty = $('#as_stock_qty').val();
        let total = unit_cost * qty;
        $('#as_stock_total_cost').val(total);
        total = (total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'); 
        $('#as_stock_total_cost1').val(total);
       
    }

    function setCost(val)
    {
        const opvalue = document.getElementById('as_item_id').options[document.getElementById('as_item_id').selectedIndex].text;
        const unit_cost = opvalue.split('-');
        $('#as_stock_unit_cost').val(unit_cost[1]);
        get_avail_stock(val,1);
    }

  

        function setSPValues(sp){
            
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    url: "{{ url('/getSPValues') }}",
                    method: 'post',
                    data: {
                        id: sp,
                    },
                    success: function(result){
                
                        $.each(result.spareparts, function(i,item){
                            if(item){
                                $('#us_category_id').val(item.SP_cateId);
                                $('#us_model_no').val(item.SP_modelNo);
                                $('#us_description').val(item.SP_desc);
                                $('#us_stock_unit_cost').val(item.SP_unitPrice);
                                
                                $('#stck_q_label2').css('display','block');
                                $('#stck_q2').html(item.SP_currQty);
                                $('#stck_q2').css('color','green');
                            }
                        })
                        

                    }
                });
            }

            $(document).ready(function(){
            $("#from_d").datepicker({
                    format:'dd-mm-yyyy',
                    autoclose: true,
                    orientation: 'bottom'
            });
            $("#to_d").datepicker({
                    format:'dd-mm-yyyy',
                    autoclose: true,
                    orientation: 'bottom'
            });

        })

        function print_inv()
        {
            var myWindow = window.open('','','width=1024,height=800');
                var  content = '<div id="div_print"  >';
                    content +=  $('#sparepart-table').html();
                    content += '</div>';

                    content += '<style type="text/css">' +
            'table,table th, table td {' +
            'border:1px solid #000;' +
            'border-collapse:collapse;text-align:center;     }' +
            'table tr td:nth-child(2), tr th:nth-child(2) { width:20%;}' +
            '</style>';
                
        
            // $('#div_print table tr td').css('border','1px solid #000');
            // $('#div_print table tr td').css('font-size','5px');
            myWindow.document.write(content);
            myWindow.print();
            myWindow.close();
        }

        function onlyShow()
        {
            const show = $('#show').val();
            $('#onlyShowValue').val(show);

            $('#filter').click();
            
        }

</script>