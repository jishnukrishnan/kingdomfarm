@extends('home')

@section('content')

	
<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-50 p-b-90">
                <form class="login100-form validate-form flex-sb flex-w" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
					<span class="login100-form-title p-b-51">
						Register
					</span>

					
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Name
                     is required">
						<input class="input100" type="text" name="name" placeholder="Name" value="{{ old('name') }}" required="">
						<span class="focus-input100"></span>
					</div>
					
                    <div class="wrap-input100 validate-input m-b-16" data-validate = "Email is required">
                        <input class="input100" type="text" name="email" placeholder="Email" value="{{ old('email') }}" required="">
                        <span class="focus-input100"></span>
                    </div>
					
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password" required="">
						<span class="focus-input100"></span>
                    </div>
                    
                    {{ $errors->first('password') }}

                    <div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
                       <input id="password-confirm" type="password" class="input100" name="password_confirmation" placeholder="Confirm Password" required>
                        <span class="focus-input100"></span>
                    </div>
					
			

					<div class="container-login100-form-btn m-t-17">
						<button class="login100-form-btn">
							Register
						</button>
					</div>

                </form>
                
                <div class="register-link">
                    <a href="{{ route('login') }}" >
                        Already have account ? Click to Login
                    </a>
                </div>


			</div>
		</div>
	</div>
	


@endsection
