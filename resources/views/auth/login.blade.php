@extends('home')

@section('content')

                    

    <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100 p-t-50 p-b-90">
                    {{-- <form class="login100-form validate-form flex-sb flex-w"> --}}
                        <span class="login100-form-title p-b-51">
                            Login
                        </span>
    
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                
                        <div class="wrap-input100 validate-input m-b-16 form-group{{ $errors->has('email') ? ' has-error' : '' }}" data-validate = "Username is required">
                            <input id="email" type="email" class="form-control input100" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
                            @if ($errors->has('email'))
                                <span class="focus-input100">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        
                        
                        <div class="wrap-input100 validate-input m-b-16 form-group{{ $errors->has('password') ? ' has-error' : '' }}" data-validate = "Password is required">
                                <input id="password" type="password" class="form-control input100" name="password" placeholder="Password" required>
                            
                                @if ($errors->has('password'))
                                    <span class="focus-input100">{{ $errors->first('password') }}</span>
                                @endif
                        </div>
                        
                     
    
                        <div class="container-login100-form-btn m-t-17">
                            <button type="submit" class="login100-form-btn">
                                Login
                            </button>
                        </div>
                        
                    </form>

                        {{-- <div class="register-link">
                            <a href="{{ route('register') }}" >
                                New user ? Click to Register
                            </a>
                        </div> --}}
                </div>
            </div>
        </div>

@endsection
