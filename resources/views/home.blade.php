{{-- @extends('layouts.app')

@section('content')
<div class="container-fluid" id="farm-image">
    <img src="{{ asset('farm.jpg') }}"  alt="Farm Image">
</div>
    
@endsection --}}


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Zeus Farm</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	

	<link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
<!--===============================================================================================-->
	{{-- <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.css')}}"> --}}

	<link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
<!--===============================================================================================-->
</head>
<body>
	
  @yield('content')

  <div id="dropDownSelect1"></div>
  
<!--===============================================================================================-->
	<script src="{{ asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!--===============================================================================================-->


</body>
</html>