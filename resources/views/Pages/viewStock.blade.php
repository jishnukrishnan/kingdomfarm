@extends('layouts.navbar')
@section('content')

   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Inventory <i class="fa fa-angle-double-right" aria-hidden="true"></i> View Stock</h2>

           <div class="row col-md-12 table-responsive">
               <table class="table table-bordered text-center table-data-tr ">
                    <tr>
                        <th>Sl.No</th>
                        <th>Ledger <form action="/makeCategoryAsc" id="cat_asc_form" method="post" style="display:inline;">{{csrf_field()}}<a href="#" class="pull-right" onclick="document.getElementById('cat_asc_form').submit();"><i class="fa fa-sort-alpha-asc" aria-hidden="true"></i></a></button></form></th>
                        <th>Stock Name <form action="/makeItemAsc" id="item_asc_form" method="post" style="display:inline;">{{csrf_field()}}<a href="#" class="pull-right" onclick="document.getElementById('item_asc_form').submit();"><i class="fa fa-sort-alpha-asc" aria-hidden="true"></i></a></button></form></th>
                        <th>Avail Stock</th>
                        <th>Unit </th>
                      
                    </tr>
                    @if(count($stock_items) > 0)
                    @php
                        $i = 1 ;

                      
                    @endphp
                    @foreach($stock_items as $item)
                    
                    <tr>
                        <td style="width:5%;">{{$i}}</td>
                        <td class="left">{{$item->category_name}}</td>
                        <td class="left">{{$item->item_name}}</td>
                        <td class="">{{$item->stock_qty}}</td>
                        <td class="">{{$item->unit_name}}</td>
                       
                        @php
                            $i++;
                        @endphp  
                    </tr>                           
                    @endforeach
                @endif
   
               </table>
           </div>
    </div>
   </div>
    
   <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Stock Item ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="del_text"></p>
            </div>
            <div class="modal-footer">
                <form action="/deleteStockItem" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" id="del_id" name="del_id">
                    <input type="submit" class="btn btn-primary" value="Delete" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    
@endsection

<script>
    function update_stockitem(stock,category,unit_id,id)
    {   
        $stock_name = stock.split('-');
        document.getElementById('stock_category').value = category;
        document.getElementById('stock_name').value = $stock_name[0];
        document.getElementById('stock_price').value = $stock_name[1];
        document.getElementById('stock_unit').value = unit_id ;
        document.getElementById('stock_id').value = id;
        document.getElementById('submit').value = "Update";
        document.getElementById('submit').innerHTML = "Update";
    }

    function delete_stockitem(stock,id) {
        document.getElementById('del_text').innerHTML = 'Are you sure to delete Stock Item named '+stock+'?';
        document.getElementById('del_text').style.color = 'red';
        document.getElementById('del_id').value = id ;
    }
</script>