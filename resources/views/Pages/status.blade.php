@extends('layouts.navbar')
<style>

</style>
@section('content')
   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Manage <i class="fa fa-angle-double-right" aria-hidden="true"></i> Status</h2>

        <div class="submit-form">
           
           <form action="/status_ins" method="POST">
                {{csrf_field()}}
                   <div class="form-group">
                        <div class="col-md-4 col-sm-8 col-xs-8">
                                <input type="text" class="form-control " name="status_name" id="status_name" placeholder="Status Name" required>                              
                        </div>
                        <input type="hidden" id="status_id" name="status_id">
                        <div class="col-md-1 col-sm-4">
                                <input type="submit" class="btn btn-primary " id="submit" name="submit" value="Add"> 
                        </div>
                   </div>
                </form>
        </div>
        
           <div class="row col-md-12 table-responsive">
               <table class="table table-bordered text-center">
                    <tr>
                        <th>Sl.No</th>
                        <th>Status Name</th>
                        {{-- <th>Action</th> --}}
                    </tr>
                    
                        @if(count($status) > 0)
                            @php
                                $i = 1 ;
                            @endphp
                            @foreach($status as $stat)
                            <tr>
                                <td style="width:5%;">{{$i}}</td>
                                <td  style="width:75%;">{{$stat->status}}</td>
                                
                                {{-- <td style="width:20%;">
                                        <button class="btn btn-warning" type="button" onClick="update_status('{{$stat->status}}',{{$stat->id}})">Edit</button>
                                        <button class="btn btn-danger" type="button" onClick="delete_status('{{$stat->status}}',{{$stat->id}})" data-toggle="modal" data-target="#deleteModal">Delete</button>
                              
                                </td>  --}}
                                @php
                                    $i++;
                                @endphp  
                            </tr>                           
                            @endforeach
                        @endif
                   
               </table>
           </div>
    </div>
   </div>
    
   <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Status ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="del_text"></p>
            </div>
            <div class="modal-footer">
                <form action="/deleteStatus" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" id="del_id" name="del_id">
                    <input type="submit" class="btn btn-primary" value="Delete" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    
@endsection

<script>
    function update_status(status,id)
    {
        document.getElementById('status_name').value = status;
        document.getElementById('status_id').value = id;
        document.getElementById('submit').value = "Update";
        document.getElementById('submit').innerHTML = "Update";
    }

    function delete_status(status,id) {
        document.getElementById('del_text').innerHTML = 'Are you sure to delete '+status+'?';
        document.getElementById('del_id').value = id ;
    }
</script>