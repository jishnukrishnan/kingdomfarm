@extends('layouts.navbar')
<style>
    .mkyellow {
        color:#f0ad4e !important;
        border-color:#d58512 !important;
    }
    .mkred {
        color:#c9302c;
        border-color:#ac2925;
    }
</style>
@section('content')

   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Manage <i class="fa fa-angle-double-right" aria-hidden="true"></i> Stock Items</h2>
    <div class="submit-form col-md-12 col-sm-12">
           <form action="/stock_ins" method="POST">
                {{csrf_field()}}
                    <div class="col-lg-3 col-md-3 col-sm-4  col-xs-4">
                            <select  class="form-control " name="stock_category" id="stock_category" required>    
                                <option value="">Select Stock Ledger</option>
                                @if(count($categories) > 0)
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                @endif
                            </select>                        
                    </div>
                    <input type="hidden" name="login_user_id" value="{{Auth::user()->id}}">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                            <input type="text" class="form-control " name="stock_name" id="stock_name" placeholder="Stock Item Name" required>                              
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                        <input type="number" class="form-control " name="stock_price" id="stock_price" step="0.0001" placeholder=" Item Price( Per Unit )" required>                              
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                        <select name="stock_unit" id="stock_unit" class="form-control" required>
                            <option value="">Select Unit</option>
                            @if(count($units) > 0)
                                @foreach($units as $unit)
                                    <option value="{{$unit->id}}">{{$unit->unit_name}}</option>
                                @endforeach
                            @endif
                    </select>                        
                    </div>
                    <input type="hidden" id="stock_id" name="stock_item_id">
                    <div class="col-md-1 col-sm-2 col-xs-2">
                            <input type="submit" class="btn btn-primary " id="submit" name="submit" value="Add"> 
                    </div>
                </form>
           </div>
           <div class="row col-md-12 table-responsive">
               <table class="table table-bordered text-center table-data-tr">
                    <tr>
                        <th>Sl.No</th>
                        <th>Ledger</th>
                        <th>Stock Name</th>
                        <th>Avail Stock</th>
                        <th>Unit </th>
                        <th>Action</th>
                    </tr>
                    @if(count($stock_items) > 0)
         
                    @foreach($stock_items as $item)
                    
                    <tr>
                        <td style="width:5%;">{{$item->id}}</td>
                        <td >{{$item->category_name}}</td>
                        <td style="text-align:left">{{$item->item_name}}</td>
                        <td class="">{{$item->stock_qty}}</td>
                        <td >{{$item->unit_name}}</td>
                        <td style="width:20%;">
                            <a  href="#" onClick="update_stockitem('{{$item->item_name}}','{{$item->cate_id}}',{{$item->unit_id}},{{$item->id}})"><i class="fa fa-pencil fa-2x mkyellow" aria-hidden="true"></i></a>
                            <a href="#" onClick="delete_stockitem('{{$item->item_name}}',{{$item->id}})" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-times fa-2x mkred" aria-hidden="true"></i></a>
                      
                        </td> 
                  
                    </tr>                           
                    @endforeach
                @endif
                     
                  
               </table>
           </div>
    </div>
   </div>
    
   <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Stock Item ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="del_text"></p>
            </div>
            <div class="modal-footer">
                <form action="/deleteStockItem" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" id="del_id" name="del_id">
                    <input type="submit" class="btn btn-primary" value="Delete" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    
@endsection

<script>
    function update_stockitem(stock,category,unit_id,id)
    {   
        $stock_name = stock.split('-');
        document.getElementById('stock_category').value = category;
        document.getElementById('stock_name').value = $stock_name[0];
        document.getElementById('stock_price').value = $stock_name[1];
        document.getElementById('stock_unit').value = unit_id ;
        document.getElementById('stock_id').value = id;
        document.getElementById('submit').value = "Update";
        document.getElementById('submit').innerHTML = "Update";
    }

    function delete_stockitem(stock,id) {
        document.getElementById('del_text').innerHTML = 'Are you sure to delete Stock Item named '+stock+'?';
        document.getElementById('del_text').style.color = 'red';
        document.getElementById('del_id').value = id ;
    }
</script>