@extends('layouts.app')
<style>
    .mapArea{
        width:800px;
        height:800px;
    }
    .page-head {
        padding-left:150px;
    }
    .mapArea a {
        display:inline-block;
        height:150px;
        width:150px;    
    }

    .fieldArea {
        height:150px;
        width:150px; 
        border:1px solid #ccc;
    }

    #farm-image {
        padding:0px !important;
        margin-top:-22px;
    }

    #farm-image img{
        width:100%;
        height: 100%;
       
    }
</style>
@section('content')
<div class="container-fluid" id="farm-image">
    <img src="{{ asset('farm.jpg') }}"  alt="Farm Image">
</div>
    
@endsection