@extends('layouts.navbar')

<style>
.fc-view-container {
    border:1px solid #fff;
    width:75%;
    margin:auto;
}

.fc-day-header {
    color:#000;
}
#calendar-div div{
  height: 100vh;
}
.fc-title {
  color:#F8333C;
  font-weight: bold;
  padding-left:5%;
}
</style>

@section('content')

{{-- <div id="calendar" style="width:70%;margin:auto;"></div> --}}

<div id="calendar-div col-md-12 col-sm-12">
    {!! $calendar->calendar() !!}
</div>
{!! $calendar->script() !!}
@endsection
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script>

// $(document).ready(function(){
//   var event={id:1 , title: 'New event', start:  new Date()};
//   $('#calendar').fullCalendar({
//       editable:true,
      
//       // header:{
//       //   left:'prev,next,today',
//       //   center:'title',
//       //   right:'month'
//       // },
      
//     // dayClick: function() {
        
//     // }
//   })

// })
</script>