@extends('layouts.navbar')
 
@section('content')
    @php
        $moneySpend = array();
        for($n = 1;$n < 68; $n++)
        {
            $moneySpend[$n] = 0 ;
        }
    @endphp
    @if(count($generalCostings) > 0)
        @foreach($generalCostings as $gc)
            @php              
                $gcAmount = $gc->stock_total_cost;   
                $plots = $gc->stock_field_id;
                // $eachPlot = explode(',',$plots);
                // $noOfPlots = count($eachPlot) - 1;
                // $eachPlotAmount = ($gcAmount/$noOfPlots);
                for($n = 1;$n <= 67; $n++)
                {
                    if($n == $plots)
                    {
                        $moneySpend[$n] += (int)$gcAmount  ;
                    } 
                }
            @endphp
        @endforeach
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="mapArea col-md-12">
            {{-- <div class="showD">
                    <div id="percentTOT" class="label-center" data-preset="line" data-type="stroke" data-stroke="#E5B181" data-stroke-width="20" data-stroke-trail="#605F5E" data-stroke-trail-width="20"></div>
                    <div id="gcTOT" ></div>
            </div> --}}
        <div class="mapDiv" >
 
        <svg width="1592" height="1521" viewBox="0 0 1592 1521" fill="none" xmlns="http://www.w3.org/2000/svg" class="img-responsive">


            <g>
                    <a href="/fields/66"> <path d="M93.2212 191L6 289V329H230V219.053V121L93.2212 191Z" fill="white"/></a>
                    <text x="135" y="220" fill="red" stroke="#f00" >Plot 66</text>
                    
                    <line x1="120" y1="245" x2="200" y2="245" style="stroke:#ccc;stroke-width:20" />  
                    @php
                        $per66 = $percent[66];
                        $percent66 = ($percent[66]/100)*80 + 120 ; 
                    @endphp 
                    <line x1="120" y1="245" x2="<?php echo $percent66;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                    <text x="145" y="250" fill="red" stroke="#000" opacity="1" >@php  echo $per66.' %' ; @endphp </text>
                    <text x="120" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[66]) ; @endphp </text> 
                </g>
            <g>
                <a href="/fields/65">  <path d="M303.297 83L343 59V329H230V125.919L303.297 83Z" fill="white"/></a>
                <text x="265" y="220" fill="red" stroke="#f00" >Plot 65</text>
                
                <line x1="250" y1="245" x2="330" y2="245" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per65 = $percent[65];
                    $percent65 = ($percent[65]/100)*80 + 250 ; 
                @endphp 
                <line x1="250" y1="245" x2="<?php echo $percent65;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="275" y="250" fill="red" stroke="#000" opacity="1" >@php  echo $per65.' %' ; @endphp </text>
                <text x="250" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[65]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/64"><path d="M437.524 1L456 15.2174V328H343V55.8385L437.524 1Z" fill="white"/></a>
                <text x="375" y="220" fill="red" stroke="#f00" >Plot 64</text>
                
                <line x1="360" y1="245" x2="440" y2="245" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per64 = $percent[64];
                    $percent64 = ($percent[64]/100)*80 + 360 ; 
                @endphp 
                <line x1="360" y1="245" x2="<?php echo $percent64;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="385" y="250" fill="red" stroke="#000" opacity="1">@php  echo $per64.' %' ; @endphp </text>
                <text x="360" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[64]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/63"> <path d="M548 45L569 57V328H456V11L548 45Z" fill="white"/></a>
                <text x="485" y="220" fill="red" stroke="#f00" >Plot 63</text>
                
                <line x1="470" y1="245" x2="550" y2="245" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per63 = $percent[63];
                    $percent63 = ($percent[63]/100)*80 + 470 ; 
                @endphp 
                <line x1="470" y1="245" x2="<?php echo $percent63;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="495" y="250" fill="red" stroke="#000" opacity="1">@php  echo $per63.' %' ; @endphp </text>
                <text x="470" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[63]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/62"><path d="M595.709 69L682 151V328H569V59L595.709 69Z" fill="white"/></a>
                <text x="595" y="220" fill="red" stroke="#f00" >Plot 62</text>
                
                <line x1="580" y1="245" x2="660" y2="245" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per62 = $percent[62];
                    $percent62 = ($percent[62]/100)*80 + 580 ; 
                @endphp 
                <line x1="580" y1="245" x2="<?php echo $percent62;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="605" y="250" fill="red" stroke="#000" opacity="1">@php  echo $per62.' %' ; @endphp </text>
                <text x="580" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[62]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/61"><path d="M738 111L795 81V328H682V151L738 111Z" fill="white"/></a>
                <text x="710" y="220" fill="red" stroke="#f00" >Plot 61</text>
                
                <line x1="695" y1="245" x2="775" y2="245" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per61 = $percent[61];
                    $percent61 = ($percent[61]/100)*80 + 695 ; 
                @endphp 
                <line x1="695" y1="245" x2="<?php echo $percent61;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="720" y="250" fill="red" stroke="#000" opacity="1">@php  echo $per61.' %' ; @endphp </text>
                <text x="695" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[61]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/60"><path d="M816 65L908 55V329H795V82L816 65Z" fill="white"/></a>
                <text x="825" y="220" fill="red" stroke="#f00" >Plot 60</text>
                
                <line x1="810" y1="245" x2="890" y2="245" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per60 = $percent[60];
                    $percent60 = ($percent[60]/100)*80 + 810 ; 
                @endphp 
                <line x1="810" y1="245" x2="<?php echo $percent60;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="835" y="250" fill="red" stroke="#000" opacity="1">@php  echo $per60.' %' ; @endphp </text>
                <text x="810" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[60]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/59"><path d="M952 55L1021 43V329H908V65L952 55Z" fill="white"/></a>
                <text x="935" y="220" fill="red" stroke="#f00" >Plot 59</text>
                
                <line x1="920" y1="245" x2="1000" y2="245" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per59 = $percent[59];
                    $percent59 = ($percent[59]/100)*80 + 920 ; 
                @endphp 
                <line x1="920" y1="245" x2="<?php echo $percent59;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="945" y="250" fill="red" stroke="#000" opacity="1">@php  echo $per59.' %' ; @endphp </text>
                <text x="920" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[59]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/58"><path d="M1082 42L1134 31V328H1021V42H1082Z" fill="white"/></a>
                <text x="1055" y="220" fill="red" stroke="#f00" >Plot 58</text>
                
                <line x1="1040" y1="245" x2="1120" y2="245" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per58 = $percent[58];
                    $percent58 = ($percent[58]/100)*80 + 1040 ; 
                @endphp 
                <line x1="1040" y1="245" x2="<?php echo $percent58;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="1065" y="250" fill="red" stroke="#000" opacity="1">@php  echo $per58.' %' ; @endphp </text>
                <text x="1040" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[58]) ; @endphp </text> 
            </g>

            <g>
                    <a href="/fields/57">  <path d="M1162 29L1247 43V329H1134V43L1162 29Z" fill="white"/></a>
                    <text x="1175" y="220" fill="red" stroke="#f00" >Plot 57</text>
                    
                    <line x1="1155" y1="245" x2="1225" y2="245" style="stroke:#ccc;stroke-width:20" />  
                    @php
                        $per57 = $percent[57];
                        $percent57 = ($percent[57]/100)*80 + 1155 ; 
                    @endphp 
                    <line x1="1155" y1="245" x2="<?php echo $percent57;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                    <text x="1175" y="250" fill="red" stroke="#000" opacity="1">@php  echo $per57.' %' ; @endphp </text>
                    <text x="1155" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[57]) ; @endphp </text> 
                </g>

                <g>
                        <a href="/fields/56">   <path d="M1272.03 55L1348 67.7867L1592 235.659L1486 269.784L1394 299.894L1364 329H1247V192V55H1272.03Z" fill="white"/></a>
                        <text x="1290" y="220" fill="red" stroke="#f00" >Plot 56</text>
                        
                        <line x1="1280" y1="245" x2="1360" y2="245" style="stroke:#ccc;stroke-width:20" />  
                        @php
                            $per56 = $percent[56];
                            $percent56 = ($percent[56]/100)*80 + 1280 ; 
                        @endphp 
                        <line x1="1280" y1="245" x2="<?php echo $percent56;?>" y2="245" style="stroke:rgb(60,141,188);stroke-width:20" />
                        <text x="1300" y="250" fill="red" stroke="#000" opacity="1">@php  echo $per56.' %' ; @endphp </text>
                        <text x="1280" y="280" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[56]) ; @endphp </text> 
                    </g>

           
            <g>
                    <a href="/fields/67"> <path d="M230 329V425H157.264H52L37.7682 401L22 369L0 329H230Z" fill="white"/></a>
                    <text x="135" y="350" fill="red" stroke="#f00" >Plot 55 A</text>
                    
                    <line x1="120" y1="380" x2="200" y2="380" style="stroke:#ccc;stroke-width:20" />  
                    @php
                        $per67 = $percent[67];
                        $percent67 = ($percent[67]/100)*80 + 120 ; 
                    @endphp 
                    <line x1="120" y1="380" x2="<?php echo $percent67;?>" y2="380" style="stroke:rgb(60,141,188);stroke-width:20" />
                    <text x="145" y="385" fill="red" stroke="#000" opacity="1" >@php  echo $per67.' %' ; @endphp </text>
                    <text x="120" y="410" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[67]) ; @endphp </text> 
                </g>
            <g>
                <a href="/fields/55"><path d="M230 425.684L230.214 328L343.172 328.248L342.958 425.933L230 425.684Z" fill="white"/></a>
                <text x="265" y="350" fill="red" stroke="#f00" >Plot 55</text>
                
                <line x1="250" y1="380" x2="330" y2="380" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per55 = $percent[55];
                    $percent55 = ($percent[55]/100)*80 + 250 ; 
                @endphp 
                <line x1="250" y1="380" x2="<?php echo $percent55;?>" y2="380" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="275" y="385" fill="red" stroke="#000" opacity="1" >@php  echo $per55.' %' ; @endphp </text>
                <text x="250" y="410" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[55]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/54"><path d="M342.959 425.684L343.206 328L456.165 328.215L455.918 425.899L342.959 425.684Z" fill="white"/></a>
                <text x="375" y="350" fill="red" stroke="#f00" >Plot 54</text>
                
                <line x1="360" y1="380" x2="440" y2="380" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per54 = $percent[54];
                    $percent54 =($percent[54]/100)*80+ 360 ; 
                @endphp 
                <line x1="360" y1="380" x2="<?php echo $percent54;?>" y2="380" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="385" y="385" fill="red" stroke="#000" opacity="1">@php  echo $per54.' %' ; @endphp </text>
                <text x="360" y="410" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[54]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/53"> <path d="M455.918 425.684L456.165 328L569.124 328.215L568.876 425.899L455.918 425.684Z" fill="white"/></a>
                <text x="485" y="350" fill="red" stroke="#f00" >Plot 53</text>
                
                <line x1="470" y1="380" x2="550" y2="380" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per53 = $percent[53];
                    $percent53 = ($percent[53]/100)*80 + 470 ; 
                @endphp 
                <line x1="470" y1="380" x2="<?php echo $percent53;?>" y2="380" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="495" y="385" fill="red" stroke="#000" opacity="1">@php  echo $per53.' %' ; @endphp </text>
                <text x="470" y="410" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[53]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/52"> <path d="M568.876 425.684L569.124 328L682.082 328.215L681.835 425.899L568.876 425.684Z" fill="white"/></a>
                <text x="595" y="350" fill="red" stroke="#f00" >Plot 52</text>
                
                <line x1="580" y1="380" x2="660" y2="380" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per52 = $percent[52];
                    $percent52 = ($percent[52]/100)*80 + 580 ; 
                @endphp 
                <line x1="580" y1="380" x2="<?php echo $percent52;?>" y2="380" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="605" y="385" fill="red" stroke="#000" opacity="1">@php  echo $per52.' %' ; @endphp </text>
                <text x="580" y="410" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[52]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/51"><path d="M681.835 425.684L682.082 328L795.041 328.215L794.794 425.899L681.835 425.684Z" fill="white"/></a>
                <text x="710" y="350" fill="red" stroke="#f00" >Plot 51</text>
                
                <line x1="695" y1="380" x2="775" y2="380" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per51 = $percent[51];
                    $percent51 =($percent[51]/100)*80 + 695 ; 
                @endphp 
                <line x1="695" y1="380" x2="<?php echo $percent51;?>" y2="380" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="720" y="385" fill="red" stroke="#000" opacity="1">@php  echo $per51.' %' ; @endphp </text>
                <text x="695" y="410" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[51]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/50"><path d="M794.794 425.684L795.041 328L908 328.215L907.753 425.899L794.794 425.684Z" fill="white"/></a>
                <text x="825" y="350" fill="red" stroke="#f00" >Plot 50</text>
                
                <line x1="810" y1="380" x2="890" y2="380" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per50 = $percent[50];
                    $percent50 = ($percent[50]/100)*80 + 810 ; 
                @endphp 
                <line x1="810" y1="380" x2="<?php echo $percent50;?>" y2="380" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="835" y="385" fill="red" stroke="#000" opacity="1">@php  echo $per50.' %' ; @endphp </text>
                <text x="810" y="410" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[50]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/49"><path d="M907.997 424.84L908.24 328.841L1021.2 329.056L1020.96 425.055L907.997 424.84Z" fill="white"/></a>
                <text x="935" y="349" fill="red" stroke="#f00" >Plot 49</text>
                
                <line x1="920" y1="380" x2="1000" y2="380" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per49 = $percent[49];
                    $percent49 = ($percent[49]/100)*80 + 920 ; 
                @endphp 
                <line x1="920" y1="380" x2="<?php echo $percent49;?>" y2="380" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="945" y="385" fill="red" stroke="#000" opacity="1">@php  echo $per49.' %' ; @endphp </text>
                <text x="920" y="410" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[49]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/48"><path d="M1020.99 426.873L1021.24 328.884L1134.2 329.099L1133.95 427.087L1020.99 426.873Z" fill="white"/></a>
                <text x="1055" y="349" fill="red" stroke="#f00" >Plot 48</text>
                
                <line x1="1040" y1="380" x2="1120" y2="380" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per48 = $percent[48];
                    $percent48 = ($percent[48]/100)*80 + 1040 ; 
                @endphp 
                <line x1="1040" y1="380" x2="<?php echo $percent48;?>" y2="380" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="1065" y="385" fill="red" stroke="#000" opacity="1">@php  echo $per48.' %' ; @endphp </text>
                <text x="1040" y="410" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[48]) ; @endphp </text> 
            </g>

            <g>
                    <a href="/fields/47"> <path d="M1135 328C1134.45 328 1134 328.448 1134 329V424C1134 424.552 1134.45 425 1135 425H1297.57C1297.85 425 1298.11 424.888 1298.3 424.69L1337.91 383.095C1337.97 383.032 1338.02 382.961 1338.06 382.884L1367.19 329.479C1367.56 328.812 1367.07 328 1366.32 328H1135Z" fill="white"/></a>
                    <text x="1190" y="349" fill="red" stroke="#f00" >Plot 47</text>
                    
                    <line x1="1175" y1="380" x2="1255" y2="380" style="stroke:#ccc;stroke-width:20" />  
                    @php
                        $per47 = $percent[47];
                        $percent47 = ($percent[47]/100)*80 + 1175 ; 
                    @endphp 
                    <line x1="1175" y1="380" x2="<?php echo $percent47;?>" y2="380" style="stroke:rgb(60,141,188);stroke-width:20" />
                    <text x="1200" y="385" fill="red" stroke="#000" opacity="1">@php  echo $per47.' %' ; @endphp </text>
                    <text x="1175" y="410" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[47]) ; @endphp </text> 
                </g>

                
               
                <g>
                        <a href="/fields/37"> <path d="M230 425V621H179.6H76V571L96.8 507L54 425H230Z" fill="white"/></a>
                        <text x="135" y="450" fill="red" stroke="#f00" >Plot 37</text>
                        
                        <line x1="120" y1="480" x2="200" y2="480" style="stroke:#ccc;stroke-width:20" />  
                        @php
                            $per37 = $percent[37];
                            $percent37 = ($percent[37]/100)*80 + 120 ; 
                        @endphp 
                        <line x1="120" y1="480" x2="<?php echo $percent37;?>" y2="480" style="stroke:rgb(60,141,188);stroke-width:20" />
                        <text x="145" y="485" fill="red" stroke="#000" opacity="1">@php  echo $per37.' %' ; @endphp </text>
                        <text x="120" y="510" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[37]) ; @endphp </text> 
                    </g>
            <g>
                <a href="/fields/46"><path d="M230 523.369L230.214 425.684L343.172 425.933L342.958 523.617L230 523.369Z" fill="white"/></a>
                <text x="265" y="450" fill="red" stroke="#f00" >Plot 46</text>
                
                <line x1="250" y1="480" x2="330" y2="480" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per46 = $percent[46];
                    $percent46 = ($percent[46]/100)*80 + 250 ; 
                @endphp 
                <line x1="250" y1="480" x2="<?php echo $percent46;?>" y2="480" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="275" y="485" fill="red" stroke="#000" opacity="1">@php  echo $per46.' %' ; @endphp </text>
                <text x="250" y="510" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[46]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/45"><path d="M342.959 523.369L343.206 425.684L456.165 425.899L455.918 523.583L342.959 523.369Z" fill="white"/></a>
                <text x="375" y="450" fill="red" stroke="#f00" >Plot 45</text>
                
                <line x1="360" y1="480" x2="440" y2="480" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per45 = $percent[45];
                    $percent45 = ($percent[45]/100)*80 + 360 ; 
                @endphp 
                <line x1="360" y1="480" x2="<?php echo $percent45;?>" y2="480" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="385" y="485" fill="red" stroke="#000" opacity="1">@php  echo $per45.' %' ; @endphp </text>
                <text x="360" y="510" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[45]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/44"><path d="M455.918 523.369L456.165 425.684L569.124 425.899L568.876 523.583L455.918 523.369Z" fill="white"/></a>
                <text x="485" y="450" fill="red" stroke="#f00" >Plot 44</text>
                
                <line x1="470" y1="480" x2="550" y2="480" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per44 = $percent[44];
                    $percent44 = ($percent[44]/100)*80 + 470 ; 
                @endphp 
                <line x1="470" y1="480" x2="<?php echo $percent44;?>" y2="480" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="495" y="485" fill="red" stroke="#000" opacity="1">@php  echo $per44.' %' ; @endphp </text>
                <text x="470" y="510" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[44]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/43"> <path d="M568.876 523.369L569.124 425.684L682.082 425.899L681.835 523.583L568.876 523.369Z" fill="white"/></a>
                <text x="595" y="450" fill="red" stroke="#f00" >Plot 43</text>
                
                <line x1="580" y1="480" x2="660" y2="480" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per43 = $percent[43];
                    $percent43 = ($percent[43]/100)*80 + 580 ; 
                @endphp 
                <line x1="580" y1="480" x2="<?php echo $percent43;?>" y2="480" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="605" y="485" fill="red" stroke="#000" opacity="1">@php  echo $per43.' %' ; @endphp </text>
                <text x="580" y="510" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[43]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/42">  <path d="M681.835 523.369L682.082 425.684L795.041 425.899L794.794 523.583L681.835 523.369Z" fill="white"/></a>
                <text x="710" y="450" fill="red" stroke="#f00" >Plot 42</text>
                
                <line x1="695" y1="480" x2="775" y2="480" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per42 = $percent[42];
                    $percent42 = ($percent[42]/100)*80 + 695 ; 
                @endphp 
                <line x1="695" y1="480" x2="<?php echo $percent42;?>" y2="480" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="720" y="485" fill="red" stroke="#000" opacity="1">@php  echo $per42.' %' ; @endphp </text>
                <text x="695" y="510" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[42]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/41"><path d="M794.794 523.369L795.041 425.684L908 425.899L907.753 523.583L794.794 523.369Z" fill="white"/></a>
                <text x="825" y="450" fill="red" stroke="#f00" >Plot 41</text>
                
                <line x1="810" y1="480" x2="890" y2="480" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per41 = $percent[41];
                    $percent41 = ($percent[41]/100)*80 + 810 ; 
                @endphp 
                <line x1="810" y1="480" x2="<?php echo $percent41;?>" y2="480" style="stroke:rgb(60,141,188);stroke-width:20" />
                <text x="835" y="485" fill="red" stroke="#000" opacity="1">@php  echo $per41.' %' ; @endphp </text>
                <text x="810" y="510" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[41]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/40"> <path d="M908 523.684L908.247 426L1021.21 426.215L1020.96 523.899L908 523.684Z" fill="white"/></a>
                <text x="935" y="450" fill="red" stroke="#f00" >Plot 40</text>
                
                <line x1="920" y1="480" x2="1000" y2="480" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per40 = $percent[40];
                    $percent40 = ($percent[40]/100)*80 + 920 ; 
                @endphp 
                <line x1="920" y1="480" x2="<?php echo $percent40;?>" y2="480" style="stroke:rgb(60,140,188);stroke-width:20" />
                <text x="945" y="485" fill="red" stroke="#000" opacity="1">@php  echo $per40.' %' ; @endphp </text>
                <text x="920" y="510" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[40]) ; @endphp </text> 
            </g>
            <g>
                <a href="/fields/39"> <path d="M1021 523.684L1021.25 426L1134.21 426.215L1133.96 523.899L1021 523.684Z" fill="white"/></a>
                <text x="1055" y="450" fill="red" stroke="#f00" >Plot 39</text>
                
                <line x1="1040" y1="480" x2="1120" y2="480" style="stroke:#ccc;stroke-width:20" />  
                @php
                    $per39 = $percent[39];
                    $percent39 = ($percent[39]/100)*80 + 1040 ; 
                @endphp 
                <line x1="1040" y1="480" x2="<?php echo $percent39;?>" y2="480" style="stroke:rgb(60,140,188);stroke-width:20" />
                <text x="1065" y="485" fill="red" stroke="#000" opacity="1">@php  echo $per39.' %' ; @endphp </text>
                <text x="1040" y="510" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[39]) ; @endphp </text> 
            </g>

            <g>
                    <a href="/fields/38">  <path d="M1135 426C1134.45 426 1134 426.448 1134 427V522C1134 522.552 1134.45 523 1135 523H1203.57C1203.84 523 1204.11 522.885 1204.3 522.683L1262 460.92L1290.58 427.652C1291.14 427.003 1290.68 426 1289.82 426H1135Z" fill="white"/></a>
                    <text x="1160" y="450" fill="red" stroke="#f00" >Plot 38</text>
                    
                    <line x1="1150" y1="480" x2="1220" y2="480" style="stroke:#ccc;stroke-width:20" />  
                    @php
                        $per38 = $percent[38];
                        $percent38 = ($percent[38]/100)*80 + 1150 ; 
                    @endphp 
                    <line x1="1150" y1="480" x2="<?php echo $percent38;?>" y2="480" style="stroke:rgb(60,140,188);stroke-width:20" />
                    <text x="1170" y="485" fill="red" stroke="#000" opacity="1">@php  echo $per38.' %' ; @endphp </text>
                    <text x="1140" y="510" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[38]) ; @endphp </text> 
                </g>

                <g>
                        <a href="/fields/36"> <path d="M230 622.06L230.214 524.376L343.172 524.624L342.958 622.309L230 622.06Z" fill="white"/></a>
                        <text x="265" y="550" fill="red" stroke="#f00" >Plot 36</text>
                        
                        <line x1="250" y1="580" x2="330" y2="580" style="stroke:#ccc;stroke-width:20" />  
                        @php
                            $per36 = $percent[36];
                            $percent36 = ($percent[36]/100)*80 + 250 ; 
                        @endphp 
                        <line x1="250" y1="580" x2="<?php echo $percent36;?>" y2="580" style="stroke:rgb(60,141,188);stroke-width:20" />
                        <text x="275" y="585" fill="red" stroke="#000" opacity="1">@php  echo $per36.' %' ; @endphp </text>
                        <text x="250" y="610" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[36]) ; @endphp </text> 
                    </g>
                    <g>
                        <a href="/fields/35"> <path d="M342.959 622.06L343.206 524.376L456.165 524.591L455.918 622.275L342.959 622.06Z" fill="white"/></a>
                        <text x="375" y="550" fill="red" stroke="#f00" >Plot 35</text>
                        
                        <line x1="360" y1="580" x2="440" y2="580" style="stroke:#ccc;stroke-width:20" />  
                        @php
                            $per35 = $percent[35];
                            $percent35 = ($percent[35]/100)*80 + 360 ; 
                        @endphp 
                        <line x1="360" y1="580" x2="<?php echo $percent35;?>" y2="580" style="stroke:rgb(60,141,188);stroke-width:20" />
                        <text x="385" y="585" fill="red" stroke="#000" opacity="1">@php  echo $per35.' %' ; @endphp </text>
                        <text x="360" y="610" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[35]) ; @endphp </text> 
                    </g>
                    <g>
                        <a href="/fields/34"><path d="M455.918 622.06L456.165 524.376L569.124 524.591L568.876 622.275L455.918 622.06Z" fill="white"/></a>
                        <text x="485" y="550" fill="red" stroke="#f00" >Plot 34</text>
                        
                        <line x1="470" y1="580" x2="550" y2="580" style="stroke:#ccc;stroke-width:20" />  
                        @php
                            $per34 = $percent[34];
                            $percent34 = ($percent[34]/100)*80 + 470 ; 
                        @endphp 
                        <line x1="470" y1="580" x2="<?php echo $percent34;?>" y2="580" style="stroke:rgb(60,141,188);stroke-width:20" />
                        <text x="495" y="585" fill="red" stroke="#000" opacity="1">@php  echo $per34.' %' ; @endphp </text>
                        <text x="470" y="610" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[34]) ; @endphp </text> 
                    </g>
                    <g>
                        <a href="/fields/33">  <path d="M568.876 622.06L569.124 524.376L682.082 524.591L681.835 622.275L568.876 622.06Z" fill="white"/></a>
                        <text x="595" y="550" fill="red" stroke="#f00" >Plot 33</text>
                        
                        <line x1="580" y1="580" x2="660" y2="580" style="stroke:#ccc;stroke-width:20" />  
                        @php
                            $per33 = $percent[33];
                            $percent33 = ($percent[33]/100)*80 + 580 ; 
                        @endphp 
                        <line x1="580" y1="580" x2="<?php echo $percent33;?>" y2="580" style="stroke:rgb(60,141,188);stroke-width:20" />
                        <text x="605" y="585" fill="red" stroke="#000" opacity="1">@php  echo $per33.' %' ; @endphp </text>
                        <text x="580" y="610" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[33]) ; @endphp </text> 
                    </g>
                    <g>
                        <a href="/fields/32"> <path d="M681.835 622.06L682.082 524.376L795.041 524.591L794.794 622.275L681.835 622.06Z" fill="white"/></a>
                        <text x="710" y="550" fill="red" stroke="#f00" >Plot 32</text>
                        
                        <line x1="695" y1="580" x2="775" y2="580" style="stroke:#ccc;stroke-width:20" />  
                        @php
                            $per32 = $percent[32];
                            $percent32 = ($percent[32]/100)*80 + 695 ; 
                        @endphp 
                        <line x1="695" y1="580" x2="<?php echo $percent32;?>" y2="580" style="stroke:rgb(60,141,188);stroke-width:20" />
                        <text x="720" y="585" fill="red" stroke="#000" opacity="1">@php  echo $per32.' %' ; @endphp </text>
                        <text x="695" y="610" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[32]) ; @endphp </text> 
                    </g>
                    <g>
                        <a href="/fields/31"> <path d="M794.794 622.06L795.041 524.376L908 524.591L907.753 622.275L794.794 622.06Z" fill="white"/></a>
                        <text x="825" y="550" fill="red" stroke="#f00" >Plot 31</text>
                        
                        <line x1="810" y1="580" x2="890" y2="580" style="stroke:#ccc;stroke-width:20" />  
                        @php
                            $per31 = $percent[31];
                            $percent31 = ($percent[31]/100)*80 + 810 ; 
                        @endphp 
                        <line x1="810" y1="580" x2="<?php echo $percent31;?>" y2="580" style="stroke:rgb(60,141,188);stroke-width:20" />
                        <text x="835" y="585" fill="red" stroke="#000" opacity="1">@php  echo $per31.' %' ; @endphp </text>
                        <text x="810" y="610" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[31]) ; @endphp </text> 
                    </g>
                    <g>
                        <a href="/fields/30">  <path d="M908 623.684L908.247 526L1021.21 526.215L1020.96 623.899L908 623.684Z" fill="white"/></a>
                        <text x="935" y="550" fill="red" stroke="#f00" >Plot 30</text>
                        
                        <line x1="920" y1="580" x2="1000" y2="580" style="stroke:#ccc;stroke-width:20" />  
                        @php
                            $per30 = $percent[30];
                            $percent30 = ($percent[30]/100)*80 + 920 ; 
                        @endphp 
                        <line x1="920" y1="580" x2="<?php echo $percent30;?>" y2="580" style="stroke:rgb(60,140,188);stroke-width:20" />
                        <text x="945" y="585" fill="red" stroke="#000" opacity="1">@php  echo $per30.' %' ; @endphp </text>
                        <text x="920" y="610" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[30]) ; @endphp </text> 
                    </g>
                    <g>
                        <a href="/fields/29">  <path d="M1022 525C1021.45 525 1021 525.448 1021 526V624C1021 624.552 1021.45 625 1022 625H1103.58C1103.85 625 1104.11 624.89 1104.29 624.695L1160 567L1198.39 526.69C1199 526.053 1198.55 525 1197.67 525H1022Z" fill="white"/></a>
                        <text x="1055" y="550" fill="red" stroke="#f00" >Plot 29</text>
                        
                        <line x1="1040" y1="580" x2="1120" y2="580" style="stroke:#ccc;stroke-width:20" />  
                        @php
                            $per29 = $percent[29];
                            $percent29 = ($percent[29]/100)*80 + 1040 ; 
                        @endphp 
                        <line x1="1040" y1="580" x2="<?php echo $percent29;?>" y2="580" style="stroke:rgb(60,140,188);stroke-width:20" />
                        <text x="1065" y="585" fill="red" stroke="#000" opacity="1">@php  echo $per29.' %' ; @endphp </text>
                        <text x="1040" y="610" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[29]) ; @endphp </text> 
                    </g>

                    <g>
                            <a href="/fields/21">  <path d="M230 623V819H174L50 843V685L82 623H230Z" fill="white"/></a>
                            <text x="135" y="650" fill="red" stroke="#f00" >Plot 21</text>
                            
                            <line x1="120" y1="680" x2="200" y2="680" style="stroke:#ccc;stroke-width:20" />  
                            @php
                                $per21 = $percent[21];
                                $percent21 = ($percent[21]/100)*80 + 120 ; 
                            @endphp 
                            <line x1="120" y1="680" x2="<?php echo $percent21;?>" y2="680" style="stroke:rgb(60,141,188);stroke-width:20" />
                            <text x="145" y="685" fill="red" stroke="#000" opacity="1">@php  echo $per21.' %' ; @endphp </text>
                            <text x="120" y="710" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[21]) ; @endphp </text> 
                        </g>

                    <g>
                            <a href="/fields/28">  <path d="M230 720.752L230.214 623.068L343.172 623.316L342.958 721L230 720.752Z" fill="white"/></a>
                            <text x="265" y="650" fill="red" stroke="#f00" >Plot 28</text>
                            
                            <line x1="250" y1="680" x2="330" y2="680" style="stroke:#ccc;stroke-width:20" />  
                            @php
                                $per28 = $percent[28];
                                $percent28 = ($percent[28]/100)*80 + 250 ; 
                            @endphp 
                            <line x1="250" y1="680" x2="<?php echo $percent28;?>" y2="680" style="stroke:rgb(60,141,188);stroke-width:20" />
                            <text x="275" y="685" fill="red" stroke="#000" opacity="1">@php  echo $per28.' %' ; @endphp </text>
                            <text x="250" y="710" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[28]) ; @endphp </text> 
                        </g>
                        <g>
                            <a href="/fields/27">  <path d="M342.959 720.752L343.206 623.068L456.165 623.282L455.918 720.967L342.959 720.752Z" fill="white"/></a>
                            <text x="375" y="650" fill="red" stroke="#f00" >Plot 27</text>
                            
                            <line x1="360" y1="680" x2="440" y2="680" style="stroke:#ccc;stroke-width:20" />  
                            @php
                                $per27 = $percent[27];
                                $percent27 = ($percent[27]/100)*80 + 360 ; 
                            @endphp 
                            <line x1="360" y1="680" x2="<?php echo $percent27;?>" y2="680" style="stroke:rgb(60,141,188);stroke-width:20" />
                            <text x="385" y="685" fill="red" stroke="#000" opacity="1">@php  echo $per27.' %' ; @endphp </text>
                            <text x="360" y="710" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[27]) ; @endphp </text> 
                        </g>
                        <g>
                            <a href="/fields/26"><path d="M455.918 720.752L456.165 623.068L569.124 623.282L568.876 720.967L455.918 720.752Z" fill="white"/></a>
                            <text x="485" y="650" fill="red" stroke="#f00" >Plot 26</text>
                            
                            <line x1="470" y1="680" x2="550" y2="680" style="stroke:#ccc;stroke-width:20" />  
                            @php
                                $per26 = $percent[26];
                                $percent26 = ($percent[26]/100)*80 + 470 ; 
                            @endphp 
                            <line x1="470" y1="680" x2="<?php echo $percent26;?>" y2="680" style="stroke:rgb(60,141,188);stroke-width:20" />
                            <text x="495" y="685" fill="red" stroke="#000" opacity="1">@php  echo $per26.' %' ; @endphp </text>
                            <text x="470" y="710" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[26]) ; @endphp </text> 
                        </g>
                        <g>
                            <a href="/fields/25">  <path d="M568.876 720.752L569.124 623.068L682.082 623.282L681.835 720.967L568.876 720.752Z" fill="white"/></a>
                            <text x="595" y="650" fill="red" stroke="#f00" >Plot 25</text>
                            
                            <line x1="580" y1="680" x2="660" y2="680" style="stroke:#ccc;stroke-width:20" />  
                            @php
                                $per25 = $percent[25];
                                $percent25 = ($percent[25]/100)*80 + 580 ; 
                            @endphp 
                            <line x1="580" y1="680" x2="<?php echo $percent25;?>" y2="680" style="stroke:rgb(60,141,188);stroke-width:20" />
                            <text x="605" y="685" fill="red" stroke="#000" opacity="1">@php  echo $per25.' %' ; @endphp </text>
                            <text x="580" y="710" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[25]) ; @endphp </text> 
                        </g>
                        <g>
                            <a href="/fields/24"> <path d="M681.835 720.752L682.082 623.068L795.041 623.282L794.794 720.967L681.835 720.752Z" fill="white"/></a>
                            <text x="710" y="650" fill="red" stroke="#f00" >Plot 24</text>
                            
                            <line x1="695" y1="680" x2="775" y2="680" style="stroke:#ccc;stroke-width:20" />  
                            @php
                                $per24 = $percent[24];
                                $percent24 = ($percent[24]/100)*80 + 695 ; 
                            @endphp 
                            <line x1="695" y1="680" x2="<?php echo $percent24;?>" y2="680" style="stroke:rgb(60,141,188);stroke-width:20" />
                            <text x="720" y="685" fill="red" stroke="#000" opacity="1">@php  echo $per24.' %' ; @endphp </text>
                            <text x="695" y="710" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[24]) ; @endphp </text> 
                        </g>
                        <g>
                            <a href="/fields/23"> <path d="M794.794 720.752L795.041 623.068L908 623.282L907.753 720.967L794.794 720.752Z" fill="white"/></a>
                            <text x="825" y="650" fill="red" stroke="#f00" >Plot 23</text>
                            
                            <line x1="810" y1="680" x2="890" y2="680" style="stroke:#ccc;stroke-width:20" />  
                            @php
                                $per23 = $percent[23];
                                $percent23 = ($percent[23]/100)*80 + 810 ; 
                            @endphp 
                            <line x1="810" y1="680" x2="<?php echo $percent23;?>" y2="680" style="stroke:rgb(60,141,188);stroke-width:20" />
                            <text x="835" y="685" fill="red" stroke="#000" opacity="1">@php  echo $per23.' %' ; @endphp </text>
                            <text x="810" y="710" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[23]) ; @endphp </text> 
                        </g>
                        <g>
                            <a href="/fields/22">   <path d="M909 625C908.448 625 908 625.448 908 626V721C908 721.552 908.448 722 909 722H1015.57C1015.84 722 1016.11 721.887 1016.3 721.688L1062 673.5L1106.4 626.688C1107 626.051 1106.55 625 1105.67 625H909Z" fill="white"/></a>
                            <text x="935" y="650" fill="red" stroke="#f00" >Plot 22</text>
                            
                            <line x1="920" y1="680" x2="1000" y2="680" style="stroke:#ccc;stroke-width:20" />  
                            @php
                                $per22 = $percent[22];
                                $percent22 = ($percent[22]/100)*80 + 920 ; 
                            @endphp 
                            <line x1="920" y1="680" x2="<?php echo $percent22;?>" y2="680" style="stroke:rgb(60,140,188);stroke-width:20" />
                            <text x="945" y="685" fill="red" stroke="#000" opacity="1">@php  echo $per22.' %' ; @endphp </text>
                            <text x="920" y="710" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[22]) ; @endphp </text> 
                        </g>
                        <g>
                                <a href="/fields/20">  <path d="M230 819.443L230.214 721.759L343.172 722.007L342.958 819.692L230 819.443Z" fill="white"/></a>
                                <text x="265" y="750" fill="red" stroke="#f00" >Plot 20</text>
                                
                                <line x1="250" y1="780" x2="330" y2="780" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per20 = $percent[20];
                                    $percent20 = ($percent[20]/100)*80 + 250 ; 
                                @endphp 
                                <line x1="250" y1="780" x2="<?php echo $percent20;?>" y2="780" style="stroke:rgb(60,141,188);stroke-width:20" />
                                <text x="275" y="785" fill="red" stroke="#000" opacity="1">@php  echo $per20.' %' ; @endphp </text>
                                <text x="250" y="810" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[20]) ; @endphp </text> 
                            </g>
                            <g>
                                <a href="/fields/19"><path d="M342.959 819.443L343.206 721.759L456.165 721.974L455.918 819.658L342.959 819.443Z" fill="white"/></a>
                                <text x="375" y="750" fill="red" stroke="#f00" >Plot 19</text>
                                
                                <line x1="360" y1="780" x2="440" y2="780" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per19 = $percent[19];
                                    $percent19 = ($percent[19]/100)*80 + 360 ; 
                                @endphp 
                                <line x1="360" y1="780" x2="<?php echo $percent19;?>" y2="780" style="stroke:rgb(60,141,188);stroke-width:20" />
                                <text x="385" y="785" fill="red" stroke="#000" opacity="1">@php  echo $per19.' %' ; @endphp </text>
                                <text x="360" y="810" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[19]) ; @endphp </text> 
                            </g>
                            <g>
                                <a href="/fields/18"><path d="M455.918 819.443L456.165 721.759L569.124 721.974L568.876 819.658L455.918 819.443Z" fill="white"/></a>
                                <text x="485" y="750" fill="red" stroke="#f00" >Plot 18</text>
                                
                                <line x1="470" y1="780" x2="550" y2="780" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per18 = $percent[18];
                                    $percent18 = ($percent[18]/100)*80 + 470 ; 
                                @endphp 
                                <line x1="470" y1="780" x2="<?php echo $percent18;?>" y2="780" style="stroke:rgb(60,141,188);stroke-width:20" />
                                <text x="495" y="785" fill="red" stroke="#000" opacity="1">@php  echo $per18.' %' ; @endphp </text>
                                <text x="470" y="810" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[18]) ; @endphp </text> 
                            </g>
                            <g>
                                <a href="/fields/17">  <path d="M568.876 819.443L569.124 721.759L682.082 721.974L681.835 819.658L568.876 819.443Z" fill="white"/></a>
                                <text x="595" y="750" fill="red" stroke="#f00" >Plot 17</text>
                                
                                <line x1="580" y1="780" x2="660" y2="780" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per17 = $percent[17];
                                    $percent17 = ($percent[17]/100)*80 + 580 ; 
                                @endphp 
                                <line x1="580" y1="780" x2="<?php echo $percent17;?>" y2="780" style="stroke:rgb(60,141,188);stroke-width:20" />
                                <text x="605" y="785" fill="red" stroke="#000" opacity="1">@php  echo $per17.' %' ; @endphp </text>
                                <text x="580" y="810" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[17]) ; @endphp </text> 
                            </g>
                            <g>
                                <a href="/fields/16">  <path d="M681.835 819.443L682.082 721.759L795.041 721.974L794.794 819.658L681.835 819.443Z" fill="white"/></a>
                                <text x="710" y="750" fill="red" stroke="#f00" >Plot 16</text>
                                
                                <line x1="695" y1="780" x2="775" y2="780" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per16 = $percent[16];
                                    $percent16 = ($percent[16]/100)*80 + 695 ; 
                                @endphp 
                                <line x1="695" y1="780" x2="<?php echo $percent16;?>" y2="780" style="stroke:rgb(60,141,188);stroke-width:20" />
                                <text x="720" y="785" fill="red" stroke="#000" opacity="1">@php  echo $per16.' %' ; @endphp </text>
                                <text x="695" y="810" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[16]) ; @endphp </text> 
                            </g>
                            <g>
                                <a href="/fields/15">  <path d="M796 722C795.448 722 795 722.448 795 723V818C795 818.552 795.448 819 796 819H925.564C925.842 819 926.107 818.884 926.297 818.68L1014.44 723.68C1015.03 723.04 1014.58 722 1013.71 722H796Z" fill="white"/></a>
                                <text x="825" y="750" fill="red" stroke="#f00" >Plot 15</text>
                                
                                <line x1="810" y1="780" x2="890" y2="780" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per15 = $percent[15];
                                    $percent15 = ($percent[15]/100)*80 + 810 ; 
                                @endphp 
                                <line x1="810" y1="780" x2="<?php echo $percent15;?>" y2="780" style="stroke:rgb(60,141,188);stroke-width:20" />
                                <text x="835" y="785" fill="red" stroke="#000" opacity="1">@php  echo $per15.' %' ; @endphp </text>
                                <text x="810" y="810" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[15]) ; @endphp </text> 
                            </g>


                            <g>
                                <a href="/fields/14"> <path d="M230 918.135L230.214 820.451L343.172 820.699L342.958 918.383L230 918.135Z" fill="white"/></a>
                                <text x="265" y="850" fill="red" stroke="#f00" >Plot 14</text>
                                
                                <line x1="250" y1="880" x2="330" y2="880" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per14 = $percent[14];
                                    $percent14 = ($percent[14]/100)*80 + 250 ; 
                                @endphp 
                                <line x1="250" y1="880" x2="<?php echo $percent14;?>" y2="880" style="stroke:rgb(60,141,188);stroke-width:14" />
                                <text x="275" y="885" fill="red" stroke="#000" opacity="1">@php  echo $per14.' %' ; @endphp </text>
                                <text x="250" y="910" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[14]) ; @endphp </text> 
                            </g>
                            <g>
                                <a href="/fields/13"> <path d="M342.959 918.135L343.206 820.451L456.165 820.665L455.918 918.35L342.959 918.135Z" fill="white"/></a>
                                <text x="375" y="850" fill="red" stroke="#f00" >Plot 13</text>
                                
                                <line x1="360" y1="880" x2="440" y2="880" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per13 = $percent[13];
                                    $percent13 = ($percent[14]/100)*80 + 360 ; 
                                @endphp 
                                <line x1="360" y1="880" x2="<?php echo $percent13;?>" y2="880" style="stroke:rgb(60,141,188);stroke-width:20" />
                                <text x="385" y="885" fill="red" stroke="#000" opacity="1">@php  echo $per13.' %' ; @endphp </text>
                                <text x="360" y="910" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[13]) ; @endphp </text> 
                            </g>
                            <g>
                                <a href="/fields/12"><path d="M455.918 918.135L456.165 820.451L569.124 820.665L568.876 918.35L455.918 918.135Z" fill="white"/></a>
                                <text x="485" y="850" fill="red" stroke="#f00" >Plot 12</text>
                                
                                <line x1="470" y1="880" x2="550" y2="880" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per12 = $percent[12];
                                    $percent12 = ($percent[12]/100)*80 + 470 ; 
                                @endphp 
                                <line x1="470" y1="880" x2="<?php echo $percent12;?>" y2="880" style="stroke:rgb(60,141,188);stroke-width:20" />
                                <text x="495" y="885" fill="red" stroke="#000" opacity="1">@php  echo $per12.' %' ; @endphp </text>
                                <text x="470" y="910" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[12]) ; @endphp </text> 
                            </g>
                            <g>
                                <a href="/fields/11"> <path d="M569 821V919H706.159L718 877.84V821H569Z" fill="white"/></a>
                                <text x="595" y="850" fill="red" stroke="#f00" >Plot 11</text>
                                
                                <line x1="580" y1="880" x2="660" y2="880" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per11 = $percent[11];
                                    $percent11 = ($percent[11]/100)*80 + 580 ; 
                                @endphp 
                                <line x1="580" y1="880" x2="<?php echo $percent11;?>" y2="880" style="stroke:rgb(60,141,188);stroke-width:20" />
                                <text x="605" y="885" fill="red" stroke="#000" opacity="1">@php  echo $per11.' %' ; @endphp </text>
                                <text x="580" y="910" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[11]) ; @endphp </text> 
                            </g>

                            <g>
                                <a href="/fields/10"> <path d="M719 820C718.448 820 718 820.448 718 821V885C718 885.552 718.448 886 719 886H861.558C861.839 886 862.108 885.881 862.298 885.673L920.479 821.673C921.063 821.03 920.608 820 919.739 820H719Z" fill="white"/></a>
                                <text x="725" y="850" fill="red" stroke="#f00" >Plot 10</text>
                                
                                <line x1="780" y1="845" x2="860" y2="845" style="stroke:#ccc;stroke-width:20" />  
                                @php
                                    $per10 = $percent[10];
                                    $percent10 = ($percent[10]/100)*80 + 780 ; 
                                @endphp 
                                <line x1="780" y1="845" x2="<?php echo $percent10;?>" y2="845" style="stroke:rgb(60,141,188);stroke-width:20" />
                                <text x="805" y="850" fill="red" stroke="#000" opacity="1">@php  echo $per10.' %' ; @endphp </text>
                                <text x="730" y="875" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[10]) ; @endphp </text> 
                            </g>


                            <g>
                                    <a href="/fields/9"> <path d="M343 919V1017H253.203L226 919H343Z" fill="white"/></a>
                                    <text x="265" y="950" fill="red" stroke="#f00" >Plot 9</text>
                                    
                                    <line x1="250" y1="970" x2="330" y2="970" style="stroke:#ccc;stroke-width:20" />  
                                    @php
                                        $per9 = $percent[9];
                                        $percent9 = ($percent[9]/100)*80 + 250 ; 
                                    @endphp 
                                    <line x1="250" y1="970" x2="<?php echo $percent9;?>" y2="970" style="stroke:rgb(60,141,188);stroke-width:14" />
                                    <text x="275" y="975" fill="red" stroke="#000" opacity="1">@php  echo $per9.' %' ; @endphp </text>
                                    <text x="255" y="995" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[9]) ; @endphp </text> 
                                </g>
                                <g>
                                    <a href="/fields/8">  <path d="M342.959 1016.83L343.206 919.142L456.165 919.357L455.918 1017.04L342.959 1016.83Z" fill="white"/></a>
                                    <text x="375" y="950" fill="red" stroke="#f00" >Plot 8</text>
                                    
                                    <line x1="360" y1="970" x2="440" y2="970" style="stroke:#ccc;stroke-width:20" />  
                                    @php
                                        $per8 = $percent[8];
                                        $percent8 = ($percent[8]/100)*80 + 360 ; 
                                    @endphp 
                                    <line x1="360" y1="970" x2="<?php echo $percent8;?>" y2="970" style="stroke:rgb(60,141,188);stroke-width:20" />
                                    <text x="385" y="975" fill="red" stroke="#000" opacity="1">@php  echo $per8.' %' ; @endphp </text>
                                    <text x="360" y="995" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[8]) ; @endphp </text> 
                                </g>
                                <g>
                                    <a href="/fields/7"><path d="M455.918 1016.83L456.165 919.142L569.124 919.357L568.876 1017.04L455.918 1016.83Z" fill="white"/></a>
                                    <text x="485" y="950" fill="red" stroke="#f00" >Plot 7</text>
                                    
                                    <line x1="470" y1="970" x2="550" y2="970" style="stroke:#ccc;stroke-width:20" />  
                                    @php
                                        $per7 = $percent[7];
                                        $percent7 = ($percent[7]/100)*80 + 470 ; 
                                    @endphp 
                                    <line x1="470" y1="970" x2="<?php echo $percent7;?>" y2="970" style="stroke:rgb(60,141,188);stroke-width:20" />
                                    <text x="495" y="975" fill="red" stroke="#000" opacity="1">@php  echo $per7.' %' ; @endphp </text>
                                    <text x="470" y="995" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[7]) ; @endphp </text> 
                                </g>
                                <g>
                                    <a href="/fields/6">  <path d="M569 919V1018H677.503L706 919H569Z" fill="white"/></a>
                                    <text x="595" y="950" fill="red" stroke="#f00" >Plot 6</text>
                                    
                                    <line x1="580" y1="970" x2="660" y2="970" style="stroke:#ccc;stroke-width:20" />  
                                    @php
                                        $per6 = $percent[6];
                                        $percent6 = ($percent[6]/100)*80 + 580 ; 
                                    @endphp 
                                    <line x1="580" y1="970" x2="<?php echo $percent6;?>" y2="970" style="stroke:rgb(60,141,188);stroke-width:20" />
                                    <text x="605" y="975" fill="red" stroke="#000" opacity="1">@php  echo $per6.' %' ; @endphp </text>
                                    <text x="580" y="995" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[6]) ; @endphp </text> 
                                </g>

                                <g>
                                        <a href="/fields/5">  <path d="M456 1017V1150H297.538L250 1017H456Z" fill="white"/></a>
                                        <text x="375" y="1050" fill="red" stroke="#f00" >Plot 5</text>
                                        
                                        <line x1="360" y1="1070" x2="440" y2="1070" style="stroke:#ccc;stroke-width:20" />  
                                        @php
                                            $per5 = $percent[5];
                                            $percent5 = ($percent[5]/100)*80 + 360 ; 
                                        @endphp 
                                        <line x1="360" y1="1070" x2="<?php echo $percent5;?>" y2="1070" style="stroke:rgb(60,141,188);stroke-width:20" />
                                        <text x="385" y="1075" fill="red" stroke="#000" opacity="1">@php  echo $per5.' %' ; @endphp </text>
                                        <text x="360" y="1095" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[5]) ; @endphp </text> 
                                    </g>
                                    <g>
                                        <a href="/fields/4"><path d="M456 1017V1150H640L656 1101L678 1017H456Z" fill="white"/></a>
                                        <text x="485" y="1050" fill="red" stroke="#f00" >Plot 4</text>
                                        
                                        <line x1="470" y1="1070" x2="550" y2="1070" style="stroke:#ccc;stroke-width:20" />  
                                        @php
                                            $per4 = $percent[4];
                                            $percent4 = ($percent[4]/100)*80 + 470 ; 
                                        @endphp 
                                        <line x1="470" y1="1070" x2="<?php echo $percent4;?>" y2="1070" style="stroke:rgb(60,141,188);stroke-width:20" />
                                        <text x="495" y="1075" fill="red" stroke="#000" opacity="1">@php  echo $per4.' %' ; @endphp </text>
                                        <text x="470" y="1095" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[4]) ; @endphp </text> 
                                    </g>

                                
                                    <g>
                                        <a href="/fields/3"> <path d="M456 1151V1268H344.135L298 1151H456Z" fill="white"/></a>
                                        <text x="375" y="1170" fill="red" stroke="#f00" >Plot 3</text>
                                        
                                        <line x1="360" y1="1190" x2="440" y2="1190" style="stroke:#ccc;stroke-width:20" />  
                                        @php
                                            $per3 = $percent[3];
                                            $percent3 = ($percent[3]/100)*80 + 360 ; 
                                        @endphp 
                                        <line x1="360" y1="1190" x2="<?php echo $percent3;?>" y2="1190" style="stroke:rgb(60,141,188);stroke-width:20" />
                                        <text x="385" y="1195" fill="red" stroke="#000" opacity="1">@php  echo $per3.' %' ; @endphp </text>
                                        <text x="360" y="1215" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[3]) ; @endphp </text> 
                                    </g>
                                    <g>
                                        <a href="/fields/2"><path d="M456 1151V1268H580.609L642 1151H456Z" fill="white"/></a>
                                        <text x="485" y="1170" fill="red" stroke="#f00" >Plot 2</text>
                                        
                                        <line x1="470" y1="1190" x2="550" y2="1190" style="stroke:#ccc;stroke-width:20" />  
                                        @php
                                            $per2 = $percent[2];
                                            $percent2 = ($percent[2]/100)*80 + 470 ; 
                                        @endphp 
                                        <line x1="470" y1="1190" x2="<?php echo $percent2;?>" y2="1190" style="stroke:rgb(60,141,188);stroke-width:20" />
                                        <text x="495" y="1195" fill="red" stroke="#000" opacity="1">@php  echo $per2.' %' ; @endphp </text>
                                        <text x="470" y="1215" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[2]) ; @endphp </text> 
                                    </g>

                                    <g>
                                            <a href="/fields/1"> <path d="M343 1269H441.75H580L441.75 1521L343 1269Z" fill="white"/></a>
                                            <text x="430" y="1300" fill="red" stroke="#f00" >Plot 1</text>
                                            
                                            <line x1="410" y1="1320" x2="490" y2="1320" style="stroke:#ccc;stroke-width:20" />  
                                            @php
                                                $per1 = $percent[1];
                                                $percent1 = ($percent[1]/100)*80 + 410 ; 
                                            @endphp 
                                            <line x1="410" y1="1320" x2="<?php echo $percent1;?>" y2="1320" style="stroke:rgb(60,141,188);stroke-width:20" />
                                            <text x="435" y="1325" fill="red" stroke="#000" opacity="1">@php  echo $per1.' %' ; @endphp </text>
                                            <text x="410" y="1350" fill="red" stroke="#d6732b" opacity="1"  >@php echo number_format($moneySpend[1]) ; @endphp </text> 
                                        </g>
                                
                                   
                                
           
           
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M231.212 624.07L231.002 719.754L341.961 719.998L342.17 624.314L231.212 624.07ZM230 720.752L342.958 721L343.172 623.316L230.214 623.068L230 720.752Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M231.212 722.761L231.002 818.446L341.961 818.69L342.17 723.005L231.212 722.761ZM230 819.443L342.958 819.692L343.172 722.007L230.214 721.759L230 819.443Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M231.212 821.453L231.002 917.137L341.961 917.381L342.17 821.697L231.212 821.453ZM230 918.135L342.958 918.383L343.172 820.699L230.214 820.451L230 918.135Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M344.204 821.453L343.962 917.137L454.92 917.348L455.162 821.664L344.204 821.453ZM342.959 918.135L455.918 918.35L456.165 820.665L343.206 820.451L342.959 918.135Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M344.204 920.144L343.962 1015.83L454.92 1016.04L455.162 920.355L344.204 920.144ZM342.959 1016.83L455.918 1017.04L456.165 919.357L343.206 919.142L342.959 1016.83Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M457.163 920.144L456.92 1015.83L567.879 1016.04L568.121 920.355L457.163 920.144ZM455.918 1016.83L568.876 1017.04L569.124 919.357L456.165 919.142L455.918 1016.83Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M457.163 821.453L456.92 917.137L567.879 917.348L568.121 821.664L457.163 821.453ZM455.918 918.135L568.876 918.35L569.124 820.665L456.165 820.451L455.918 918.135Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M344.204 722.761L343.962 818.445L454.92 818.656L455.162 722.972L344.204 722.761ZM342.959 819.443L455.918 819.658L456.165 721.974L343.206 721.759L342.959 819.443Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M457.163 722.761L456.92 818.445L567.879 818.656L568.121 722.972L457.163 722.761ZM455.918 819.443L568.876 819.658L569.124 721.974L456.165 721.759L455.918 819.443Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M570.121 722.761L569.879 818.445L680.838 818.656L681.08 722.972L570.121 722.761ZM568.876 819.443L681.835 819.658L682.082 721.974L569.124 721.759L568.876 819.443Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M683.08 722.761L682.837 818.445L793.796 818.656L794.038 722.972L683.08 722.761ZM681.835 819.443L794.794 819.658L795.041 721.974L682.082 721.759L681.835 819.443Z" fill="#5C3333"/>
          
            <path fill-rule="evenodd" clip-rule="evenodd" d="M344.204 624.07L343.962 719.754L454.92 719.965L455.162 624.281L344.204 624.07ZM342.959 720.752L455.918 720.967L456.165 623.282L343.206 623.068L342.959 720.752Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M457.163 624.07L456.92 719.754L567.879 719.965L568.121 624.281L457.163 624.07ZM455.918 720.752L568.876 720.967L569.124 623.282L456.165 623.068L455.918 720.752Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M570.121 624.07L569.879 719.754L680.838 719.965L681.08 624.281L570.121 624.07ZM568.876 720.752L681.835 720.967L682.082 623.282L569.124 623.068L568.876 720.752Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M683.08 624.07L682.837 719.754L793.796 719.965L794.038 624.281L683.08 624.07ZM681.835 720.752L794.794 720.967L795.041 623.282L682.082 623.068L681.835 720.752Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M796.039 624.07L795.796 719.754L906.755 719.965L906.997 624.281L796.039 624.07ZM794.794 720.752L907.753 720.967L908 623.282L795.041 623.068L794.794 720.752Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M457.163 624.07L456.92 719.754L567.879 719.965L568.121 624.281L457.163 624.07ZM455.918 720.752L568.876 720.967L569.124 623.282L456.165 623.068L455.918 720.752Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M344.204 525.378L343.962 621.062L454.92 621.273L455.162 525.589L344.204 525.378ZM342.959 622.06L455.918 622.275L456.165 524.591L343.206 524.376L342.959 622.06Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M457.163 525.378L456.92 621.062L567.879 621.273L568.121 525.589L457.163 525.378ZM455.918 622.06L568.876 622.275L569.124 524.591L456.165 524.376L455.918 622.06Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M570.121 525.378L569.879 621.062L680.838 621.273L681.08 525.589L570.121 525.378ZM568.876 622.06L681.835 622.275L682.082 524.591L569.124 524.376L568.876 622.06Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M683.08 525.378L682.837 621.062L793.796 621.273L794.038 525.589L683.08 525.378ZM681.835 622.06L794.794 622.275L795.041 524.591L682.082 524.376L681.835 622.06Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M796.039 525.378L795.796 621.062L906.755 621.273L906.997 525.589L796.039 525.378ZM794.794 622.06L907.753 622.275L908 524.591L795.041 524.376L794.794 622.06Z" fill="#5C3333"/>
          
            <path fill-rule="evenodd" clip-rule="evenodd" d="M909.245 527.002L909.003 622.686L1019.96 622.897L1020.2 527.213L909.245 527.002ZM908 623.684L1020.96 623.899L1021.21 526.215L908.247 526L908 623.684Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M344.204 426.686L343.962 522.371L454.92 522.582L455.162 426.897L344.204 426.686ZM342.959 523.369L455.918 523.583L456.165 425.899L343.206 425.684L342.959 523.369Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M457.163 426.686L456.92 522.371L567.879 522.582L568.121 426.897L457.163 426.686ZM455.918 523.369L568.876 523.583L569.124 425.899L456.165 425.684L455.918 523.369Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M570.121 426.686L569.879 522.371L680.838 522.582L681.08 426.897L570.121 426.686ZM568.876 523.369L681.835 523.583L682.082 425.899L569.124 425.684L568.876 523.369Z" fill="#5C3333"/>
          
            <path fill-rule="evenodd" clip-rule="evenodd" d="M683.08 426.686L682.837 522.371L793.796 522.582L794.038 426.897L683.08 426.686ZM681.835 523.369L794.794 523.583L795.041 425.899L682.082 425.684L681.835 523.369Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M796.039 426.686L795.796 522.371L906.755 522.582L906.997 426.897L796.039 426.686ZM794.794 523.369L907.753 523.583L908 425.899L795.041 425.684L794.794 523.369Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M909.245 427.002L909.003 522.686L1019.96 522.897L1020.2 427.213L909.245 427.002ZM908 523.684L1020.96 523.899L1021.21 426.215L908.247 426L908 523.684Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1022.24 427.002L1022 522.686L1132.96 522.897L1133.2 427.213L1022.24 427.002ZM1021 523.684L1133.96 523.899L1134.21 426.215L1021.25 426L1021 523.684Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M580 1269H343L441.75 1521L580 1269ZM578.311 1270H344.466L441.901 1518.65L578.311 1270Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M455 1267V1152H299.469L344.815 1267H455ZM344.135 1268L298 1151H456V1268H344.135Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M457 1267V1152H640.346L580.005 1267H457ZM580.609 1268L642 1151H456V1268H580.609Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M173.904 818L51 841.788V685.243L82.6092 624H229V818H173.904ZM230 819V623H82L50 685V843L174 819H230Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M97.8809 506.91L55.65 426H229V620H77V571.158L97.8809 506.91ZM76 571V621H230V425H54L96.8 507L76 571Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M22.8871 368.538L1.69127 330H229V424H52.5696L38.6481 400.523L22.8871 368.538ZM37.7682 401L52 425H230V329H0L22 369L37.7682 401Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M570 918V822H717V877.699L705.406 918H570ZM706.159 919L718 877.84V821H569V919H706.159Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M457 1149V1018H676.704L655.04 1100.72L639.275 1149H457ZM640 1150L656 1101L678 1017H456V1150H640Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M229 328H7V289.381L93.8436 191.805L229 122.635V328ZM230 329H6V289L93.2212 191L230 121V329Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M570 1017V920H704.672L676.75 1017H570ZM677.503 1018L706 919H569V1018H677.503Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M719 885V821H919.739L861.558 885H719ZM718 821C718 820.448 718.448 820 719 820H919.739C920.608 820 921.063 821.03 920.479 821.673L862.298 885.673C862.108 885.881 861.839 886 861.558 886H719C718.448 886 718 885.552 718 885V821Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M796 818V723L1013.71 723L925.564 818H796ZM795 723C795 722.448 795.448 722 796 722H1013.71C1014.58 722 1015.03 723.04 1014.44 723.68L926.297 818.68C926.107 818.884 925.842 819 925.564 819H796C795.448 819 795 818.552 795 818V723Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1015.57 721H909V626L1105.67 626L1015.57 721ZM908 626C908 625.448 908.448 625 909 625H1105.67C1106.55 625 1107 626.051 1106.4 626.688L1016.3 721.688C1016.11 721.887 1015.84 722 1015.57 722H909C908.448 722 908 721.552 908 721V626Z" fill="#5C3333"/>
          
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1022 624V526H1197.67L1159.28 566.305L1159.28 566.31L1103.58 624H1022ZM1160 567L1104.29 624.695C1104.11 624.89 1103.85 625 1103.58 625H1022C1021.45 625 1021 624.552 1021 624V526C1021 525.448 1021.45 525 1022 525H1197.67C1198.55 525 1199 526.053 1198.39 526.69L1160 567Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1135 522V427H1289.82L1289.82 427L1261.26 460.253L1203.57 522H1135ZM1134 427C1134 426.448 1134.45 426 1135 426H1289.82C1290.68 426 1291.14 427.003 1290.58 427.652L1262 460.92L1204.3 522.683C1204.11 522.885 1203.84 523 1203.57 523H1135C1134.45 523 1134 522.552 1134 522V427Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1135 424V329L1366.32 329L1366.32 329L1337.19 382.406L1297.57 424H1135ZM1134 329C1134 328.448 1134.45 328 1135 328H1366.32C1367.07 328 1367.56 328.812 1367.19 329.479L1338.06 382.884C1338.02 382.961 1337.97 383.032 1337.91 383.095L1298.3 424.69C1298.11 424.888 1297.85 425 1297.57 425H1135C1134.45 425 1134 424.552 1134 424V329Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M909.245 427.002L909.003 522.686L1019.96 522.897L1020.2 427.213L909.245 427.002ZM908 523.684L1020.96 523.899L1021.21 426.215L908.247 426L908 523.684Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M455 1149V1018H251.419L298.243 1149H455ZM297.538 1150L250 1017H456V1150H297.538Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M342 1016V920H227.315L253.964 1016H342ZM253.203 1017L226 919H343V1017H253.203Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M344 327H455V15.7097L437.445 2.20154L344 56.4145V327ZM343 55.8385L437.524 1L456 15.2174V328H343V55.8385Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M457 327H568V57.5803L547.576 45.9093L457 12.4357V327ZM456 11L548 45L569 57V328H456V11Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M570 327H681V151.429L595.168 69.8651L570 60.4422V327ZM569 59L595.709 69L682 151V328H569V59Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M683 327H794V82.6564L738.526 111.853L683 151.515V327ZM682 151L738 111L795 81V328H682V151Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M796 328H907V56.1146L816.401 65.9624L796 82.4771V328ZM795 82L816 65L908 55V329H795V82Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M909 328H1020V44.1889L952.197 55.9808L909 65.7982V328ZM908 65L952 55L1021 43V329H908V65Z" fill="#5C3333"/>
            
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1082.1 43H1022V327H1133V32.2337L1082.1 43ZM1082 42H1021V328H1134V31L1082 42Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1135 328H1246V43.8488L1162.16 30.0394L1135 43.618V328ZM1134 43L1162 29L1247 43V329H1134V43Z" fill="#5C3333"/>
           
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1486 269.784L1592 235.659L1348 67.7867L1272.03 55H1247V329H1364L1394 299.894L1486 269.784ZM1363.59 328L1393.47 299.015L1485.69 268.833L1485.69 268.832L1589.76 235.331L1347.62 68.736L1271.94 56H1248V328H1363.59Z" fill="#5C3333"/>
          
            <path fill-rule="evenodd" clip-rule="evenodd" d="M303.803 83.8629L231 126.493V328H342V60.773L303.803 83.8629ZM343 59V329H230V125.919L303.297 83L343 59Z" fill="#5C3333"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M581.999 1268.08L343 1268L343.002 1263L582.001 1263.08L581.999 1268.08Z" fill="#19E919"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M678 1018H250V1013H678V1018Z" fill="#19E919"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M922 821L230 819L230.01 815.364L922.01 817.364L922 821Z" fill="#19E919"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1108 626H78V625H1108V626Z" fill="#19E919"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1306 423.87L50.0035 426.244L50 424.374L1306 422L1306 423.87Z" fill="#19E919"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M231 926L231 329H235L235 926H231Z" fill="#19E919"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M338.959 1260.69L249.955 1012.26L254.662 1010.58L343.666 1259.01L338.959 1260.69Z" fill="#19E919"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M679.551 819.003L680.551 329.003L684 329.01L683 819.01L679.551 819.003Z" fill="#D6732B"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M794.653 817.013L796.153 82.0115L792.673 82.0044L791.173 817.006L794.653 817.013Z" fill="#D6732B"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M906.516 722.003L907.879 53.9978L911.594 54.0054L910.23 722.01L906.516 722.003Z" fill="#D6732B"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1018.44 625.008L1019.62 44.9932L1023.18 45.0005L1022 625.015L1018.44 625.008Z" fill="#D6732B"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1133 523.015L1133.97 46.988L1137.44 46.9951L1136.47 523.022L1133 523.015Z" fill="#D6732B"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M569.157 1010.95L571.09 64.0085L567.386 64.001L565.453 1010.95L569.157 1010.95Z" fill="#D6732B"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M452.288 1012.98L454.312 21.0029L457.933 21.0103L455.908 1012.99L452.288 1012.98Z" fill="#D6732B"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M340.216 1016.99L342.171 59.0024L346.024 59.0103L344.069 1017L340.216 1016.99Z" fill="#D6732B"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M715.061 886.006L715 821.006L717.939 821.003L718 886.003L715.061 886.006Z" fill="#EF6D0F"/>
            <path d="M231.476 926.747L169 818.701L230.646 819.222L231.476 926.747Z" fill="white"/>
            <path d="M229.309 270.702C229.696 271.096 230.33 271.1 230.723 270.712L237.13 264.392C237.523 264.004 237.528 263.371 237.14 262.978C236.752 262.584 236.119 262.58 235.726 262.968L230.03 268.586L224.412 262.89C224.024 262.497 223.391 262.493 222.998 262.881C222.605 263.269 222.601 263.902 222.988 264.295L229.309 270.702ZM230 126.993L229.021 269.993L231.021 270.007L232 127.007L230 126.993Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M193.695 331.129C194.089 330.742 194.094 330.109 193.706 329.715L187.39 323.303C187.003 322.91 186.37 322.905 185.976 323.292C185.583 323.68 185.578 324.313 185.966 324.707L191.579 330.406L185.88 336.02C185.486 336.408 185.482 337.041 185.869 337.434C186.257 337.828 186.89 337.832 187.283 337.445L193.695 331.129ZM5.99242 330L192.986 331.417L193.001 329.417L6.00758 328L5.99242 330Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M422.71 329.704C423.099 329.312 423.096 328.679 422.704 328.29L416.312 321.954C415.919 321.565 415.286 321.568 414.897 321.961C414.509 322.353 414.511 322.986 414.904 323.375L420.586 329.006L414.954 334.688C414.565 335.081 414.568 335.714 414.961 336.103C415.353 336.491 415.986 336.489 416.375 336.096L422.71 329.704ZM198.004 331L422.004 330L421.996 328L197.996 329L198.004 331Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M653.71 328.757C654.099 328.365 654.097 327.732 653.704 327.343L647.314 321.005C646.922 320.617 646.289 320.619 645.9 321.011C645.511 321.403 645.514 322.037 645.906 322.425L651.586 328.059L645.952 333.739C645.563 334.131 645.566 334.764 645.958 335.153C646.35 335.542 646.983 335.54 647.372 335.147L653.71 328.757ZM422.004 330L653.004 329.053L652.996 327.053L421.996 328L422.004 330Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M229.434 327.764C229.826 328.153 230.459 328.152 230.848 327.76L237.197 321.381C237.587 320.989 237.585 320.356 237.194 319.967C236.802 319.577 236.169 319.579 235.78 319.97L230.136 325.64L224.466 319.997C224.074 319.608 223.441 319.609 223.052 320.001C222.662 320.392 222.664 321.025 223.055 321.415L229.434 327.764ZM229 269.002L229.14 327.057L231.14 327.052L231 268.998L229 269.002Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M1232.7 329.711C1233.09 329.323 1233.1 328.69 1232.71 328.297L1226.38 321.895C1226 321.502 1225.36 321.498 1224.97 321.887C1224.58 322.275 1224.57 322.908 1224.96 323.301L1230.58 328.992L1224.89 334.614C1224.5 335.002 1224.5 335.636 1224.89 336.028C1225.27 336.421 1225.91 336.425 1226.3 336.037L1232.7 329.711ZM1065.99 329L1231.99 330L1232.01 328L1066.01 327L1065.99 329Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M1246.66 205.215C1247.05 205.602 1247.69 205.596 1248.07 205.202L1254.38 198.781C1254.76 198.387 1254.76 197.753 1254.37 197.366C1253.97 196.979 1253.34 196.985 1252.95 197.379L1247.35 203.087L1241.64 197.482C1241.24 197.095 1240.61 197.101 1240.22 197.495C1239.84 197.889 1239.84 198.522 1240.24 198.909L1246.66 205.215ZM1245 55.0091L1246.36 204.511L1248.36 204.492L1247 54.9909L1245 55.0091Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M682.657 330.214C683.05 330.601 683.684 330.597 684.071 330.203L690.386 323.791C690.774 323.397 690.769 322.764 690.375 322.376C689.982 321.989 689.349 321.994 688.961 322.387L683.348 328.087L677.648 322.474C677.254 322.086 676.621 322.091 676.234 322.485C675.846 322.878 675.851 323.511 676.244 323.899L682.657 330.214ZM681 152.008L682.359 329.509L684.359 329.494L683 151.992L681 152.008Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M1246.29 309.707C1246.68 310.098 1247.32 310.098 1247.71 309.707L1254.07 303.343C1254.46 302.953 1254.46 302.319 1254.07 301.929C1253.68 301.538 1253.05 301.538 1252.66 301.929L1247 307.586L1241.34 301.929C1240.95 301.538 1240.32 301.538 1239.93 301.929C1239.54 302.319 1239.54 302.953 1239.93 303.343L1246.29 309.707ZM1246 204V309H1248V204H1246Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M1232.01 331.18L1247.01 331L1246.99 329L1231.99 329.18L1232.01 331.18Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M858.7 329.125C859.091 328.736 859.092 328.103 858.702 327.711L852.351 321.334C851.962 320.943 851.329 320.942 850.937 321.331C850.546 321.721 850.545 322.354 850.934 322.746L856.58 328.414L850.911 334.059C850.52 334.449 850.519 335.082 850.908 335.474C851.298 335.865 851.931 335.866 852.323 335.476L858.7 329.125ZM652.998 329L857.992 329.417L857.996 327.417L653.002 327L652.998 329Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M1066.7 329.71C1067.1 329.322 1067.1 328.689 1066.71 328.296L1060.38 321.903C1059.99 321.51 1059.35 321.507 1058.96 321.896C1058.57 322.285 1058.57 322.918 1058.96 323.31L1064.59 328.993L1058.9 334.624C1058.51 335.012 1058.51 335.646 1058.9 336.038C1059.28 336.43 1059.92 336.433 1060.31 336.045L1066.7 329.71ZM851.995 329L1066 330L1066 328L852.005 327L851.995 329Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M466.71 524.704C467.099 524.312 467.097 523.679 466.704 523.29L460.314 516.952C459.922 516.563 459.289 516.566 458.9 516.958C458.511 517.35 458.514 517.983 458.906 518.372L464.586 524.006L458.952 529.686C458.563 530.078 458.566 530.711 458.958 531.1C459.35 531.489 459.983 531.486 460.372 531.094L466.71 524.704ZM235.004 525.947L466.004 525L465.996 523L234.996 523.947L235.004 525.947Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M697.71 524.704C698.099 524.312 698.097 523.679 697.704 523.29L691.314 516.952C690.922 516.563 690.289 516.566 689.9 516.958C689.511 517.35 689.514 517.983 689.906 518.372L695.586 524.006L689.952 529.686C689.563 530.078 689.566 530.711 689.958 531.1C690.35 531.489 690.983 531.486 691.372 531.094L697.71 524.704ZM466.004 525.947L697.004 525L696.996 523L465.996 523.947L466.004 525.947Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M934.71 524.704C935.099 524.312 935.097 523.679 934.704 523.29L928.314 516.953C927.921 516.564 927.288 516.567 926.899 516.959C926.511 517.351 926.513 517.984 926.905 518.373L932.586 524.006L926.953 529.687C926.564 530.079 926.567 530.712 926.959 531.101C927.351 531.49 927.984 531.487 928.373 531.095L934.71 524.704ZM697.004 526L934.004 525L933.996 523L696.996 524L697.004 526Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M464.71 721.704C465.099 721.312 465.097 720.679 464.704 720.29L458.314 713.952C457.922 713.563 457.289 713.566 456.9 713.958C456.511 714.35 456.514 714.983 456.906 715.372L462.586 721.006L456.952 726.686C456.563 727.078 456.566 727.711 456.958 728.1C457.35 728.489 457.983 728.486 458.372 728.094L464.71 721.704ZM233.004 722.947L464.004 722L463.996 720L232.996 720.947L233.004 722.947Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M697.71 720.704C698.099 720.312 698.097 719.679 697.704 719.29L691.314 712.952C690.922 712.563 690.289 712.566 689.9 712.958C689.511 713.35 689.514 713.983 689.906 714.372L695.586 720.006L689.952 725.686C689.563 726.078 689.566 726.711 689.958 727.1C690.35 727.489 690.983 727.486 691.372 727.094L697.71 720.704ZM466.004 721.947L697.004 721L696.996 719L465.996 719.947L466.004 721.947Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M931.71 721.704C932.099 721.312 932.097 720.679 931.704 720.29L925.314 713.952C924.922 713.563 924.289 713.566 923.9 713.958C923.511 714.35 923.514 714.983 923.906 715.372L929.586 721.006L923.952 726.686C923.563 727.078 923.566 727.711 923.958 728.1C924.35 728.489 924.983 728.486 925.372 728.094L931.71 721.704ZM700.004 722.947L931.004 722L930.996 720L699.996 720.947L700.004 722.947Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M446.707 919.707C447.098 919.317 447.098 918.683 446.707 918.293L440.343 911.929C439.953 911.538 439.319 911.538 438.929 911.929C438.538 912.319 438.538 912.953 438.929 913.343L444.586 919L438.929 924.657C438.538 925.047 438.538 925.681 438.929 926.071C439.319 926.462 439.953 926.462 440.343 926.071L446.707 919.707ZM233 920H446V918H233V920Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M659.707 919.707C660.098 919.317 660.098 918.683 659.707 918.293L653.343 911.929C652.953 911.538 652.319 911.538 651.929 911.929C651.538 912.319 651.538 912.953 651.929 913.343L657.586 919L651.929 924.657C651.538 925.047 651.538 925.681 651.929 926.071C652.319 926.462 652.953 926.462 653.343 926.071L659.707 919.707ZM446 920H659V918H446V920Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M658.979 919L705.979 920L706.021 918L659.021 917L658.979 919Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M513.707 1151.71C514.098 1151.32 514.098 1150.68 513.707 1150.29L507.343 1143.93C506.953 1143.54 506.319 1143.54 505.929 1143.93C505.538 1144.32 505.538 1144.95 505.929 1145.34L511.586 1151L505.929 1156.66C505.538 1157.05 505.538 1157.68 505.929 1158.07C506.319 1158.46 506.953 1158.46 507.343 1158.07L513.707 1151.71ZM300 1152H513V1150H300V1152Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M512.992 1151L641.992 1152L642.008 1150L513.008 1149L512.992 1151Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M456.703 1055.29C456.311 1054.9 455.677 1054.9 455.289 1055.3L448.96 1061.69C448.571 1062.09 448.575 1062.72 448.967 1063.11C449.36 1063.5 449.993 1063.49 450.381 1063.1L456.008 1057.41L461.695 1063.04C462.088 1063.43 462.721 1063.43 463.109 1063.03C463.498 1062.64 463.494 1062.01 463.102 1061.62L456.703 1055.29ZM458.153 1268.99L457 1055.99L455 1056.01L456.153 1269L458.153 1268.99Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M456 1056.02L457 1015.02L455 1014.98L454 1055.98L456 1056.02Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M229.5 124.097V125V129.395L233.278 127.149L344.278 61.1488L344.325 61.1208L344.371 61.0908L433.148 2.89259L458.849 16.2194L458.971 16.2824L459.098 16.3318L534.041 45.3096L569.985 61.2845L596.568 73.0992L679.243 154.778L680.78 156.297L682.506 154.996L739.308 112.145L795.086 85.2519L795.322 85.1383L795.529 84.9782L816.991 68.3938L907.5 56.8394V66V69.2731L910.658 68.4119L954.527 56.4475L1023.2 45.5H1084H1084.34L1084.66 45.4108L1132.5 32.2787V43V46.983L1136.09 45.2514L1164.37 31.5953L1246.5 45.1219V54V56.5H1249H1272.79L1343.04 68.3727L1583.49 234.234L1391.2 299.633L1390.62 299.828L1390.2 300.265L1363.2 328.265L1362.95 328.526L1362.78 328.848L1335.96 380.505L1294.23 422.232L1294.22 422.246L1294.21 422.26L1198.21 521.26L1198.17 521.3L1198.13 521.341L1104.19 627.271L1104.13 627.341L1014.2 721.266L1014.19 721.271L856.937 884.5H715H713.11L712.595 886.318L676.595 1013.32L676.592 1013.33L638.654 1149.1L578.788 1270.83L442.426 1513.14L349.349 1271.14L349.333 1271.1L255.391 1013.26L232.422 923.381L232.33 923.022L232.138 922.705L169.138 818.705L168.241 817.223L166.538 817.543L52.5 838.986V686.616L84.2139 626.161L84.8802 624.891L84.12 623.675L79.5 616.283V572.4L100.375 508.779L100.71 507.758L100.2 506.812L5.57429 331.506L10.3943 290.054L93.5733 194.002L229.5 124.097Z" stroke="#364F35" stroke-width="5"/>
            <path d="M1149.71 525.707C1150.1 525.317 1150.1 524.683 1149.71 524.293L1143.34 517.929C1142.95 517.538 1142.32 517.538 1141.93 517.929C1141.54 518.319 1141.54 518.953 1141.93 519.343L1147.59 525L1141.93 530.657C1141.54 531.047 1141.54 531.681 1141.93 532.071C1142.32 532.462 1142.95 532.462 1143.34 532.071L1149.71 525.707ZM936 526H1149V524H936V526Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M930.975 722L1010.98 724L1011.02 722L931.025 720L930.975 722Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path d="M1148.96 523.999L1193.96 525.999L1194.04 524.001L1149.04 522.001L1148.96 523.999Z" fill="#22E3E3" fill-opacity="0.95"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1250.07 326L1358.05 327.959L1357.98 331.958L1250 329.999L1250.07 326Z" fill="#19E919"/>
            </svg>

                
    </div>
@endsection
<script src="{{ asset('js/jquery.min.js') }}"></script>
