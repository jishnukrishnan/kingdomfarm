@extends('layouts.navbar')
<style>
    .mkred {
        color:#c9302c;
        border-color:#ac2925;
    }
</style>
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
   <div class="">   
    {{-- <form action="/field_data_insert" method="POST">
        {{csrf_field()}} --}}
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Templates <i class="fa fa-angle-double-right" aria-hidden="true"></i> View
            
        </h2>
        <div class="col-md-12 table-responsive" >
        <table class="table table-bordered text-center table-data-tr">
            <tr>
                <th style="width:10%;">Sl.no</th>
                <th style="width:80%;">Template Name</th>
                <th style="width:10%;">Action</th>        
            </tr>
            @if(count($templates) > 0)
                <?php  $i = 1 ;
                ?>              
                @foreach($templates as $template)
   
                    <tr>
                        <td><?php echo $i ; ?></td>
                        <td class="left" style="text-transform:capitalize;"><?php echo $template->template_name ; ?></td>
                        <td>
                            <a href="/viewSinglePlot/{{$template->template_name}}" ><i class="fa fa-pencil fa-2x mkyellow" aria-hidden="true"></i></a>
                            <a  onClick="delete_template('{{$template->template_name}}')" data-toggle="modal" data-target="#deleteTempModal" ><i class="fa fa-times fa-2x mkred" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <?php  $i++ ;
                    ?>     
                @endforeach
            @endif

         

    </table>
    </div>
    {{-- </form> --}}
   </div>

   <div class="modal" tabindex="-1" role="dialog" id="deleteTempModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Template ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="del_text"></p>
            </div>
            <div class="modal-footer">
                <form action="/delete_template" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" id="del_id" name="del_id">
                    <input type="submit" class="btn btn-primary" value="Delete" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
          </div>
        </div>
      </div>

@endsection

<script>
     function delete_template(template) {
        document.getElementById('del_text').innerHTML = 'Are you sure to delete Template " '+template+' " ?';
        document.getElementById('del_text').style.color = 'red';
        document.getElementById('del_id').value = template ;
    }
</script>
