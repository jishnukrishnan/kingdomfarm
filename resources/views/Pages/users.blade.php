@extends('layouts.navbar')
<style>
        .mkyellow {
            color:#f0ad4e !important;
            border-color:#d58512 !important;
        }
    </style>
@section('content')
<div class="">
        
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Manage <i class="fa fa-angle-double-right" aria-hidden="true"></i> Users </h2>
    
           <div class="row col-md-12 col-sm-12 table-responsive">
               <table class="table table-bordered text-center">
                    <tr>
                        <th>Sl.No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Check/ Update</th>
                    </tr>

                    @if(count($users) > 0)
                        @php
                            $i = 1;
                            // dd($users);
                        @endphp
                        @foreach($users as $user)
                            <tr>
                            <input type="hidden" name="user_id[]" value="{{$user->id}}">
                                <td>{{$i}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    
                                    <a href="#" onClick="change_privilege('{{$user->name}}',{{$user->id}})" title="Edit" data-toggle="modal" data-target="#privilegeModal"><i class="fa fa-pencil fa-2x mkyellow" aria-hidden="true"></i></a>

                                </td>
                            </tr>
                            @php
                                $i++ ;
                            @endphp
                        @endforeach
                    @endif
                    {{$users->links()}}
               </table>
           </div>
        </form>
    </div>
   </div>

   <div class="modal" tabindex="-1" role="dialog" id="privilegeModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Change Privilege ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" style="display:flex">
              <p id="del_text" style="flex:3"></p>
              <form action="/updatePrivilege" method="POST">
                {{csrf_field()}}
              <select name="privilege_id" id="privilege_id" class="form-control" style="flex:1">
                  <option value="">Select</option>
                  <option value="1">Admin</option>
                  <option value="2">Manager</option>  
                  <option value="3">User</option>
              </select>
            </div>
            <div class="modal-footer">                 
                    <input type="hidden" id="user_id" name="user_id">
                    <input type="submit" class="btn btn-primary" value="Update" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    
    
  
@endsection
<script>
function change_privilege(name,id){
        $('#privilege_id').val('');
        document.getElementById('del_text').innerHTML = 'Are you sure to change Privilege of " '+name+' " ?';
        document.getElementById('del_text').style.color = '#262626';
        document.getElementById('user_id').value = id ;
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/selectedPrivilege') }}",
                  method: 'post',
                  data: {
                     id: id,
                  },
                  success: function(result){
                      if(result.privilege){
                        $('#privilege_id').val(result.privilege.privilege_id);
                      }
                    
                  }
            }); 
}
</script>