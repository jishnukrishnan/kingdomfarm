@extends('layouts.navbar')

<style>
#acre-label {
    padding: 8px;
    font-size: 18px;
    text-transform: uppercase;
}
.mk-disabled {
    pointer-events:none;
    background:#ccc;   
}
.mainArea .modal th ,.mainArea .modal td {
    color: #000;
}

#harvestQty {
    color:#000;
    float:right;
}

</style>
@section('content')

   <div class="">
   <h2 class="page-head">View Old Plot Data - <span style="color:#65ad23;"> <?php if($field_datas[0]->item_field==67){ echo '55 A' ; } else { echo $field_datas[0]->item_field ;} ?> - <?php echo $id ; ?></span></h2>
   <a href="/oldPlotDatas" ><button class="btn btn-default pull-left" style="margin-bottom:10px;margin-right:10px" >Go Back</button></a>

    <button class="btn btn-primary pull-left" style="margin-bottom:10px;margin-right:10px" data-toggle="modal" data-target="#costAnalysisModal" >Cost Analysis </button>
    
<form action="/field_data_insert" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="login_user_id" value="{{Auth::user()->id}}">
    <input type="hidden" id="fieldId" name="fieldId" value="{{$id}}">
    <input type="hidden" name="f_unique_id" value="<?php echo @$field_datas[0]->field_unique_id; ?>" />

    <input type="text" name="acres" class="form-control headNameInp pull-right" id="acres" value="{{@$field_datas[0]->field_acres}}" style="margin-bottom:10px;margin-right:10px" placeholder="No of Acres" required>
    <label class="pull-right" id="acre-label">Acres</label>

    <div class="col-md-12 col-sm-12 table-responsive">
    <table class="table table-bordered text-center table-data-tr">
        <thead>
            <tr>
                <th style="width:5%;">Sl.no</th>
                <th style="width:16%;">Description</th>
                <th style="width:10%;">Start Day</th>
                <th style="width:10%;">End Day</th>
                <th style="width:8%;">Status</th>
                <th style="width:4%;">Qty</th>
                <th style="width:16%;">Stock Ledger</th>
                <th style="width:12%;">Stock Name</th> 
                <th style="width:2%;">Add</th>
                <th style="width:2%;">Print</th>
            </tr>
        </thead>
        <tbody id="plotDetails" >
                @if(count($field_datas) > 0)
                @php
                    $i = 1 ;
                    $needed = array(5,6,10,11,14,15,16);
                @endphp
                @foreach($field_datas as $field_data)   
                <input type="hidden" name="temp_name" value="{{$field_data->item_template}}" />   
                <tr class="table-data-tr">   
                    <input type="hidden" name="field_data_id[]" value="{{$field_data->id}}" />
                        <td style="width:2%;">
                            @php
                                echo $i ;
                            @endphp
                           
                        </td>
                        <td style="width:20%;" class="left">
                            @php
                                echo $field_data->description ;
                            @endphp
                             <input type="hidden" id="item_desc_{{$i}}" value="{{$field_data->description}}" />
                            <input type="hidden" id="from_day_{{$i}}" value={{$field_data->start_day}}>
                            <input type="hidden" id="to_day_{{$i}}" value={{$field_data->end_day}}>
                            @if($i == 19)
                        <input type="text" id="harvestQty" name="harvestQty"  placeholder="Harvest Qty" value="{{ @$harvest}}">
                            @endif
                        </td>
                        
                        <td style="width:8%;" >
                        <input type="text" id="start_cal_{{$i}}" onchange="changeDate({{$i}})" value="<?php echo date('d-m-Y',strtotime($field_data->start_date)); ?>" name="start_day[]" data-date-start-date="{{date('d-m-Y')}}" placeholder="dd-mm-yyyy" class="form-control mk-pd-0 text-center">
                            
                        </td>
                        <input type="hidden" id="prev_start_cal_{{$i}}" value="<?php echo date('d-m-Y',strtotime($field_data->start_date)); ?>">
                        <td style="width:8%;" >
                            <input type="text" id="end_cal_{{$i}}"  name="end_day[]" value="<?php echo date('d-m-Y',strtotime($field_data->end_date)); ?>" placeholder="dd-mm-yyyy" data-date-start-date="{{date('d-m-Y')}}" class="form-control mk-pd-0 text-center" disabled>
                            <input type="hidden" id="end_calv_{{$i}}"  name="end_dayv[]" <?php echo date('d-m-Y',strtotime($field_data->end_date)); ?>>
                        </td>
                        <td style="width:8%;">
                            <select name="item_status[]" id="item_status_{{$i}}" onchange="activateNextStatus(<?php echo $i ; ?>)" class="form-control" >
                                <option value="">Select</option>
                                @if(count($status) > 0)
                                    @foreach($status as $stat)
                                        <option value="{{$stat->id}}" <?php if($stat->id == $field_data->item_status) { echo 'selected' ; } ?>>{{$stat->status}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </td>
                        <td style="width:4%;">
                            <input type="text" placeholder="Qty" name="item_qty[]"  id="item_qty_{{$i}}" value="<?php if($field_data->item_qty > 0){ echo $field_data->item_qty; } ?>" <?php if(!in_array($i,$needed)){?> style = "pointer-events:none;background:#ccc;"  <?php }else{ echo "required" ;} ?> class="form-control" style="width:80px;" >
                        </td>
                        <td style="width:16%;">
                            <select name="item_category[]" id="item_category_{{$i}}" onchange="getItem({{$i}})" <?php if(!in_array($i,$needed)){?> style = "pointer-events:none;background:#ccc;"   <?php }else{ echo "required" ;} ?> class="form-control">
                                <option value="">Select</option>                                      
                                @if(count($categories) > 0)
                                    @foreach($categories as $category)
                            <option value="{{$category->id}}" <?php if($category->id == $field_data->item_category) {?> selected <?php } ?> >{{$category->category_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </td>
                        <td style="width:12%;">
                            <select name="item_stock[]" id="item_stock_{{$i}}" <?php if(!in_array($i,$needed)){?> style = "pointer-events:none;background:#ccc;"   <?php }else{ echo "required" ;} ?> class="form-control" >
                                <option value="">Select</option>
                                @if(count($stock_items) > 0)
                                @foreach($stock_items as $stock_item)
                                    <option value="{{$stock_item->id}}" <?php if($stock_item->id == $field_data->item_stock) {?> selected <?php } ?>>{{$stock_item->item_name}}</option>
                                @endforeach
                            @endif
                            </select>
                        </td>  
                    <td style="width:2%;"> <?php if(in_array($i,$needed)){?> <a href="#" onclick="add_row({{$i}})" id="this_rw_add_{{$i}}"><i class="fa fa-plus"></i></a> <?php } ?></td>
                    <td style="width:2%;"> <a href="#" onclick="print_this('{{$id}}','{{$field_data->description}}','{{date('d-m-Y',strtotime($field_data->start_date))}}','{{date('d-m-Y',strtotime($field_data->end_date))}}',{{$i}},0)"><i class="fa fa-print"></i></a> </td>
                    </tr>
                    <tbody id="new_Row_{{$i}}">
                    @if(count($extra_field_d) > 0)
                    @php
                        $x = 1 ;
                    @endphp
                    @foreach($extra_field_d as $ef)
           
                        @if($ef->items_id == $i)
                            <tr id="new_Row_{{$i}}_{{$x}}" >
                            <input type="hidden" name="main_plot_id[]" value="{{$i}}"/>
                                <td colspan="5"></td>
                            
                            <td style="width:4%;">
                                <input type="text" placeholder="Qty" name="item_qty_sub[]"  id="item_qty_{{$i}}_{{$x}}" value="<?php if($ef->ef_item_qty > 0){ echo $ef->ef_item_qty; } ?>"  class="form-control" style="width:80px;" >
                            </td>
                            <td style="width:16%;">
                                <select name="item_category_sub[]" id="item_category_{{$i}}_{{$x}}" onchange="getItemSub({{$i}},{{$x}})"  class="form-control">
                                    <option value="">Select</option>              
                                    @if(count($categories) > 0)
                                        @foreach($categories as $category)
                                <option value="{{$category->id}}" <?php if($category->id == $ef->ef_item_category) {?> selected <?php } ?> >{{$category->category_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </td>
                            <td style="width:12%;">
                                <select name="item_stock_sub[]" id="item_stock_{{$i}}_{{$x}}" class="form-control" >
                                    <option value="">Select</option>
                                    @if(count($stock_items) > 0)
                                    @foreach($stock_items as $stock_item)
                                        <option value="{{$stock_item->id}}" <?php if($stock_item->id == $ef->ef_item_stock) {?> selected <?php } ?>>{{$stock_item->item_name}}</option>
                                    @endforeach
                                @endif
                                </select>
                            </td>  
                        <td><a href="#" onclick="rem_row({{$i}},{{$x}})" id="rem_row_id_{{$i}}"><i class="fa fa-times"></i></a></td>
                            <td><a href="#" onclick="print_this('{{$id}}',document.getElementById('item_desc_{{$i}}').value,document.getElementById('start_cal_{{$i}}').value,document.getElementById('end_calv_{{$i}}').value,'{{$i}}','{{$x}}')"><i class="fa fa-print"></i></a> </td>
                        </tr>
                        @endif
                        @php
                            $x++;
                        @endphp
                    @endforeach
                    @endif
                    </tbody>
                    @php
                        $i++;
                    @endphp
                @endforeach
           @endif
        </tbody>
    </table>
</form>
</div>


      <div class="modal" tabindex="-1" role="dialog" id="costAnalysisModal">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Cost Analysis </h5>
              <button type="button" class=" btn btn-danger pull-right"  data-dismiss="modal" aria-label="Close">Close</button>
              <button type="submit" class="btn btn-warning pull-right" style="margin-right:10px;" onclick="print_inv()" id="print" name="print" > Print</button>
             
            </div>
            @php
               
                $totalAmt = 0 ;
                $output = 0 ;
            @endphp
            <div class="modal-body table-responsive" id="cost-analysis-table">
               <table class="table table-bordered table-hovered" >
                    <tr>
                        <th>Sl.no</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Qty</th>
                        <th>Description</th>
                        <th>Total</th>
                    </tr>
                   
                    @foreach($gc_datas as $gc_data)
                        <?php
                            
                            // $gc_plots = explode(',',$gc_data->gc_plots);
                            // $total_plots = count($gc_plots) - 1;
                            $gc_amount = $gc_data->stock_total_cost ;
                            $gc_process = $gc_data->stock_process;
                            if($gc_process == 3 || $gc_process == 2 || $gc_process == 5)
                            {
                                $totalAmt += $gc_amount;
  
                        ?>
                        <tr>
                            <td style="text-align:center;">{{$gc_data->inv_ref_id}}</td>
                            <td style="text-align:center;"><?php if(!empty($gc_data->created_at)) { echo date('d-m-Y',strtotime($gc_data->created_at)); } ?></td>
                            <td style="text-align:right;">{{number_format($gc_amount,2,'.','')}}</td>
                            <td style="text-align:right;">{{$gc_data->stock_qty_new}}</td>
                            <td>{{$gc_data->inv_desc}}</td>
                            <td style="text-align:right;">{{number_format($totalAmt,2,'.','')}}</td>
                        
                        </tr>   
                        <?php 
                            }
                           
                         ?>    
                    @endforeach
                        
                    <tr>
                       
                        @php
                            $output = 0 ;
                        @endphp
                        @foreach($gc_datas as $gc_data)
                            <?php
                                
                                // $gc_plots = explode(',',$gc_data->gc_plots);
                                // $total_plots = count($gc_plots) - 1;
                                $gc_amount = $gc_data->stock_total_cost ;
                                $gc_process = $gc_data->stock_process;
                                $qty = $gc_data->stock_qty_new;
                                if($gc_process == 4)
                                {          
                                    $output = $totalAmt/ $qty ;
                            ?>
                            <tr>
                                <td style="text-align:center;">{{$gc_data->inv_ref_id}}</td>
                                <td style="text-align:center;"><?php if(!empty($gc_data->created_at)) { echo date('d-m-Y',strtotime($gc_data->created_at)); } ?></td>
                                <td style="text-align:right;">{{number_format($gc_amount,2,'.','')}}</td>
                                <td style="text-align:right;">{{$qty}}</td>
                                <td>{{$gc_data->inv_desc}}</td>
                                <td style="text-align:right;">{{number_format($output,2,'.','')}}</td>
                            
                            </tr>   
                            <?php 
                                }
                               
                             ?>    
                        @endforeach
                            
               </table>

               
            </div>
      
          </div>
        </div>
      </div>

      


@endsection

<script src="{{ asset('js/jquery.min.js') }}"></script>

<script>
 function print_this(fieldid,desc,start,end,id,subid){
        // console.log('id = ',id);
        // console.log('subid =',subid);
        var acres = $('#acres').val();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        } 

        if(mm<10) {
            mm = '0'+mm
        } 

        today =  dd + '/' + mm + '/' + yyyy;

        let qty = 0; let category = item = '';
        if(subid != 0)
        {
            qty = $('#item_qty_'+id+'_'+subid).val();
            category = document.getElementById('item_category_'+id+'_'+subid).options[document.getElementById('item_category_'+id+'_'+subid).selectedIndex].text;
            item = document.getElementById('item_stock_'+id+'_'+subid).options[document.getElementById('item_stock_'+id+'_'+subid).selectedIndex].text;
        }
        else
        {
            qty = $('#item_qty_'+id).val();
            category = document.getElementById('item_category_'+id).options[document.getElementById('item_category_'+id).selectedIndex].text;
            item = document.getElementById('item_stock_'+id).options[document.getElementById('item_stock_'+id).selectedIndex].text;
        }
        // console.log(category);
    //      let qty = $('#item_qty_'+id+'_'+subid).val();
        var new_Qty = acres * qty ;
        if(new_Qty == 0)
        {
            new_Qty = 'Nil';
        }
        if(category == 'Select'){ category = 'Nil' ;} if(item=='Select') { item = 'Nil' ;}
        var myWindow = window.open('','','width=1024,height=800');
        var content = '<h3 style="text-align:center">Delivery Order Date</h3><br/><span style="float:right;"> Date: '+today+'</span> Plot No:- '+fieldid+' &emsp; Acres: '+acres+'<br/><br /> <table style="width:100%;border-collapse:collapse;" border="1"><tr><th>Description</th><th>Start</th><th>End</th><th>Qty</th><th>Category</th><th>Item</th></tr><tr><td>'+desc+'</td><td>'+start+'</td><td>'+end+'</td><td>'+new_Qty+'</td><td>'+category+'</td><td>'+item+'</td><table>';
        myWindow.document.write(content);
        myWindow.print();
    }

    function print_inv()
    {
        var myWindow = window.open('','','width=1024,height=800');
            var  content = '<div id="div_print"  >';
                content +=  $('#cost-analysis-table').html();
                content += '</div>';

                content += '<style type="text/css">' +
        'table,table th, table td {' +
        'border:1px solid #000;' +
        'border-collapse:collapse;text-align:center;padding:2px; }' +  
        '#div_print table { width:100% ;}'+ 
        '</style>';
               
      
        // $('#div_print table tr td').css('border','1px solid #000');
        // $('#div_print table tr td').css('font-size','5px');
        myWindow.document.write(content);
        myWindow.print();
        myWindow.close();
    }
</script>

