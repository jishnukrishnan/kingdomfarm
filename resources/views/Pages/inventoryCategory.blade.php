@extends('layouts.navbar')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Inventory <i class="fa fa-angle-double-right" aria-hidden="true"></i> Category <i class="fa fa-angle-double-right" aria-hidden="true"></i> {{$category_name[0]->name}}  
            <a href="/inventory"><button class="btn btn-default pull-right" >Go Back</button></a></h2>
        
       
   
    <div class=" status-form">
           <div class="row col-md-12 table-responsive">
               <table class="table table-bordered text-center table-data-tr">
                    <tr>
                        <th>Sl.No</th>
                        <th>Time</th>
                        <th>Add/ Utilise</th>
                        <th>Stock Ledger</th>
                        <th>Stock Item</th> 
                        <th>Prev. Qty</th>                    
                        <th>Qty</th>
                        <th>Cur. Qty</th>
                        <th>Unit Cost</th>
                        <th>Total Cost</th>
                        <th>Issued Plot </th>
                        <th>Units</th>
                    </tr>
                    @if(count($inventories) > 0)
                    @php
                        $i = 1 ;
                    @endphp
                    @foreach($inventories as $items)
                    <tr >
                        <td>{{$items->inv_ref_id}}</td>
                        <td>{{date('d-m-Y h:i a',strtotime($items->created_at))}}</td>
                        <td class="left">
                            @php
                               if($items->stock_process == 1){
                                    echo "Added";
                               }
                               else if($items->stock_process == 2) {
                                   echo "Utilised";
                               }
                            @endphp
                        </td>
                        <td class="left">{{$items->category_name}}</td>
                        <td class="left">{{$items->item_name}}</td>    
                        <td>{{$items->stock_qty_prev}}</td>
                        <td>{{$items->stock_qty_new}}</td>
                        <td>{{$items->stock_qty_curr}}</td>
                        
                        <td>
                            <?php 
                                echo $items->stock_unit_cost ;
                             ?>
                        </td>
                        <td>
                            <?php 
                                echo $items->stock_total_cost ;
                         ?>
                        </td>
                        <td>
                            <?php if($items->stock_process == 2){
                                echo 'Field '.$items->stock_field_id ;
                            } ?>
                        </td>
                        <td class="left">{{$items->unit_name}}</td>
                        @php
                            $i++;
                        @endphp  
                    </tr>                           
                    @endforeach
                   
                @endif
                   
               </table>
              
           </div>
    </div>
   </div>
    
   
    
@endsection
<script src="{{ asset('js/jquery.min.js') }}"></script>