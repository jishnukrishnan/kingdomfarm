@extends('layouts.navbar')
<style>
        .mkyellow {
            color:#f0ad4e !important;
            border-color:#d58512 !important;
        }
        .mkred {
            color:#c9302c;
            border-color:#ac2925;
        }
    </style>
@section('content')

   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> General Costing <i class="fa fa-angle-double-right" aria-hidden="true"></i> Add</h2>
    <div class="submit-form">
            <meta name="csrf-token" content="{{ csrf_token() }}">
           <form action="/insertGenCost" method="POST">
                {{csrf_field()}}
                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                        <label for="gc_amount">Amount</label>
                        <input type="text" name="gc_amount" id="gc_amount" class="form-control" placeholder="Enter Amount" required>            
                    </div>
                    <input type="hidden" name="login_user_id" value="{{Auth::user()->id}}">
                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5">
                        <label for="gc_desc">Description</label>
                        <textarea name="gc_desc" id="gc_desc" rows="1" style="resize:none;" class="form-control" placeholder="Enter Description" required></textarea>                              
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="margin-bottom:5px;" id="gc_plots_div">
                        <label for="gc_plots" >PLOT ( Ctrl + )</label>
                        <select name="gc_plots[]" id="gc_plots" class="form-control" onchange="check_selection(1)"  required multiple>
                            @for($i = 1 ; $i <= 66 ; $i++)
                                <option value="{{$i}}">PLOT - {{$i}}</option>
                                @if($i == 55)
                                    <option value="67">PLOT - 55 A</option>
                                @endif
                            @endfor
                    </select>                        
                    </div>
                    <input type="hidden" name="us_login_user_id" value="{{Auth::user()->id}}">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                        <label for="gc_plots" >Select All</label><br />&emsp;
                        <input type="checkbox" onclick="check_selection(2)" id="gc_all_plots" name="gc_all_plots">
                    </div>
                    <input type="hidden" id="gc_id" name="gc_id">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <label  style="visibility: hidden;">Plot ffff</label>
                            <input type="submit" class="btn btn-primary " id="submit" name="submit" value="Add"> 
                    </div>
                </form>
                <form  action="/filtergclist" method="POST">
                    {{csrf_field()}}
                <div class="col-lg-1 col-md-1 col-sm-4 col-xs-4">
                        <label style="visibility: hidden;">Plot</label><br/>
                    <select name="show" id="show" class="form-control" onchange="this.form.submit()">
                        <option value=""> All </option>
                        <option value="50" <?php if(@$selected['show'] == 50){ echo 'selected' ; } ?>>50 </option>
                        <option value="100" <?php if(@$selected['show'] == 100){ echo 'selected' ; } ?>>100 </option>
                        <option value="200" <?php if(@$selected['show'] == 200){ echo 'selected' ; } ?>>200 </option>
                        <option value="500" <?php if(@$selected['show'] == 500){ echo 'selected' ; } ?>>500 </option>
                    </select>                        
                </div>
                <input type="hidden" class="btn btn-primary " id="submitFilter"  value="submitFilter"> 
            </form>
           </div>
           <div class=" col-md-12 table-responsive">
               <table class="table table-bordered text-center table-data-tr">
                    <tr>
                        <th>Sl.No</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Description</th>
                        <th>Plots </th>
                        {{-- <th>Action</th> --}}
                    </tr>

                    @if(count($gencost) > 0)
                    @php
                        // $i = 1 ;
                        // $i =  (($gencost->currentPage() - 1 ) * $gencost->perPage() ) + $i;
                      
                    @endphp
                    @foreach($gencost as $gc)
                    <tr>
                        <td style="width:5%;">{{$gc->inv_ref_id}}</td>
                        <td style="width:10%;">{{date('d-m-Y',strtotime($gc->created_at))}}</td>
                        <td style="text-align:right">{{number_format($gc->stock_total_cost)}}</td>
                        <td class="left" style="width:20%;">{{$gc->inv_desc}}</td>
                        <td class="left">
                            @if($gc->stock_field_id == 67)
                                {{'Plot 55 A'}}
                            @else
                                {{'Plot '.$gc->stock_field_id}}
                            @endif
                        </td>
                        {{-- <td style="width:20%;" class="action-icons">
                            <a  href="#" onclick="update_GC({{$gc->id}})"><i class="fa fa-pencil fa-2x mkyellow" aria-hidden="true"></i></a>
                            <a href="#" onclick="delete_GC({{$gc->id}})" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-times fa-2x mkred" aria-hidden="true"></i></a>
                      
                        </td>  --}}
                        @php
                            // $i++;
                        @endphp  
                    </tr>                           
                    @endforeach
                @endif
               
                   
               </table>
              
           </div>
    </div>
   </div>
    
   <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete General Costing ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="del_text"></p>
            </div>
            <div class="modal-footer">
                <form action="/deleteGC" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" id="del_id" name="del_id">
                    <input type="submit" class="btn btn-primary" value="Delete" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
          </div>
        </div>
      </div>

    
@endsection

<script>

    function check_selection(from) {
        var result = [];
        if(from == 1)
        {
            if($('#gc_all_plots').prop('checked') ==  true){
                $('#gc_all_plots').prop('checked',false);
            }
            // $('#gc_plots option:selected').css('background-color','rgb(60, 141, 188)');
            
        } 
        else if (from == 2)
        {
            if($('#gc_all_plots').prop('checked') == true)
            {
                $('#gc_plots option').attr('selected',true);
                for(var i = 0; i < 68; i++)
                {
                    result.push(i);
                }
                $('#gc_plots').val(result);
            }
            else{
                $('#gc_plots option').attr('selected',false);
                $('#gc_plots').val('');
            }
        }   
    }

    function update_GC(id)
    {
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/getGCItem') }}",
                  method: 'post',
                  data: {
                     id: id,
                  },
                  success: function(result){

                    $('#gc_amount').val(result['generalcost'][0].gc_amount);
                    $('#gc_desc').val(result['generalcost'][0].gc_desc);
                    //$('#gc_amount').val(result['generalcost'][0].gc_amount);
                    let plots = result['generalcost'][0].gc_plots;
                    let plot_array = plots.split(',');

                    $('#gc_plots_div').empty();
                    $('#gc_plots_div').append('<label for="gc_plots" >PLOT ( Ctrl + )</label>');

                    $('#gc_plots_div').append('<select name="gc_plots[]" id="gc_plots" class="form-control" required multiple><?php for($i = 1 ; $i < 21 ; $i++) { ?><option value="<?php echo $i ; ?>"  >PLOT - <?php echo $i ; ?></option> <?php  } ?> </select>');

                    for(let i = 0; i < 68; i++) {
                       for(let k = 0;k < plot_array.length; k++)
                       {
                           if(plot_array[k] == i)
                           {
                               $('#gc_plots option[value='+i+']').attr('selected','true')
                           }
                       }
                    }

                    $('#submit').val('Update');
                    $('#gc_id').val(id);

                  }
            });
    }

    function delete_GC(id) {
        document.getElementById('del_text').innerHTML = 'Are you sure to this delete General Costing ?';
        document.getElementById('del_text').style.color = 'red';
        document.getElementById('del_id').value = id ;
    }

   
</script>