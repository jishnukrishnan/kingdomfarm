@extends('layouts.navbar')
<style>
    .mkyellow {
        color:#f0ad4e !important;
        border-color:#d58512 !important;
    }
    .mkred {
        color:#c9302c;
        border-color:#ac2925;
    }
    </style>
@section('content')
   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Manage <i class="fa fa-angle-double-right" aria-hidden="true"></i> Units</h2>
    <div class="submit-form col-md-12 col-lg-12 col-sm-12">
           <form action="/units_ins" method="POST">
                {{csrf_field()}}
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-8">
                            <input type="text" class="form-control " name="unit_name" id="unit_name" placeholder="Unit Name" required>                              
                    </div>
                    <input type="hidden" id="unit_id" name="unit_id">
                    <input type="hidden" name="login_user_id" value="{{Auth::user()->id}}">
                    <div class="col-md-2 col-sm-4 col-xs-2">
                            <input type="submit" class="btn btn-primary " id="submit" name="submit" value="Add"> 
                    </div>
                </form>
           </div>
           <div class="row col-md-12 table-responsive">
               <table class="table table-bordered text-center table-data-tr">
                    <tr>
                        <th>Sl.No</th>
                        <th>Unit Name</th>
                        <th>Action</th>
                    </tr>
                    
                    @if(count($units) > 0)
                
                    @foreach($units as $unit)
                    <tr>
                        <td style="width:5%;">{{$unit->id}}</td>
                        <td >{{$unit->unit_name}}</td>
                        
                        <td style="width:20%;" class="action-icons">
                                <a href="#" onClick="update_unit('{{$unit->unit_name}}',{{$unit->id}})"><i class="fa fa-pencil fa-2x mkyellow" aria-hidden="true"></i></a>
                                <a href="#"  onClick="delete_unit('{{$unit->unit_name}}',{{$unit->id}})" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-times fa-2x mkred" aria-hidden="true"></i></a>
                      
                        </td> 
                  
                    </tr>                           
                    @endforeach
                @endif
                   
               </table>
           </div>
    </div>
   </div>
    
   <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Unit ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="del_text"></p>
            </div>
            <div class="modal-footer">
                <form action="/deleteUnit" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" id="del_id" name="del_id">
                    <input type="submit" class="btn btn-primary" value="Delete" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    
@endsection

<script>
    function update_unit(unit,id)
    {
        document.getElementById('unit_name').value = unit;
        document.getElementById('unit_id').value = id;
        document.getElementById('submit').value = "Update";
        document.getElementById('submit').innerHTML = "Update";
    }

    function delete_unit(unit,id) {
        document.getElementById('del_text').innerHTML = 'Are you sure to delete unit " '+unit+' " ?';
        document.getElementById('del_text').style.color = 'red';
        document.getElementById('del_id').value = id ;
    }
</script>