@extends('layouts.navbar')
<style>

    .field-head {
        display:inline;
        width:10% !important;
        font-size:15px;
        color:#262626;
        margin-left:15px;
    }
    .mk-pd-0
    {
        padding:0 !important;
    }
</style>
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
   <div class="">   
    <form action="/insert_template" method="POST">
        {{csrf_field()}}
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Template <i class="fa fa-angle-double-right" aria-hidden="true"></i> View <i class="fa fa-angle-double-right" aria-hidden="true"></i>
             <span style="color:#65ad23;">{{$template}}</span> 
             <input type="hidden" name="template_name" value="{{$template}}">
              <button class="btn btn-success pull-right" style="margin-bottom:10px;margin-right:10px" name="submit" value="Update" >Update</button>
        </h2>
        <div class="col-md-12 col-sm-12 table-responsive ">
        <table class="table table-bordered text-center table-data-tr">
            <tr>
                <th style="width:5%;">Sl.no</th>
                <th style="width:16%;">Description</th>
                <th style="width:10%;">Start Day</th>
                <th style="width:10%;">End Day</th>
                <th style="width:8%;">Status</th>
                <th style="width:4%;">Qty</th>
                <th style="width:16%;">Stock Ledger</th>
                <th style="width:12%;">Stock Name</th>           
            </tr>
 
           @if(count($templates) > 0)
                @php
                    $i = 1 ;
                    $needed = array(5,6,10,11,14,15,16);
                @endphp
                @foreach($templates as $template)      
                
                <tr class="table-data-tr">   

                        <td style="width:2%;">
                            @php
                                echo $i ;
                            @endphp
                           
                        </td>
                        <input type="hidden" name="template_id[]" value="{{$template->id}}" />
                        <td style="width:20%;" class="left">
                            @php
                                echo $template->description ;
                            @endphp
                             {{-- <input type="hidden" name="item_desc[]" value="{{$plot_data->description}}" /> --}}
                            <input type="hidden" id="from_day_{{$i}}" value={{$template->start_day}}>
                            <input type="hidden" id="to_day_{{$i}}" value={{$template->end_day}}>
                        </td>
                        
                        <td style="width:8%;" >
                        <input type="text" id="start_cal_{{$i}}" onchange="changeDate({{$i}})" name="start_day[]" placeholder="dd-mm-yyyy" data-date-start-date="{{date('d-m-Y')}}" value="{{date('d-m-Y',strtotime($template->start_date))}}" class="form-control mk-pd-0 text-center">
                            
                        </td>
                        <input type="hidden" id="prev_start_cal_{{$i}}" value="{{date('d-m-Y',strtotime($template->start_date))}}">
                        <td style="width:8%;" >
                            <input type="text" id="end_cal_{{$i}}"  name="end_day[]" placeholder="dd-mm-yyyy" value="{{date('d-m-Y',strtotime($template->end_date))}}" data-date-start-date="{{date('d-m-Y')}}" class="form-control mk-pd-0 text-center" disabled>
                            <input type="hidden" id="end_calv_{{$i}}"  name="end_dayv[]" value="{{$template->end_date}}">
                        </td>
                        <td style="width:8%;">
                            <select name="item_status[]" id="item_status_{{$i}}" class="form-control" >
                                <option value="">Select</option>
                                @if(count($status) > 0)
                                    @foreach($status as $stat)
                                        <option value="{{$stat->id}}" <?php if($stat->id == $template->item_status_id) { echo 'selected' ; } ?>>{{$stat->status}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </td>
                        <td style="width:4%;">
                        <input type="text" placeholder="Qty" name="item_qty[]"  id="item_qty_{{$i}}" value="{{$template->item_qty}}" <?php if(!in_array($i,$needed)){?> style="pointer-events:none;background:#ccc;" <?php } ?> class="form-control" style="width:80px;" >
                        </td>
                        <td style="width:16%;">
                            <select name="item_category[]" id="item_category_{{$i}}" <?php if(!in_array($i,$needed)){?> style="pointer-events:none;background:#ccc;" <?php } ?> onchange="getItem({{$i}})" class="form-control">
                                <option value="">Select</option>                                      
                                @if(count($categories) > 0)
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" <?php if($category->id == $template->item_ledger_id) { echo 'selected' ; } ?>>{{$category->category_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </td>
                        <td style="width:12%;">
                            <select name="item_stock[]" id="item_stock_{{$i}}"  <?php if(!in_array($i,$needed)){?> style="pointer-events:none;background:#ccc;" <?php } ?> class="form-control" >
                                <option value="">Select</option>
                                @if(count($stock_items) > 0)
                                @foreach($stock_items as $stock_item)
                                    <option value="{{$stock_item->id}}" <?php if($stock_item->id == $template->item_stock_id) { echo 'selected' ; } ?>>{{$stock_item->item_name}}</option>
                                @endforeach
                            @endif
                            </select>
                        </td>  
                    </tr>
                    @php
                        $i++;
                    @endphp
                @endforeach
          
           @endif
    </table>
    </div>
    </form>
   </div>

@endsection
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script>
    $( document ).ready(function() {
        var from , to ;
   
        for(var j = 1 ; j <= 24 ; j++){
            // from = $('#from_day_'+j).val();
            // to = $('#to_day_'+j).val();
            // from = parseInt(from) - 1;
            // to = parseInt(to) - 1;
            // var todaysDate = new Date();
            // var from_d = addDays(todaysDate,from);
            // var to_d = addDays(todaysDate,to);
            $("#start_cal_"+j).datepicker({
                format: 'dd-mm-yyyy',  
                autoclose: true,
                orientation: 'bottom'
            });
            $("#end_cal_"+j).datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true
            });
            // $('#start_cal_'+j).val(formatDate(from_d));
            // $('#prev_start_cal_'+j).val(formatDate(from_d));
            // $('#end_cal_'+j).val(formatDate(to_d));
            // $('#end_calv_'+j).val(formatDate(to_d));
            
        }


       
    });

  


    function cal_date(id){
        var length = $('#sortable li').length;
        var days = $('#item_days_'+id).val();
        days = parseInt(days);
        var prev = $("#cal_"+id).val();
        if(prev != '')
        {
            var ds = prev.split("-");
            var new_d = ds[2]+'-'+ds[1]+'-'+ds[0];
            var date = addDays(new_d, days);
            var date_to = formatDate(date);
            var next_id = parseInt(id) + 1 ;
            $('#cal_'+next_id).val(date_to);
        }

       
    }

    function changeDate(id){

        var prev_date  = $('#prev_start_cal_'+id).val();
        var stdate = $('#start_cal_'+id).val(); 
        var prev1 = prev_date.split("-");
        var prevD = prev1[2]+'-'+prev1[1]+'-'+prev1[0];      
        prev_date = new Date(prevD);
        var prev2 = stdate.split("-");
        var prevD2 = prev2[2]+'-'+prev2[1]+'-'+prev2[0];
        stdate = new Date(prevD2);
        var days = DateDiff(stdate,prev_date);
        $('#prev_start_cal_'+id).val(formatDate(prevD2));
       
        if(stdate != ''){
            for(var j = id; j <= 24  ;j++){  
                days = parseInt(days);
                startDate = $('#start_cal_'+j).val();   
                endDate = $('#end_cal_'+j).val();                 
                var ds1 = startDate.split("-");
                var new_d1 = ds1[2]+'-'+ds1[1]+'-'+ds1[0];
                var ds2 = endDate.split("-");
                var new_d2 = ds2[2]+'-'+ds2[1]+'-'+ds2[0];
                var from_d = addDays(new_d1,days);
                var to_d = addDays(new_d2,days);           
                $('#start_cal_'+j).val(formatDate(from_d));
                $('#end_cal_'+j).val(formatDate(to_d));
                $('#end_calv_'+j).val(formatDate(to_d));
                
            }
        }
        $('#start_cal_'+id).val(formatDate(prevD2));
    }

    function DateDiff(date1, date2) {
        var datediff = date1.getTime() - date2.getTime();
        return (datediff / (24*60*60*1000));
    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [ day, month, year].join('-');
    }

    function getItem(id){
            var cate_id = $('#item_category_'+id).val();
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/getItem') }}",
                  method: 'post',
                  data: {
                     id: cate_id,
                  },
                  success: function(result){
                    $('#item_stock_'+id).empty();
                    $('#item_stock_'+id).append('<option>Select</option>');
                    $.each(result.stock_items, function(i,item){
                        if(item){
                            $('#item_stock_'+id).append('<option value="'+item.id+'">'+item.item_name+'</option>');
                        }
                    })
                     

                  }
            });
         }

    

</script>