@extends('layouts.navbar')

<style>
#acre-label {
    padding: 8px;
    font-size: 18px;
    text-transform: uppercase;
}
.mk-disabled {
    pointer-events:none;
    background:#ccc;   
}
.mainArea .modal th ,.mainArea .modal td {
    color: #000;
}

#harvestQty {
    color:#000;
    float:right;
}

</style>
@section('content')

   <div class="">
   <h2 class="page-head">Manage Plot - <span style="color:#65ad23;"> <?php if($id==67){ echo '55 A' ; } else { echo $id ;} ?></span></h2>
   <a href="/dashboard" ><button class="btn btn-default pull-left" style="margin-bottom:10px;margin-right:10px" >Go Back</button></a>
    <button class="btn btn-warning pull-left" style="margin-bottom:10px;margin-right:10px" data-toggle="modal" <?php if(count($field_datas) > 0){ echo 'disabled ' ;} ?> id="loadTempBtn" data-target="#loadFromModal" >Load Template</button>
    <button class="btn btn-primary pull-left" style="margin-bottom:10px;margin-right:10px" data-toggle="modal" data-target="#costAnalysisModal" >Cost Analysis </button>
    
<form action="/field_data_insert" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="login_user_id" value="{{Auth::user()->id}}">
    <input type="hidden" id="fieldId" name="fieldId" value="{{$id}}">
    <input type="hidden" name="f_unique_id" value="<?php echo @$field_datas[0]->field_unique_id; ?>" />
    <button class="btn btn-success pull-right" id="submit_btn" name="submit_btn" style="margin-bottom:10px;margin-right:10px" value="Update">Update</button>
    <input type="text" name="acres" class="form-control headNameInp pull-right" id="acres" value="{{@$field_datas[0]->field_acres}}" style="margin-bottom:10px;margin-right:10px" placeholder="No of Acres" required>
    <label class="pull-right" id="acre-label">Acres</label>
    <button class="btn btn-danger pull-right" style="margin-bottom:10px;margin-right:10px"  name="finish" value="Finish Season" <?php if(count($field_datas) == 0){ echo 'disabled ' ;} ?> >Finish Season</button>
    <div class="col-md-12 col-sm-12 table-responsive">
    <table class="table table-bordered text-center table-data-tr">
        <thead>
            <tr>
                <th style="width:5%;">Sl.no</th>
                <th style="width:16%;">Description</th>
                <th style="width:10%;">Start Day</th>
                <th style="width:10%;">End Day</th>
                <th style="width:8%;">Status</th>
                <th style="width:4%;">Qty</th>
                <th style="width:16%;">Stock Ledger</th>
                <th style="width:12%;">Stock Name</th> 
                <th style="width:2%;">Add</th>
                <th style="width:2%;">Print</th>
            </tr>
        </thead>
        <tbody id="plotDetails" >
                @if(count($field_datas) > 0)
                @php
                    $i = 1 ;
                    $needed = array(5,6,10,11,14,15,16);
                   
                @endphp
                @foreach($field_datas as $field_data)   
                <input type="hidden" name="temp_name" value="{{$field_data->item_template}}" />   
                <tr class="table-data-tr">   
                    <input type="hidden" name="field_data_id[]" value="{{$field_data->id}}" />
                        <td style="width:2%;">
                            @php
                                echo $i ;
                            @endphp
                           
                        </td>
                        <td style="width:20%;" class="left">
                            @php
                                echo $field_data->description ;
                            @endphp
                             <input type="hidden" id="item_desc_{{$i}}" value="{{$field_data->description}}" />
                            <input type="hidden" id="from_day_{{$i}}" value={{$field_data->start_day}}>
                            <input type="hidden" id="to_day_{{$i}}" value={{$field_data->end_day}}>
                            @if($i == 19)
                        <input type="number" min="0" step="0.001" id="harvestQty" name="harvestQty" class="w-50"  placeholder="Harvest Qty" value="{{ @$harvest}}">
                            @endif
                        </td>
                     
                        <td style="width:8%;" >
                        <input type="text" id="start_cal_{{$i}}" onchange="changeDate({{$i}})" value="<?php echo date('d-m-Y',strtotime($field_data->start_date)); ?>" name="start_day[]" data-date-start-date="{{date('d-m-Y')}}" placeholder="dd-mm-yyyy" class="form-control mk-pd-0 text-center">
                            
                        </td>
                        <input type="hidden" id="prev_start_cal_{{$i}}" value="<?php echo date('d-m-Y',strtotime($field_data->start_date)); ?>">
                        <td style="width:8%;" >
                            <input type="text" id="end_cal_{{$i}}"  name="end_day[]" value="<?php echo date('d-m-Y',strtotime($field_data->end_date)); ?>" placeholder="dd-mm-yyyy" data-date-start-date="{{date('d-m-Y')}}" class="form-control mk-pd-0 text-center" tabindex="-1" disabled>
                            <input type="hidden" id="end_calv_{{$i}}"  name="end_dayv[]" <?php echo date('d-m-Y',strtotime($field_data->end_date)); ?>>
                        </td>
                        <td style="width:8%;">
                            <select name="item_status[]" id="item_status_{{$i}}" onchange="activateNextStatus(<?php echo $i ; ?>)" class="form-control" >
                                <option value="">Select</option>
                                @if(count($status) > 0)
                                    @foreach($status as $stat)
                                        <option value="{{$stat->id}}" <?php if($stat->id == $field_data->item_status) { echo 'selected' ; } ?>>{{$stat->status}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </td>
                        <td style="width:4%;">
                        <input type="number" placeholder="Qty" name="item_qty[]"  id="item_qty_{{$i}}" value="<?php if($field_data->item_qty > 0){ echo $field_data->item_qty; } ?>" <?php if(!in_array($i,$needed)){?> style = "pointer-events:none;background:#ccc;"  tabindex="-1" <?php }else{ echo "required" ;} ?>   step="0.01"   class="form-control" style="width:80px;" >
                        </td>
                        <td style="width:16%;">
                            <select name="item_category[]" id="item_category_{{$i}}" onchange="getItem({{$i}})" <?php if(!in_array($i,$needed)){?> style = "pointer-events:none;background:#ccc;"   tabindex="-1" <?php }else{ echo "required" ;} ?> class="form-control">
                                <option value="">Select</option>                                      
                                @if(count($categories) > 0)
                                    @foreach($categories as $category)
                            <option value="{{$category->id}}" <?php if($category->id == $field_data->item_category) {?> selected <?php } ?> >{{$category->category_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </td>
                        <td style="width:12%;">
                            <select name="item_stock[]" id="item_stock_{{$i}}" <?php if(!in_array($i,$needed)){?> style = "pointer-events:none;background:#ccc;"  tabindex="-1"  <?php }else{ echo "required" ;} ?> class="form-control" >
                                <option value="">Select</option>
                                @if(count($stock_items) > 0)
                                @foreach($stock_items as $stock_item)
                                    <option value="{{$stock_item->id}}" <?php if($stock_item->id == $field_data->item_stock) {?> selected <?php } ?>>{{$stock_item->item_name}}</option>
                                @endforeach
                            @endif
                            </select>
                        </td>  
                    <td style="width:2%;"> <?php if(in_array($i,$needed)){?> <a href="#" onclick="add_row({{$i}})" id="this_rw_add_{{$i}}"><i class="fa fa-plus"></i></a> <?php } ?></td>
                    <td style="width:2%;"> <a href="#" onclick="print_this('{{$id}}','{{$field_data->description}}','{{date('d-m-Y',strtotime($field_data->start_date))}}','{{date('d-m-Y',strtotime($field_data->end_date))}}',{{$i}},0)"><i class="fa fa-print"></i></a> </td>
                    </tr>
                    <tbody id="new_Row_{{$i}}">
                    @if(count($extra_field_d) > 0)
                    @php
                        $x = 1 ;
                    @endphp
                    @foreach($extra_field_d as $ef)
           
                        @if($ef->items_id == $i)
                            <tr id="new_Row_{{$i}}_{{$x}}" >
                            <input type="hidden" name="main_plot_id[]" value="{{$i}}"/>
                                <td colspan="5"></td>
                            
                            <td style="width:4%;">
                                <input type="text" placeholder="Qty" name="item_qty_sub[]"  id="item_qty_{{$i}}_{{$x}}" value="<?php if($ef->ef_item_qty > 0){ echo $ef->ef_item_qty; } ?>"  class="form-control" style="width:80px;" >
                            </td>
                            <td style="width:16%;">
                                <select name="item_category_sub[]" id="item_category_{{$i}}_{{$x}}" onchange="getItemSub({{$i}},{{$x}})"  class="form-control">
                                    <option value="">Select</option>              
                                    @if(count($categories) > 0)
                                        @foreach($categories as $category)
                                <option value="{{$category->id}}" <?php if($category->id == $ef->ef_item_category) {?> selected <?php } ?> >{{$category->category_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </td>
                            <td style="width:12%;">
                                <select name="item_stock_sub[]" id="item_stock_{{$i}}_{{$x}}" class="form-control" >
                                    <option value="">Select</option>
                                    @if(count($stock_items) > 0)
                                    @foreach($stock_items as $stock_item)
                                        <option value="{{$stock_item->id}}" <?php if($stock_item->id == $ef->ef_item_stock) {?> selected <?php } ?>>{{$stock_item->item_name}}</option>
                                    @endforeach
                                @endif
                                </select>
                            </td>  
                        <td><a href="#" onclick="rem_row({{$i}},{{$x}})" id="rem_row_id_{{$i}}"><i class="fa fa-times"></i></a></td>
                            <td><a href="#" onclick="print_this('{{$id}}',document.getElementById('item_desc_{{$i}}').value,document.getElementById('start_cal_{{$i}}').value,document.getElementById('end_calv_{{$i}}').value,'{{$i}}','{{$x}}')"><i class="fa fa-print"></i></a> </td>
                        </tr>
                        @endif
                        @php
                            $x++;
                        @endphp
                    @endforeach
                    @endif
                    </tbody>
                    @php
                        $i++;
                    @endphp
                @endforeach
           @endif
        </tbody>
    </table>
</form>
</div>

   <div class="modal" tabindex="-1" role="dialog" id="loadFromModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Load Template ?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                @if(count($templates) > 0)
                @foreach($templates as $template)
                    <input type="hidden" value="{{$template->template_name}}" name="template_name" id="template_name">
                    <button class="btn fieldBtn" onclick="fetch_template('<?php echo $template->template_name ; ?>')" data-dismiss="modal" > {{$template->template_name}}</button>
                @endforeach
                @else
                    <h3 class="text-danger">Sorry !!... No Templates Found <a href="/plot"><small><i>Click Here</i></small></a></h3>
                @endif
            </div>
      
          </div>
        </div>
      </div>
      

      <div class="modal" tabindex="-1" role="dialog" id="costAnalysisModal">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Cost Analysis </h5>
               <button type="button" class=" btn btn-danger pull-right"  data-dismiss="modal" aria-label="Close">Close</button>
               <button type="submit" class="btn btn-warning pull-right" style="margin-right:10px;" onclick="print_inv2()" id="print" name="print" > Print</button>
            </div>
            @php
               
                $totalAmt = 0 ;
                $output = 0 ;
            @endphp
            <div class="modal-body table-responsive" id="cost-analysis-table">
               <table class="table table-bordered table-hovered">
                    <tr>
                        <th>Sl.no</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Qty</th>
                        <th>Description</th>
                        <th>Total</th>
                    </tr>
                   
                    @foreach($gc_datas as $gc_data)
                        <?php
                            
                            // $gc_plots = explode(',',$gc_data->gc_plots);
                            // $total_plots = count($gc_plots) - 1;
                            $gc_amount = $gc_data->stock_total_cost ;
                            $gc_process = $gc_data->stock_process;
                            if($gc_process == 3 || $gc_process == 2 || $gc_process == 5)
                            {
                                $totalAmt += $gc_amount;
  
                        ?>
                        <tr>
                            <td style="text-align:center;">{{$gc_data->inv_ref_id}}</td>
                            <td style="text-align:center;"><?php if(!empty($gc_data->created_at)) { echo date('d-m-Y',strtotime($gc_data->created_at)); } ?></td>
                            <td style="text-align:right;">{{number_format($gc_amount,2,'.','')}}</td>
                            <td style="text-align:right;">{{$gc_data->stock_qty_new}}</td>
                            <td>{{$gc_data->inv_desc}}</td>
                            <td style="text-align:right;">{{number_format($totalAmt,2,'.','')}}</td>
                        
                        </tr>   
                        <?php 
                            }
                           
                         ?>    
                    @endforeach
                    
                    <tr>
                       
                            @php
                                $output = 0 ;
                            @endphp
                            @foreach($gc_datas as $gc_data)
                                <?php
                                    
                                    // $gc_plots = explode(',',$gc_data->gc_plots);
                                    // $total_plots = count($gc_plots) - 1;
                                    $gc_amount = $gc_data->stock_total_cost ;
                                    $gc_process = $gc_data->stock_process;
                                    $qty = $gc_data->stock_qty_new;

                                    if($gc_process == 4)
                                    {          
                                        $output = $totalAmt/ $qty ;
                                ?>
                                <tr>
                                    <td style="text-align:center;">{{$gc_data->inv_ref_id}}</td>
                                    <td style="text-align:center;"><?php if(!empty($gc_data->created_at)) { echo date('d-m-Y',strtotime($gc_data->created_at)); } ?></td>
                                    <td style="text-align:right;">{{number_format($gc_amount,2,'.','')}}</td>
                                    <td style="text-align:right;">{{$qty}}</td>
                                    <td>{{$gc_data->inv_desc}}</td>
                                    <td style="text-align:right;" >{{number_format($output,2,'.','')}}</td>
                                
                                </tr>   
                                <?php 
                                    }
                                   
                                 ?>    
                            @endforeach
                            
               </table>


            </div>
      
          </div>
        </div>
      </div>

      


@endsection

<script src="{{ asset('js/jquery.min.js') }}"></script>

<script>

    $( document ).ready(function() {
    
        setDatepicker();
        for(let i = 1;i < 25;i++)
        {
            check_status(i);    
            
            chStatus(i);
        }

    });

    function activateNextStatus(id)
    {
        let next_id = parseInt(id) + 1 ; 
        let idValue = $('#item_status_'+id).val();
        if(idValue == 3)
        {
            $('#item_status_'+next_id).css('pointer-events','auto');
        }
        else{
            for(var i = id + 1; i < 25; i++)
            {
                $('#item_status_'+i).val('');   
                chStatus(i);
            }
        }
        $('#item_status_1').css('pointer-events','auto');
        
        if(id >= 19 && $('#item_status_19').val() == 3)
        {
            $('#harvestQty').attr('required',true);
        }
        else{
            $('#harvestQty').attr('required',false);
        }

    }

    function chStatus(i)
    {
       // console.log(i);
        let idValue = $('#item_status_'+i).val();
        let prev_id = i - 1 ;
        let prev_id_value = 0 ;
        if(prev_id > 0)
        {
            prev_id_value = $('#item_status_'+prev_id).val();
        }
  
        if(prev_id_value == 3 && prev_id_value != 3)
        {
            $('#item_status_'+next_id).css('pointer-events','auto');   
        }
        else if(idValue == '' && prev_id_value == '' && i != 1){
            $('#item_status_'+i).css('pointer-events','none');
        }

        
    }

    function add_row(id)
    {

        let currLen = $('#new_Row_'+id+' tr').length + 1;
        let fieldId = $('#fieldId').val();
        // console.log(currLen);
        let desc = $('#item_desc_'+id).val();
        let startDate = $('#start_cal_'+id).val();
        let endDate = $('#end_calv_'+id).val();
        $('#new_Row_'+id).append('<tr id="new_Row_'+id+'_'+currLen+'"><td colspan="5"></td>'+
            '<input type="hidden" name="main_plot_id[]" value="'+id+'"/>'+
            '<td><input type="number" placeholder="Qty" name="item_qty_sub[]"  id="item_qty_'+id+'_'+currLen+'" class="form-control" style="width:80px;" required></td>'+
            '<td><select id="item_category_'+id+'_'+currLen+'" name="item_category_sub[]" onchange="getItemSub('+id+','+currLen+')" class="form-control" required><option value="">Select</option><?php foreach($categories as $stk_cate){?><option value="<?php echo $stk_cate->id; ?>"><?php echo $stk_cate->category_name ; ?></option><?php } ?></select></td>'+
            '<td><select id="item_stock_'+id+'_'+currLen+'"  name="item_stock_sub[]" class="form-control" required><option value=" ">Select</option><?php foreach($stock_items as $stk_it){?><option value="<?php echo $stk_it->id; ?>"><?php echo $stk_it->item_name ; ?></option><?php } ?></select></td>'+
            '<td><a href="#" onclick="rem_row('+id+','+currLen+')"><i class="fa fa-times"></i></a></td>'+
            '<td><a href="#" onclick="print_this(\''+fieldId+'\',\''+desc+'\',\''+startDate+'\',\''+endDate+'\','+id+','+currLen+')"><i class="fa fa-print"></i></a> </td>'+
            '</tr>');
      
            $('#item_qty_'+id+'_'+currLen).attr('step','0.01');
        

        // console.log(a); 

        //,'+document.getElementById('item_category_'+id+'_'+currLen).options[document.getElementById('item_category_'+id+'_'+currLen).selectedIndex].text+'
    }

    function rem_row(id,subid) {
        // var length = $('#sortable li').length;
        // var new_length = parseInt(length) - 1;
        // $('#row_count').val(new_length);
        $('#new_Row_'+id+'_'+subid).remove();
    }
    
   

    // function print_this(desc,start,end,qty,category,stock){
    //     var myWindow = window.open('','','width=1024,height=800')
    //     myWindow.document.write("Description : "+desc+" <br /> Start Date : "+start+ " <br /> End Date : "+end+" <br /> Qty : "+qty +" <br /> Legder : "+ category+"<br /> Item : "+stock);
    //     myWindow.print();
    // }

    function print_this(fieldid,desc,start,end,id,subid){
        // console.log('id = ',id);
        // console.log('subid =',subid);
        var acres = $('#acres').val();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        } 

        if(mm<10) {
            mm = '0'+mm
        } 

        today =  dd + '/' + mm + '/' + yyyy;

        let qty = 0; let category = item = '';
        if(subid != 0)
        {
            qty = $('#item_qty_'+id+'_'+subid).val();
            category = document.getElementById('item_category_'+id+'_'+subid).options[document.getElementById('item_category_'+id+'_'+subid).selectedIndex].text;
            item = document.getElementById('item_stock_'+id+'_'+subid).options[document.getElementById('item_stock_'+id+'_'+subid).selectedIndex].text;
        }
        else
        {
            qty = $('#item_qty_'+id).val();
            category = document.getElementById('item_category_'+id).options[document.getElementById('item_category_'+id).selectedIndex].text;
            item = document.getElementById('item_stock_'+id).options[document.getElementById('item_stock_'+id).selectedIndex].text;
        }
        // console.log(category);
    //      let qty = $('#item_qty_'+id+'_'+subid).val();
        var new_Qty = acres * qty ;
        if(new_Qty == 0)
        {
            new_Qty = 'Nil';
        }
        if(category == 'Select'){ category = 'Nil' ;} if(item=='Select') { item = 'Nil' ;}
        var myWindow = window.open('','','width=1024,height=800');
        var content = '<h3 style="text-align:center">Delivery Order Date</h3><br/><span style="float:right;"> Date: '+today+'</span> Plot No:- '+fieldid+' &emsp; Acres: '+acres+'<br/><br /> <table style="width:100%;border-collapse:collapse;" border="1"><tr><th>Description</th><th>Start</th><th>End</th><th>Qty</th><th>Category</th><th>Item</th></tr><tr><td>'+desc+'</td><td>'+start+'</td><td>'+end+'</td><td>'+new_Qty+'</td><td>'+category+'</td><td>'+item+'</td><table>';
        myWindow.document.write(content);
        myWindow.print();
    }

    function setDatepicker()
    {
        for(var k = 1 ; k <= 24 ; k++){

            $("#start_cal_"+k).datepicker({
                format:'dd-mm-yyyy',
                autoclose: true,
                startDate: '-3d',
                orientation: 'bottom'
            });
            $("#end_cal_"+k).datepicker({
                format:'dd-mm-yyyy',
                autoclose: true
            });
        }
    }

  

    function changeDate(id){
        //console.log(id);
        var prev_date  = $('#prev_start_cal_'+id).val();
        var stdate = $('#start_cal_'+id).val(); 
        var prev1 = prev_date.split("-");
        var prevD = prev1[2]+'-'+prev1[1]+'-'+prev1[0];      
        prev_date = new Date(prevD);
        var prev2 = stdate.split("-");
        var prevD2 = prev2[2]+'-'+prev2[1]+'-'+prev2[0];
        stdate = new Date(prevD2);
        var days = DateDiff(stdate,prev_date);
        $('#prev_start_cal_'+id).val(formatDate(prevD2));
        
        if(stdate != ''){
           
            for(var j = id; j <= 24  ;j++){  
                days = parseInt(days);
                startDate = $('#start_cal_'+j).val();   
                endDate = $('#end_cal_'+j).val();                 
                var ds1 = startDate.split("-");
                var new_d1 = ds1[2]+'-'+ds1[1]+'-'+ds1[0];
                var ds2 = endDate.split("-");
                var new_d2 = ds2[2]+'-'+ds2[1]+'-'+ds2[0];
                var from_d = addDays(new_d1,days);
                var to_d = addDays(new_d2,days);           
                $('#start_cal_'+j).val(formatDate(from_d));
                $('#end_cal_'+j).val(formatDate(to_d));
                $('#end_calv_'+j).val(formatDate(to_d));
                
            }
        }
        $('#start_cal_'+id).val(formatDate(prevD2));
    }

    function DateDiff(date1, date2) {
        var datediff = date1.getTime() - date2.getTime();
        return (datediff / (24*60*60*1000));
    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [ day, month, year].join('-');
    }

    function getItem(id){
            var cate_id = $('#item_category_'+id).val();
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/getItem') }}",
                  method: 'post',
                  data: {
                     id: cate_id,
                  },
                  success: function(result){
                    $('#item_stock_'+id).empty();
                    $('#item_stock_'+id).append('<option value="">Select</option>');
                    $.each(result.stock_items, function(i,item){
                        if(item){
                            $('#item_stock_'+id).append('<option value="'+item.id+'">'+item.item_name+'</option>');
                        }
                    })
                     

                  }
            });
    }

    function getItemSub(id,subid)
    {
        
        var cate_id = $('#item_category_'+id+'_'+subid).val();
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/getItem') }}",
                  method: 'post',
                  data: {
                     id: cate_id,
                  },
                  success: function(result){
                    $('#item_stock_'+id+'_'+subid).empty();
                    $('#item_stock_'+id+'_'+subid).append('<option value="">Select</option>');
                    $.each(result.stock_items, function(i,item){
                        if(item){
                            $('#item_stock_'+id+'_'+subid).append('<option value="'+item.id+'">'+item.item_name+'</option>');
                        }
                    })
                     

                  }
            });
    }

    function fetch_template(temp_name) {
          let field_id = $('#fieldId').val();
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/fetch_template') }}",
                  method: 'post',
                  data: {
                     name: temp_name,
                     field:field_id
                  },
                  success: function(result){
                    $('#plotDetails').empty();
                    $('#plotDetails').append('<input type="hidden" name="temp_name" value="'+result.templates[0].template_name+'" />');
                    $.each(result.templates, function(i,template){
                        if(template){
                            let j = i + 1 ;
                            let needed = [5,6,10,11,14,15,16];
                    $('#plotDetails').append('<tr class="table-data-tr"><td style="width:5%;">'+j+'</td>'+
                        '<td style="width:20%;" class="left">'+template.description+'</td>' +
                    '<input type="hidden" id="from_day_'+j+'" value="'+template.start_day+'">'+
                    '<input type="hidden" id="to_day_'+j+'" value="'+template.end_day+'">' +
                    '<td style="width:8%;" ><input type="text" id="start_cal_'+j+'" value="'+formatDate(template.start_date)+'" onchange="changeDate('+j+')" name="start_day[]" data-date-start-date="'+todayDate()+'" placeholder="dd-mm-yyyy" class="form-control mk-pd-0 text-center"></td>'+
                    '<input type="hidden" id="prev_start_cal_'+j+'" value="'+formatDate(template.start_date)+'">'+
                    '<td style="width:8%;" ><input type="text" id="end_cal_'+j+'"  value="'+formatDate(template.end_date)+'" name="end_day[]" placeholder="dd-mm-yyyy" data-date-start-date="'+todayDate()+'" class="form-control mk-pd-0 text-center" tabindex="-1" disabled>'+
                    '<input type="hidden" id="end_calv_'+j+'"  name="end_dayv[]" value="'+formatDate(template.end_date)+'"></td>'+
                    '<td style="width:8%;"><select name="item_status[]" id="item_status_'+j+'" onchange="check_status('+j+')" class="form-control" > <option value="">Select</option>'+
                    '<?php foreach($status as $stat){ ?><option value="<?php echo $stat->id; ?>"><?php echo $stat->status ; ?></option><?php } ?></select></td>'+
                    '<td style="width:4%;"><input type="number"  placeholder="Qty" name="item_qty[]"  id="item_qty_'+j+'"  class="form-control" style="width:80px;" ></td> '+
                    '<td style="width:16%;"><select name="item_category[]" id="item_category_'+j+'" onchange="getItem('+j+')"  class="form-control">'+
                    '<option value="">Select</option>     '+     
                    '<?php foreach($categories as $category){ ?><option value="<?php echo $category->id; ?>"  ><?php echo $category->category_name ; ?></option><?php } ?></select>'+                            
                             
                    '<td style="width:12%;">'+
                    ' <select name="item_stock[]" id="item_stock_'+j+'"  class="form-control" >'+
                    '<option value="">Select</option>'+
                    '<?php foreach($stock_items as $stock_item){ ?><option value="<?php echo $stock_item->id; ?>"  ><?php echo $stock_item->item_name ; ?></option><?php } ?></select>'+          
                    '</td><td></td><td></td></tr>');
                        if(template.item_status_id != 0)
                        {
                            $('#item_status_'+j).val(template.item_status_id) ;
                        }
                        if(template.item_ledger_id != 0)
                        {
                            $('#item_category_'+j).val(template.item_ledger_id) ;
                        }
                        if(template.item_stock_id != 0)
                        {
                            $('#item_stock_'+j).val(template.item_stock_id) ;
                        }

                        if(template.item_qty != 0)
                        {
                            $('#item_qty_'+j+'').val(template.item_qty);
                        }

                        // if(j > 1)
                        // {
                        //     $prev_status = $('#item_status_'+i).val();
                        //     if($prev_status != 3)
                        //     {
                        //         $('#item_status_'+j).addClass('mk-disabled');
                        //     }
                        // }

                        $('#item_status_'+j).addClass('mk-disabled');

                        if(needed.includes(j) == false){
                            $('#item_qty_'+j).css('pointer-events','none');
                            $('#item_qty_'+j).css('background','#ccc');
                            $('#item_category_'+j).css('pointer-events','none');
                            $('#item_category_'+j).css('background','#ccc');
                            $('#item_stock_'+j).css('pointer-events','none');
                            $('#item_stock_'+j).css('background','#ccc');
                            $('#item_qty_'+j).attr('tabindex',"-1");
                            $('#item_category_'+j).attr('tabindex',"-1");
                            $('#item_stock_'+j).attr('tabindex',"-1");

                        }

                     
                            $('#item_qty_'+j).attr('step','0.01');
                        

                        if(needed.includes(j) == true){
                            $('#item_qty_'+j).attr('required','required');
                            $('#item_category_'+j).attr('required','required');
                            $('#item_stock_'+j).attr('required','required');
                        }
                       
                        $('#submit_btn').html('Save');
                        $('#submit_btn').val('Save');


                        }
                       
                    })

                    setDatepicker();
                     

                  }
            });

            
    }

    function check_status(id)
    {     
        let prev_status = $('#item_status_'+id).val();
        const next_id = parseInt(id) + 1 ;
        if(prev_status == 3)
        {
            $('#item_status_'+id).addClass('mk-disabled');
            $('#start_cal_'+id).addClass('mk-disabled');
            $('#item_qty_'+id).addClass('mk-disabled');
            $('#item_category_'+id).addClass('mk-disabled');
            $('#item_stock_'+id).addClass('mk-disabled');
            $('#this_rw_add_'+id).css('pointer-events','none');
            $('#rem_row_id_'+id).css('pointer-events','none');
            for(var k = 0; k < 5 ; k++)
            {
                $('#item_qty_'+id+'_'+k).css('pointer-events','none');
                $('#item_category_'+id+'_'+k).css('pointer-events','none');
                $('#item_stock_'+id+'_'+k).css('pointer-events','none');
            }
        }
       
       
            
           
    }


    function todayDate()
    {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        } 

        if(mm<10) {
            mm = '0'+mm
        } 

        today = dd + '-' + mm + '-'  + yyyy;
        return today;
    }

    function print_inv2()
    {
        var myWindow = window.open('','','width=1024,height=800');
            var  content = '<div id="div_print"  >';
                content +=  $('#cost-analysis-table').html();
                content += '</div>';

                content += '<style type="text/css">' +
        'table,table th, table td {' +
        'border:1px solid #000;' +
        'border-collapse:collapse;text-align:center;padding:2px; }' +  
        '#div_print table { width:100% ;}'+ 
        '</style>';
               
      
        // $('#div_print table tr td').css('border','1px solid #000');
        // $('#div_print table tr td').css('font-size','5px');
        myWindow.document.write(content);
        myWindow.print();
        myWindow.close();
    }

</script>