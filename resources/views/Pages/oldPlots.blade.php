@extends('layouts.navbar')
<style>
    .mkred {
        color:#c9302c;
        border-color:#ac2925;
    }
</style>
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
   <div class="">   
    {{-- <form action="/field_data_insert" method="POST">
        {{csrf_field()}} --}}
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Old Plot Datas <i class="fa fa-angle-double-right" aria-hidden="true"></i> View
            
        </h2>
        <div class="col-md-12 table-responsive" >
        <table class="table table-bordered text-center table-data-tr">
            <tr>
                <th style="width:10%;">Sl.no</th>
                <th style="width:20%;">Plot Unique Id</th>
                <th style="width:20%;">Plot </th>
                <th style="width:20%;">Acres </th>
                <th style="width:20%;">Template </th>
                <th style="width:20%;">Start Date </th>
                <th style="width:10%;">Action</th>        
            </tr>
            @if(count($plotdatas) > 0)
                <?php  $i = 1 ;
                ?>              
                @foreach($plotdatas as $plotdata)
   
                    <tr>
                        <td><?php echo $i ; ?></td>
                        <td ><?php echo $plotdata->field_unique_id ; ?></td>
                        <td><?php if($plotdata->item_field == 67) { echo '55 A'; } else { echo $plotdata->item_field ;} ; ?></td>
                        <td><?php echo $plotdata->field_acres ; ?></td>
                        <td><?php echo $plotdata->item_template ; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($plotdata->created_at))  ; ?></td>
                        <td>
                            <a href="/viewSingleOldPlotData/{{$plotdata->field_unique_id}}" ><i class="fa fa-file fa-2x" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <?php  $i++ ;
                    ?>     
                @endforeach
            @endif

         

    </table>
    </div>
    {{-- </form> --}}
   </div>

   

@endsection
