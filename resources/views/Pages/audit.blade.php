@extends('layouts.navbar')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Audits  </h2>
            
   
<div class="row submit-form col-md-12 col-sm-12">
                <form action="/filterAudit" method="POST">
                        {{csrf_field()}}

                        <div class="col-lg-1 col-md-2 col-sm-4 col-xs-4">
                                <label>From  </label>
                                <input type="text" class="form-control " name="from_d" id="from_d" value="<?php echo @$selected['from_date']; ?>" placeholder="dd-mm-yy" >                              
                        </div>
                        <div class="col-lg-1 col-md-2 col-sm-3 col-xs-3">
                                <label>To </label>
                                <input type="text" class="form-control " name="to_d" id="to_d" value="<?php echo @$selected['to_date']; ?>" placeholder="dd-mm-yy" >                             
                        </div>


                        <div class="col-lg-2 col-md-2 col-sm-4  col-xs-4">
                            <label>Stock Item </label>
                                <select  class="form-control " name="stock_item" id="stock_item" >    
                                    <option value="">Select Stock Item</option>
                                    @if(count($stock_items) > 0)
                                        @foreach($stock_items as $items)
                                            <option value="{{$items->id}}" <?php if($items->id == @$selected['stock_item']){ echo 'selected' ;} ?>>{{$items->item_name}}</option>
                                        @endforeach
                                    @endif
                                </select>                        
                        </div>
                            
                           
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                            <label>Plot</label>
                            <select name="plot_id" id="plot_id" class="form-control" >
                                <option value="">Select Plot</option>
                                
                                    @for($i = 1; $i < 67; $i++)
                                        <option value="{{$i}}" <?php if(@$selected['plot_id'] == $i){ echo 'selected' ; } ?>>Plot {{$i}}</option>
                                        @if($i == 55)
                                            <option value="67" <?php if(@$selected['plot_id'] == 67){ echo 'selected' ; } ?>>Plot 55 A</option>
                                        @endif
                                    @endfor
                                
                            </select>                        
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                            <label>Add/ Utilise</label>
                            <select name="process_id" id="process_id" class="form-control" >
                                <option value="">Select </option>
                                <option value="1" <?php if(@$selected['process'] == 1){ echo 'selected' ; } ?>>Added </option>
                                <option value="2" <?php if(@$selected['process'] == 2){ echo 'selected' ; } ?>>Utilised </option>
                            </select>                        
                        </div>

                        <input type="hidden" name="onlyShowValue" id="onlyShowValue" />
                            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
                                    <label style="visibility: hidden;">Plot</label><br/>
                                    <input type="submit" class="btn btn-success " id="filter" name="filter" value="Filter"> 
                            </div>

                            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
                                    <label style="visibility: hidden;">Plot</label><br/>
                                    <input type="submit" class="btn btn-danger " id="reset" name="reset" value="Reset"> 
                            </div>

                           

                        </form>

                            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
                                    <label style="visibility: hidden;">Plot</label><br/>
                                    <button type="submit" class="btn btn-primary pull-right" style="margin-top:-15px" onclick="print_inv()" id="print" name="print" > Print</button>
                            </div>

                            <div class="col-lg-1 col-md-1 col-sm-4 col-xs-4">
                                    <label style="visibility: hidden;">Plot</label><br/>
                                <select name="show" id="show" class="form-control" style="margin-top:-15px" onchange="onlyShow()">
                                    <option value=""> All </option>
                                    <option value="50" <?php if(@$selected['show'] == 50){ echo 'selected' ; } ?>>50 </option>
                                    <option value="100" <?php if(@$selected['show'] == 100){ echo 'selected' ; } ?>>100 </option>
                                    <option value="200" <?php if(@$selected['show'] == 200){ echo 'selected' ; } ?>>200 </option>
                                    <option value="500" <?php if(@$selected['show'] == 500){ echo 'selected' ; } ?>>500 </option>
                                </select>                        
                            </div>
                      
                </div>

           <div class="row col-md-12 col-sm-12 table-responsive" id="audit-table">
               <table class="table table-bordered text-center table-data-tr " >
                    <tr>
                        <th style="width:2%;">Sl.No</th>
                        <th style="width:8%;">Time</th>
                        <th style="width:7%;">Add/ Utilise</th>
                        <th style="width:7%;">Stock Ledger</th>
                        <th style="width:8%;">Stock Item</th> 
                        <th style="width:4%;">Prev. Qty</th>                    
                        <th style="width:4%;">Qty</th>
                        <th style="width:4%;">Cur. Qty</th>
                        <th style="width:5%;">Unit Cost</th>
                        <th style="width:4%;">Total Cost</th>
                        <th style="width:4%;"> Plot </th>
                        <th style="width:4%;"> Unit </th>
                    </tr>
                    @php
                        $addtotal =   0 ;
                       
                    @endphp
                    @if(count($audits) > 0)
                   
                    @foreach($audits as $items)
                    @php
                         $addtotal += $items->stock_total_cost ;
                     
                    @endphp
                    <tr >
                        <td>{{$items->inv_ref_id}}</td>
                        <td >{{date('d-m-Y h:i a',strtotime($items->created_at))}}</td>
                        <td class="left">
                            @php
                               if($items->stock_process == 1){
                                    echo "Added";
                               }
                               else if($items->stock_process == 2) {
                                   echo "Utilised";
                               }
                               else if($items->stock_process == 3) {
                                   echo "General Costing";
                               }
                               else if($items->stock_process == 5) {
                                   echo "Workshop Utilised";
                               }

                            @endphp
                        </td>
                        <td class="left">{{$items->category_name}}</td>
                 
                        <td class="left">{{$items->item_name}}</td>    
                    <td><?php if($items->stock_process != 3){ ?> {{$items->stock_qty_prev}} <?php } ?></td>
                        <td><?php if($items->stock_process != 3){ ?>{{$items->stock_qty_new}} <?php } ?></td>
                        <td><?php if($items->stock_process != 3){ ?>{{$items->stock_qty_curr}} <?php } ?></td>
                        
                        <td>
                            <?php if($items->stock_process != 3){
                                echo  number_format($items->stock_unit_cost,2);
                                }   
                             ?>
                        </td>
                      
                        <td>
                        <?php 
                            echo  number_format($items->stock_total_cost,2);
                            
                            ?>
                            </td>
                        <td>
                            <?php if($items->stock_process == 2 || $items->stock_process == 3 || $items->stock_process == 5){
                                if($items->stock_field_id == '67'){
                                    echo 'Plot 55 A' ;
                                }
                               else
                               {
                                echo 'Plot '.$items->stock_field_id ;
                               }
                            } ?>
                        </td>
                        <td>{{$items->unit_name}}</td>
                    </tr>   
  
                    @endforeach
                   
                @endif
                <tr>
                    <td colspan="9">TOTAL</td>
                    <td>{{number_format($addtotal,2)}}</td>
                    <td colspan="2"></td>
                </tr>
                  
               </table>
           </div>
    </div>
   </div>

@endsection
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script>

    $(document).ready(function(){
        $("#from_d").datepicker({
                format:'dd-mm-yyyy',
                autoclose: true,
                orientation: 'bottom'
        });
        $("#to_d").datepicker({
                format:'dd-mm-yyyy',
                autoclose: true,
                orientation: 'bottom'
        });

    })

    function print_inv()
    {
        var myWindow = window.open('','','width=1024,height=800');
            var  content = '<div id="div_print"  >';
                content +=  $('#audit-table').html();
                content += '</div>';

                content += '<style type="text/css">' +
        'table,table th, table td {' +
        'border:1px solid #000;' +
        'border-collapse:collapse;text-align:center;     }' +
        'table tr td:nth-child(2), tr th:nth-child(2) { width:15%;}' +
        '</style>';
               
      
        // $('#div_print table tr td').css('border','1px solid #000');
        // $('#div_print table tr td').css('font-size','5px');
        myWindow.document.write(content);
        myWindow.print();
        myWindow.close();
    }

    function onlyShow()
    {
        const show = $('#show').val();
        $('#onlyShowValue').val(show);

        $('#filter').click();
        
    }
</script>