@extends('layouts.navbar')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
   <div class="">
        <h2 class="page-head"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Inventory  <button class="btn btn-danger pull-right round-btn "  data-toggle="modal" data-target="#utiliseStockModal"><i class="fa fa-minus fa-2x"></i></button>
            <button class="btn btn-success pull-right round-btn " data-toggle="modal" data-target="#addStockModal"><i class="fa fa-plus fa-2x"></i></button></h2>
        
       
   
    <div class=" status-form">
           <div class="row col-md-12 col-sm-12 table-responsive">
               <table class="table table-bordered text-center  table-data-tr ">
                    <tr>
                        <th>Sl.No</th>
                        <th>Time</th>
                        <th>Add/ Utilise</th>
                        <th>Stock Ledger</th>
                        <th>Stock Item</th> 
                        <th>Prev. Qty</th>                    
                        <th>Qty</th>
                        <th>Cur. Qty</th>
                        <th>Unit Cost</th>
                        <th>Total Cost</th>
                        <th>Issued Plot </th>
                        <th>Unit</th>
                    </tr>
                    @if(count($inventories) > 0)
                    @php
                        $i = 1 ;
                        $count = 0 ;
                        $cate = [];
                    @endphp
                    @foreach($inventories as $items)
                    @php
                        array_push($cate,$items->stock_category);
                        $counts = array_count_values($cate);
                        $times = $counts[$items->stock_category];
                    @endphp
                    @if($times <= 3)    
                    <tr >
                        <td>{{$items->inv_ref_id}}</td>
                        <td>{{date('d-m-Y h:i a',strtotime($items->created_at))}}</td>
                        <td class="left">
                            @php
                               if($items->stock_process == 1){
                                    echo "Added";
                               }
                               else if($items->stock_process == 2) {
                                   echo "Utilised";
                               }
                            @endphp
                        </td>
                        <td class="left"><a href="/filterInventory/{{$items->stock_category}}">{{$items->category_name}} </a></td>
                 
                        <td class="left">{{$items->item_name}}</td>    
                        <td>{{$items->stock_qty_prev}} </td>
                        <td>{{$items->stock_qty_new}} </td>
                        <td>{{$items->stock_qty_curr}} </td>
                        
                        <td>
                            <?php 
                                echo  number_format($items->stock_unit_cost,2);
                             ?>
                        </td>
                        <td>
                            <?php 
                                echo  number_format($items->stock_total_cost,2);
                             ?>
                        </td>
                        <td>
                            <?php if($items->stock_process == 2){
                                if($items->stock_field_id == '67'){
                                    echo 'Plot 55 A' ;
                                }
                               else
                               {
                                echo 'Plot '.$items->stock_field_id ;
                               }
                            } ?>
                        </td>
                        <td>
                            {{$items->unit_name}}
                        </td>
                        @php
                            $i++;
                        @endphp  
                    </tr>   
                    @endif      
                    @endforeach
                   
                @endif
                   
               </table>
               {{-- {{$inventories->links()}} --}}
           </div>
    </div>
   </div>
    
   <div class="modal" tabindex="-1" role="dialog" id="addStockModal">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Add Stock </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fa fa-remove"></i></span>
              </button>
            </div>
            <form action="/addStock" method="POST">
                {{csrf_field()}}
            <div class="modal-body col-md-12">

                <div class="col-md-3">
                        <div class="form-group">
                                <label for="">Category</label>
                                <select name="as_category_id" id="as_category_id" class="form-control" onchange="getItem(1)" required>
                                    <option value="">Select</option>
                                    @if(count($categories) > 0)
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                        </div>
                </div>
                
                <div class="col-md-3">
                        <div class="form-group">
                                <label for="">Item</label>
                                <select name="as_item_id" id="as_item_id" onchange="setCost(this.value)" class="form-control" required>
                                        <option value="">Select</option>
                                        @if(count($stock_items) > 0)
                                            @foreach($stock_items as $item)
                                                <option value="{{$item->id}}">{{$item->item_name}}</option>
                                            @endforeach
                                        @endif
                                </select>
                                
                        </div>
                </div>
                <div class="col-md-2">
                        <div class="form-group">
                                <label for="">Qty</label>
                                <input type="number" min="0" step="0.01" name="as_stock_qty" id="as_stock_qty" class="form-control" onchange="calTotal()" placeholder="qty" required>
                                <label style="display:none" id="stck_q_label1" for="">Available : <span id="stck_q1"></span></label>
                        </div>
                        
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                            <label for="">Unit Cost</label>
                            <input type="text" name="as_stock_unit_cost" id="as_stock_unit_cost" onchange="calTotal()" class="form-control" style="pointer-events:none;
                            background:#ccc;" placeholder="Unit" >
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                            <label for="">Total Cost</label>
                            <input type="text" name="as_stock_total_cost1" id="as_stock_total_cost1" class="form-control" placeholder="Total" disabled>
                            <input type="hidden" name="as_stock_total_cost" id="as_stock_total_cost" class="form-control"  >
                    </div>
                </div>
     
            </div>
            <input type="hidden" name="as_stock_process" id="as_stock_process" value="1">
            <input type="hidden" name="as_login_user_id" value="{{Auth::user()->id}}">
            <div class="modal-footer">
                <input type="submit" class="btn btn-success" id="submit" value="Add" />
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>    
            </div>
          </div>
        </form>
        </div>
      </div>


      <div class="modal" tabindex="-1" role="dialog" id="utiliseStockModal">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Utilise Stock </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fa fa-remove"></i></span>
              </button>
            </div>
            <form action="/utiliseStock" method="POST">
                {{csrf_field()}}
            <div class="modal-body col-md-12">

                <div class="col-md-3">
                        <div class="form-group">
                                <label for="">Category</label>
                                <select name="us_category_id" id="us_category_id" class="form-control" onchange="getItem(2)" required>
                                    <option value="">Select</option>
                                    @if(count($categories) > 0)
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                        </div>
                </div>
                
                <div class="col-md-3">
                        <div class="form-group">
                                <label for="">Item</label>
                                <select name="us_item_id" id="us_item_id" class="form-control" onchange="get_avail_stock(this.value,2)" required>
                                        <option value=" ">Select</option>
                                        @if(count($stock_items) > 0)
                                            @foreach($stock_items as $item)
                                                <option value="{{$item->id}}">{{$item->item_name}}</option>
                                            @endforeach
                                        @endif
                                </select>
                        </div>
                </div>
                <div class="col-md-2">
                        <div class="form-group">
                                <label for="us_stock_qty">Qty</label>
                                <input type="number" min="0" step="0.01" name="us_stock_qty" id="us_stock_qty" class="form-control" placeholder="qty" required>
                        </div>
                        <label style="display:none" id="stck_q_label2" for="">Available : <span id="stck_q2"></span></label>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="margin-bottom:5px;" id="gc_plots_div">
                        <label for="us_plots" >PLOT ( Ctrl + )</label>
                        <select name="us_plots[]" id="us_plots" class="form-control" onchange="check_selection(1)"  required multiple>
                            @for($i = 1 ; $i <= 66 ; $i++)
                                <option value="{{$i}}">PLOT - {{$i}}</option>
                                @if($i == 55)
                                    <option value="67">PLOT - 55 A</option>
                                @endif
                            @endfor
                    </select>                        
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                        <label for="us_plots" >Select All</label><br />&emsp;
                        <input type="checkbox" onclick="check_selection(2)" id="us_all_plots" name="us_all_plots">
                    </div>
     
            </div>
            <input type="hidden" name="us_stock_process" id="us_stock_process" value="2">
            <input type="hidden" name="us_login_user_id" value="{{Auth::user()->id}}">
            <div class="modal-footer">
                <input type="submit" class="btn btn-danger" id="submit" value="Utilise" />
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>    
            </div>
          </div>
        </form>
        </div>
      </div>
    
@endsection

<script>

    function check_selection(from) {
        var result = [];
        if(from == 1)
        {
            if($('#us_all_plots').prop('checked') ==  true){
                $('#us_all_plots').prop('checked',false);
            }
            // $('#gc_plots option:selected').css('background-color','rgb(60, 141, 188)');
            
        } 
        else if (from == 2)
        {
            if($('#us_all_plots').prop('checked') == true)
            {
                $('#us_plots option').attr('selected',true);
                for(var i = 0; i < 68; i++)
                {
                    result.push(i);
                }
                $('#us_plots').val(result);
            }
            else{
                $('#us_plots option').attr('selected',false);
                $('#us_plots').val('');
            }
        }   
    }

    function calTotal()
    {
        let unit_cost = $('#as_stock_unit_cost').val();
        unit_cost = unit_cost.replace(',','');
        let qty = $('#as_stock_qty').val();
        let total = unit_cost * qty;
        $('#as_stock_total_cost').val(total);
        total = (total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'); 
        $('#as_stock_total_cost1').val(total);
       
    }

    function setCost(val)
    {
        const opvalue = document.getElementById('as_item_id').options[document.getElementById('as_item_id').selectedIndex].text;
        const unit_cost = opvalue.split('-');
        $('#as_stock_unit_cost').val(unit_cost[1]);
        calTotal();
        get_avail_stock(val,1);
    }

    function get_avail_stock(id,value)
    {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/getAvailItem') }}",
                  method: 'post',
                  data: {
                     id: id,
                  },
                  success: function(result){
                      //  console.log(result);
                      if(value == 1)
                      {
                        $('#stck_q_label1').css('display','block');
                        $('#stck_q1').html(result.qty);
                        $('#stck_q1').css('color','green');
                      }
                      else if(value == 2)
                      {
                        $('#stck_q_label2').css('display','block');
                        $('#stck_q2').html(result.qty);
                        $('#stck_q2').css('color','green');
                      }
                  }
            });
    }

    function getItem(from){
         if(from == 1)
         {
            var id = $('#as_category_id').val();
         }
         else if(from == 2){
            var id = $('#us_category_id').val();
         }
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: "{{ url('/getItem') }}",
                  method: 'post',
                  data: {
                     id: id,
                  },
                  success: function(result){
 
                    if(from == 1)
                    {
                        $('#as_item_id').empty();
                        $('#as_item_id').append('<option>Select</option>');
                    }
                    else if(from == 2){
                        $('#us_item_id').empty();
                        $('#us_item_id').append('<option>Select</option>');
                    }
                                
                    $.each(result.stock_items, function(i,item){
                        if(item){

                            if(from == 1)
                            {
                                $('#as_item_id').append('<option value="'+item.id+'">'+item.item_name+'</option>');
                            }
                            else if(from == 2)
                            {
                                $('#us_item_id').append('<option value="'+item.id+'">'+item.item_name+'</option>');
                            }
                            
                        }
                    })
                     

                  }
            });
         }

</script>