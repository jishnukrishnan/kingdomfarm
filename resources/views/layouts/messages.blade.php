@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <div class="alert alert-danger">
            {{$error}}
        </div>
    @endforeach
@endif
        
@if(session('success'))
    <div class="alert alert-success setTime" >
        {{session('success')}}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger setTime">
        {{session('error')}}
    </div>
@endif

<script>
    setTimeout(function(){ $('.setTime').fadeOut('slow'); }, 2000);
</script>