<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content=" ">
		<meta name="author" content=" ">
        <title>{{config('App.name', 'Kingdom - Farm')}}</title>
        <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
		<!-- Custom styles for this template -->
        <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
        <link href="{{ asset('css/owl.theme.default.min.css')}}"  rel="stylesheet">
        <link href="{{ asset('css/style.css')}}" rel="stylesheet">
        <!-- Fonts -->
    </head>
    <body>
        <body id="page-top">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand page-scroll" href="#"><h1><b>Zeus Agro Farm</b></h1></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="hidden">
                                <a href="#page-top"></a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#">Home</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#about">About</a>
                            </li>
                            <!-- <li>
                                <a class="page-scroll" href="#features">Features</a>
                            </li> -->
                            <li>
                                <a class="page-scroll" href="#contact">Contact</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <!-- Header -->
            <header >
                <div class="container-fluid">
                    <div class="slider-container">
    
                            <div class="owl-slider owl-carousel">
                                <div class="item">
                                    <div class="owl-slider-item">
                                        <img src="{{ asset('img/slider-1.jpeg') }}" class="img-responsive" alt="portfolio">
                                        <div class="intro-text">
                                            <div class="intro-lead-in">We are the number one</div>
                                            <div class="intro-heading">Lorem, ipsum. </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="owl-slider-item">
                                        <img src="{{ asset('img/slider-2.jpeg') }}" class="img-responsive" alt="portfolio">
                                        <div class="intro-text">
                                            <div class="intro-lead-in">We are the number one</div>
                                            <div class="intro-heading">Lorem ipsum dolor sit amet.</div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
    
                    </div>
                </div>
            </header>
    
    
            <section id="about" class="light-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="section-title">
                                <h2>About</h2>
                                <p>Zeus Farm is a certified organic farm raising vegetables, berries, and crops for over 200 Farm Share Members so, <br><strong>they eat healthier, feel better, save money</strong></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- about module -->
                        <div class="col-md-3 text-center">
                            <div class="mz-module">
                                <div class="mz-module-about">
                                    <i class="fa fa-briefcase color1 ot-circle"></i>
                                    <h3>ORGANIC</h3>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
                                </div>
                                <!-- <a href="#" class="mz-module-button">read more</a> -->
                            </div>
                        </div>
                        <!-- end about module -->
                        <!-- about module -->
                        <div class="col-md-3 text-center">
                            <div class="mz-module">
                                <div class="mz-module-about">
                                    <i class="fa fa-photo color2 ot-circle"></i>
                                    <h3>HEALTHY</h3>
                                    <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe</p>
                                </div>
                                <!-- <a href="#" class="mz-module-button">read more</a> -->
                            </div>
                        </div>
                        <!-- end about module -->
                        <!-- about module -->
                        <div class="col-md-3 text-center">
                            <div class="mz-module">
                                <div class="mz-module-about">
                                    <i class="fa fa-camera-retro color3 ot-circle"></i>
                                    <h3>TASTY</h3>
                                    <p>Accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
                                </div>
                                <!-- <a href="#" class="mz-module-button">read more</a> -->
                            </div>
                        </div>
                        <!-- end about module -->
                        <!-- about module -->
                        <div class="col-md-3 text-center">
                            <div class="mz-module">
                                <div class="mz-module-about">
                                    <i class="fa fa-cube color4 ot-circle"></i>
                                    <h3>FRESH</h3>
                                    <p> Itaque earum rerum hic tenetur a sapiente, ut aut reiciendis maiores</p>
                                </div>
                                <!-- <a href="#" class="mz-module-button">read more</a> -->
                            </div>
                        </div>
                        <!-- end about module -->
                    </div>
                </div>
                <!-- /.container -->
            </section>
    
            <!-- <section id="features" class="section-features">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="section-title">
                                <h2>Farm Features</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row row-gutter">
                        <div class="col-md-4 col-gutter">
                            <div class="featured-item">
                                <div class="featured-icon"><i class="fa fa-television"></i></div>
                                <div class="featured-text">
                                    <h4>Lorem ipsum dolor sit amet.</h4>
                                    <p>INFRA theme looks awesome at any size, be it a Laptop screen, Mobile or Tablet.</p>
                                </div>
                            </div>
                            <div class="featured-item">
                                <div class="featured-icon"><i class="fa fa-cogs"></i></div>
                                <div class="featured-text">
                                    <h4>Lorem ipsum dolor sit amet</h4>
                                    <p>You can take full control of your theme with our powerful yet easy-to-use theme options panel from setting to styling.</p>
                                </div>
                            </div>
                
                        </div>
                        <div class="col-md-4 col-gutter">
                            <div class="featured-item">
                                <div class="featured-icon"><i class="fa fa-indent"></i></div>
                                <div class="featured-text">
                                    <h4>Lorem ipsum dolor sit amet</h4>
                                    <p>We include just the right amount of useful widget areas and sidebars so you can place your content.</p>
                                </div>
                            </div>
                            <div class="featured-item">
                                <div class="featured-icon"><i class="fa fa-paste"></i></div>
                                <div class="featured-text">
                                    <h4>Lorem ipsum dolor sit amet</h4>
                                    <p>We include our own widgets for Reviews, Social, Advertising and additional custom widgets to enhance your site.</p>
                                </div>
                            </div>
                
                        </div>
                        <div class="col-md-4 col-gutter">
                            <div class="featured-item">
                                <div class="featured-icon"><i class="fa fa-font"></i></div>
                                <div class="featured-text">
                                    <h4>Lorem ipsum dolor sit amet</h4>
                                    <p>We include FontAwesome Icons &amp; Hundreds of Google Fonts to Choose from to customize your site.</p>
                                </div>
                            </div>
                            <div class="featured-item">
                                <div class="featured-icon"><i class="fa fa-wordpress"></i></div>
                                <div class="featured-text">
                                    <h4>Lorem ipsum dolor sit amet</h4>
                                    <p>We continuously test our themes so you will know they are always compatible with the latest version of WordPress.</p>
                                </div>
                            </div>
                    
                        </div>
                    </div>
                </div>
            </section> -->
    
    
            <section id="contact" class="dark-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="section-title">
                                <h2>Contact Us</h2>
                                <p>If you have some Questions or need Help! Please Contact Us!</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="section-text">
                                <h4>Email</h4>
                                <p><i class="fa fa-envelope"></i> zeusfarm@gmail.com</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="section-text">
                                <h4>Business Hours</h4>
                                <p><i class="fa fa-clock-o"></i> <span class="day">Weekdays:</span><span>9am to 8pm</span></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <form name="sentMessage" id="contactForm" method="POST" novalidate="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Your Name *" id="name" required="" data-validation-required-message="Please enter your name.">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Your Email *" id="email" required="" data-validation-required-message="Please enter your email address.">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control" placeholder="Your Message *" id="message" required="" data-validation-required-message="Please enter a message."></textarea>
                                            <p class="help-block text-danger"></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <div id="success"></div>
                                        <button type="submit" class="btn">Send Message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <p id="back-top">
                <a href="#top"><i class="fa fa-angle-up"></i></a>
            </p>
            <footer>
                <div class="container text-center">
                    <p>© <span>2019</span> <a href="#">Zeus Agro Farm</a> .    All Rights Reserved.</p>
                </div>
            </footer>
    
        
    
            <!-- Bootstrap core JavaScript
                ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
            <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
            <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
            <script src="{{ asset('js/cbpAnimatedHeader.js') }}"></script>
            <script src="{{ asset('js/jquery.appear.js') }}"></script>
            <script src="{{ asset('js/SmoothScroll.min.js') }}"></script>
            <script src="{{ asset('js/theme-scripts.js') }}"></script>
    </body>
</html>
