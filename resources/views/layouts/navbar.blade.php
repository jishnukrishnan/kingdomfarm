
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset="utf-8">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Zeus Agro Ltd Farm Management Programme | Dashboard</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
      <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
      <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
      <!-- AdminLTE Skins. Choose a skin from the css/skins
           folder instead of downloading all of them to reduce the load. -->
      <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">

      <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">

      <link rel="stylesheet" href="{{ asset('css/fullcalendar.css') }}">

      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <!-- Google Font -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
      
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
    
      <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>ZA</b>Ltd</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Zeus</b> Agro Ltd Farm Management Programme</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
    
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
    
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  {{-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> --}}
                  {{-- <i class="fa fa-angle-down pull-right" ></i> --}}
                  <span class="hidden-xs">
                    {{@Auth::user()->name}}
                    </span>
                        {{-- Session::put('username', $user->username); --}}
                </a>
                <ul class="dropdown-menu" style="width:50%;">
                  <!-- User image -->
                  <li class="user-header" style="height: 0px ; padding:0px;z-index:-2;" >
    
                    <p>
                            {{-- {{Auth::user()->name}} --}}
                    </p>
                  </li>
                  <!-- Menu Body -->
                  {{-- <li class="user-body">
                    <div class="row">
                      <div class="col-xs-4 text-center">
                        <a href="#">Followers</a>
                      </div>
                      <div class="col-xs-4 text-center">
                        <a href="#">Sales</a>
                      </div>
                      <div class="col-xs-4 text-center">
                        <a href="#">Friends</a>
                      </div>
                    </div>
                    <!-- /.row -->
                  </li> --}}
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    {{-- <div class="pull-left">
                      
                    </div> --}}
                    <div class="pull-right">
                            <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                            Logout
                        </a>
    
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          
          <!-- search form -->
          {{-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                  </span>
            </div>
          </form> --}}
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->

          
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            
            <li class="active ">
              <a href="/dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                {{-- <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span> --}}
              </a>
              {{-- <ul class="treeview-menu">
                <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
              </ul> --}}
            </li>

            <li >
                <a href="/calendar">
                    <i class="fa fa-calendar"></i> <span>Calendar</span>
                </a>
            </li>


            <li class="treeview">
              <a href="#">
                <i class="fa fa-square"></i> <span>Templates</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="/plot"><i class="fa fa-circle-o"></i> Add Template</a></li>
                <li><a href="/viewplot"><i class="fa fa-circle-o"></i> View Template</a></li>           
               
              </ul>

            </li>
       
   

            <li class="treeview">
                <a href="#">
                  <i class="fa fa-archive"></i> <span>Inventory</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="/inventory"><i class="fa fa-circle-o"></i> Inventory</a></li>
                  <li><a href="/view_stock"><i class="fa fa-circle-o"></i> View Stock</a></li>           
                </ul>
            </li>
         

            <li >
              
              <a href="/genCosting">
                  <i class="fa fa-credit-card"></i> <span>General Costing</span>
              </a>
            </li>

            <li >
              
                <a href="/audits">
                    <i class="fa fa-pencil-square-o"></i> <span>Audit</span>
                </a>
              </li>

              
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-truck" aria-hidden="true"></i> <span>Workshop Management</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="/assetList"><i class="fa fa-circle-o"></i> Asset List</a></li>
                  <li><a href="/spareParts"><i class="fa fa-circle-o"></i> Spare Parts</a></li>  
                  <li><a href="/viewWorkshopStock"><i class="fa fa-circle-o"></i> View Stock</a></li>        
                  <li><a href="/manageCategory"><i class="fa fa-circle-o"></i> Manage Category</a></li>
                  <li><a href="/manageOperator"><i class="fa fa-circle-o"></i> Manage Operator</a></li>
                  
                </ul>
              </li>

         <?php //if(Session::get('privilege') == 1 || Session::get('privilege') == 2){ ?> 
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cogs"></i> <span>Manage</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="/stock_category"><i class="fa fa-circle-o"></i> Stock Ledger</a></li>
                <li><a href="/stock_items"><i class="fa fa-circle-o"></i> Stock Items</a></li>           
                <li><a href="/units"><i class="fa fa-circle-o"></i> Units</a></li>
                <li><a href="/status"><i class="fa fa-circle-o"></i> Status</a></li>
                <li><a href="/users"><i class="fa fa-circle-o"></i> Users</a></li>
              </ul>
            </li>
          <?php// } ?>

          

          <li >
            <a href="/oldPlotDatas">
              <i class="fa fa-history" aria-hidden="true"></i> <span>Old Plot Datas</span>
            </a>
          </li>


            <li class="treeview">
                    <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off"></i> <span>Logout</span>
                    
                        
                    </a>
    
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
            </li>
    
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
    
        <div class="mainArea">
            @include('layouts.messages')
            @yield('content')
        </div>
    
      {{-- <footer class="main-footer">
        <div class="pull-right hidden-xs">
    
        </div>
        <strong>Copyright &copy; 2018 <a href="https://www.kingdommining.com/">Kingdom</a>.</strong> All rights
        reserved.
      </footer> --}}
    
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    
    <!-- jQuery 3 -->
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('bower_components/moment/moment.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- Image Resizer -->
    <script src="{{ asset('js/imageMapResizer.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>

    <script src="{{ asset('js/fullcalendar.js') }}"></script>
    

    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    </body>
    </html>
