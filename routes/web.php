<?php


Route::group(['middleware' => 'auth'], function() { 
    
    Route::get('/status', 'PagesController@status');
    Route::post('/status_ins', 'PagesController@insertStatus');
    Route::post('/deleteStatus','PagesController@deleteStatus');
    Route::get('/stock_items', 'PagesController@viewStockItem');
    Route::post('/stock_ins', 'PagesController@addStockItem');
    Route::post('/deleteStockItem', 'PagesController@delStockItem');
    Route::get('/stock_category', 'PagesController@viewStockCategory');
    Route::post('/stock_cat_ins', 'PagesController@addStockCategory');
    Route::post('/deleteStockCat', 'PagesController@delStockCategory');
    Route::get('/inventory', 'PagesController@viewInventory');
    Route::get('/plot', 'PagesController@viewPlot');
    Route::post('/addStock', 'PagesController@addStock');
    Route::post('/utiliseStock', 'PagesController@utiliseStock');
    Route::get('/dashboard', 'PagesController@dashboard');
    Route::get('/fields/{id}', 'PagesController@viewfield');
    Route::post('/field_data_insert', 'PagesController@insertField');
    // Route::post('/loadField','PagesController@loadData');
    Route::get('/units', 'PagesController@viewUnit');
    Route::post('/units_ins', 'PagesController@insertUnit');
    Route::post('/deleteUnit','PagesController@deleteUnit');
    Route::post('/getItem','PagesController@getItemsByCategory');
    Route::get('/viewplot','PagesController@viewPlotData');
    Route::get('/viewSinglePlot/{id}','PagesController@viewSinglePlot');
    Route::post('/insert_template','TemplateController@store');
    Route::post('/check_template_exits','TemplateController@check_exists');
    Route::post('/fetch_template','TemplateController@fetch_template');
    Route::post('/delete_template','TemplateController@delete_template');
    Route::get('/filterInventory/{id}','TemplateController@filter_inventory');
    Route::get('/calendar','TemplateController@viewCalender');
    Route::get('/genCosting','TemplateController@generalCosting');
    Route::post('/insertGenCost','TemplateController@insertGenCost');
    Route::post('/getGCItem','TemplateController@getGCItem');
    Route::post('/deleteGC','TemplateController@deleteGC');
    Route::get('/users','TemplateController@viewUsers');
    Route::post('/updatePrivilege','TemplateController@updatePrivileges');
    Route::post('/getPlotData','TemplateController@plotDetails');
    Route::post('/selectedPrivilege','TemplateController@assignedPrivilege');
    Route::post('/getAvailItem','TemplateController@getAvailItem');
    Route::get('/view_stock','TemplateController@viewStockItem');
    Route::get('/audits','TemplateController@Audits');
    Route::post('/filterAudit','TemplateController@filterAudit');
    Route::post('/makeCategoryAsc','TemplateController@makeCategoryAsc');
    Route::post('/makeItemAsc','TemplateController@makeItemAsc');
    Route::get('/oldPlotDatas','TemplateController@oldPlotDatas');
    Route::get('/viewSingleOldPlotData/{id}','TemplateController@viewSingleOldPlotData');
    Route::post('/filtergclist','TemplateController@filtergclist');

    Route::get('/assetList','workshopController@manageAssets');
    Route::post('/assetListIns','workshopController@addAssets');
    Route::post('/deleteAssetlist','workshopController@deleteAsset');
    Route::post('/getAssetList','workshopController@getAsset');
    
    Route::get('/spareParts','workshopController@manageSpareParts');
    Route::post('/addSparePart','workshopController@addSpareParts');
    Route::post('/utiliseSpareParts','workshopController@utiliseSpareParts');
    Route::post('/getSPValues','workshopController@getSPValues');
    Route::post('/filterSpareparts','workshopController@filterSPValues');
    Route::get('/filterSparepartCategory/{id}','workshopController@filterSPCategory');
    Route::post('/makeWCategoryAsc','workshopController@makeWCategoryAsc');
    Route::post('/makeSpareAsc','workshopController@makeSpareAsc');
    
    Route::get('/viewWorkshopStock','workshopController@viewWorkshopStock');

    Route::get('/manageCategory','workshopController@manageCategory');
    Route::post('/workshopCateIns','workshopController@addCategory');
    Route::post('/deleteWorkCate','workshopController@deleteCategory'); 

    Route::get('/manageOperator','workshopController@manageOperator');
    Route::post('/operatorIns','workshopController@addOperator');
    Route::post('/getOperator','workshopController@getOperator');
    Route::post('/deleteOperator','workshopController@deleteOperator');
    

});



Route::get('/', 'PagesController@index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



